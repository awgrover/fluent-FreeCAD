# Fluent programming for FreeCAD PartDesign

Inspired by cadquery, but this makes FreeCAD objects in PartDesign. So, you can build on existing bodies, and experiment modifying the generated bodies/steps.

Once a script is run, the bodies are persistent. No macro/code is required to open a document, or even to update parameteric values.

Opinionated. Because, I only use FreeCAD occasionally, and I'm trying to work out workflows.

## Usage

I generally use this to

* make every dimension parametric from spreadsheets
* operate further on sketches/bodies that I've built manually: modifying, positioning, and making variants of them. For example, a sketch can be padded to make a reference object, and then subtracted from a bunch of other shapes that it fits within.
* iterate on the steps, piece by piece.
* do most of my CAM operations, by: selecting, duping, layout, operations, gcode export.

A script goes in a text-document, then you run it with a macro.

* macro_executor.FCMacro goes in FreeCAD's Macro folder
* The fluent3 folder goes in FreeCAD's Mod folder
* a TextDocument goes in a group
* Mark it as "Syntax Highlighting" = Python, in the View Properties
* Paste some boiler plate in

```
   import fluent3; import importlib; importlib.reload(fluent3) # reload
   from fluent3 import * # import all
```

* Write a fluent sequence, like:

```
    (Fluent(Script)
        .body("squarething")
        ... PartDesign operations ...
    )
```

* Save everything. Run it by selecting the TextDocument, and execute the `macro_executor.FCMacro`
* It can corrupt the running FreeCAD if it has errors. Revert might fix it, restarting should fix it.

### You can use an external editor
Which is how I do it.

* the `macro_executor.FCMacro` will find the corresponding .py file, copy it into the Text Document, then execute it
* If it can't find the .py file, then it just executes the code in the Text Document. So, you don't need to keep the external .py file around, nor send it with the .FCStd.

* Add a first line:

    `#external`

```
The .py file name is:
    "{TextDocument's.Label}.py"
or, if the Label is all uppercase, then also use the lowercase version of .Label
And is searched for in:
* "{os.path.dirname(TextDocument's.Document.getFileName())}/{TextDocument's.Document.Label}.scripts/"
* "{os.path.dirname(TextDocument's.Document.getFileName())}/"
e.g. for Chair.FCstd, for a TextDocument "make-legs"
/home/bob/chair-project/Chair.scripts/make-legs.py
/home/bob/chair-project/make-legs.py
```


For a specific filename.

    `#external=somename.py`

* Same directory search as above

For a specific filename, in a sub-directory.

    `#external=somepath/somename.py`

* Same directory search as above, but with somepath/ appended to directories

For a absolute path to a specific filename.

    `#external=/somepath/somename.py`

* Only searches for that exact .py file at that path

### Script it in chunks

So you can manage the objects, and debug the script.

* Make a group. Because the script will delete the previously made elements first, but only in the group.
* Only do rotation and translation using a LCS. Things just don't for me otherwise.
* Make sketches by hand. How would you programmatically handle redundant-constraints?
* Use a spreadsheet to hold and calculate values. Expressions in properties should be almost trivial (like a diameter/2). You should be able to change things in the spreadsheet, and never have to update a property-expression.
* MORE ...

So, like this:

      Application
        your-document
            Group-master-thingies
                body
                    master-sketch1...
                    master-lcs1...
            Group-make-thingie1
                manually-made-bodies
                TextDocument-to-make-more-stuff
                (stuff that is made)

MORE...

# Installing my macros

```
macro_dir=~/.local/share/FreeCAD/Macro
find . -name '*.FCMacro' | xargs -iX ln -s `pwd`/X $macro_dir
ln -s $macro_dir/move_up.FCMacro $macro_dir/move_down.FCMacro
ln -s $macro_dir/hide-job.FCMacro $macro_dir/show-job.FCMacro
ln -s `pwd`/fluent3 $macro_dir
ln -s `pwd`/fluent3/xopensbp_post.py $macro_dir
mkdir -p $macro_dir/post
find . -name '*_filter.py' | xargs -iX ln -s `pwd`/X $macro_dir/post
```
