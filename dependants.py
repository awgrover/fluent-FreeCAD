# Show who depends on the selected object(s)
# By selecting them, and by printing an indented tree
# If you have multiple documents open, it will show the dependants in those documents
# Shows Expressions and ExternalGeometry specifices

# INSTALL
# cd ~/.local/share/FreeCAD/Mod/lib
# pip3 install -t . pydot

import os, re
import importlib
import pydot
importlib.reload(pydot)

SingleDoc = False # True to look at only the active-doc. False looks at all open docs

Dot = None

Groupers = (
    'App::DocumentObjectGroup',
    'App::Part',
)
NoSelect = [
    *Groupers,
    ]

def print_top_down(sofar):
    indent = 0
    parents = sofar.InList
    if parents:
        for p in parents:
            xindent = print_top_down(p)
            indent=max(xindent,indent)
    print(" " * indent, sofar.Label)
    return indent+2

SubGraphs={} # name:obj
def ensure_cluster( document ):
    cluster_name = pydot.quote_if_necessary( document.Label )
    
    #print(f"subs { list(Dot.obj_dict['subgraphs'].values()) }")
    if cluster_name in SubGraphs:
        return SubGraphs[cluster_name] 
    else:
        s = pydot.Cluster( 
            document.Label,
            label=document.Label + ".FCstd",
            style='rounded',
            )
        SubGraphs[cluster_name] = s
        Dot.add_subgraph( s )
        return s

def ensure_nodes( *objects ):
    # give a list of objects (things with Labels)
    # we'll give back a list of pydot Nodes
    all=[]

    for obj in objects:
        which_graph = Dot if obj.Document == App.ActiveDocument else ensure_cluster( obj.Document )
        if n:=which_graph.get_node( pydot.quote_if_necessary(obj.Label) ):
            all.extend(n)
        else:
            tid = TypeId=obj.TypeId if 'Proxy' not in dir(obj) else str(obj.Proxy.__class__)
            n = pydot.Node( 
                obj.Label, 
                shape='Mrecord' if next((x for x in Groupers if obj.isDerivedFrom(x) ), None) else 'record', 
                #label=(f'{obj.Document.Label}|' if obj.Document != App.ActiveDocument else '') + obj.Label, 
                label= '<id>' + obj.Label, 
                TypeId=f'"{tid}"' 
                )
            which_graph.add_node(n)
            all.append(n)

    return all

def ensure_edge( source, destination ):
    # source/dest can be node or { node : property }

    if isinstance(source,dict):
        node,prop =  (list(source.keys())[0], list(source.values())[0] )
        ensure_property( node, prop )
        source = node.obj_dict['name'] + ":" + pydot.quote_if_necessary( prop )
    else:
        source = source.obj_dict['name'] + ':id'

    if isinstance(destination,dict):
        destination = list(destination.keys())[0].obj_dict['name'] + ":" + pydot.quote_if_necessary(list(destination.values())[0])
    else:
        destination = destination.get_name() + ':id'

    graph_edge = pydot.Edge( source, destination )
    edge_points = (graph_edge.get_source(), graph_edge.get_destination())

    if not edge_points in Dot.obj_dict['edges'].keys():
        Dot.add_edge( graph_edge )

def ensure_property( node, prop ):
    pname = f'<{prop}>{prop}'
    m = re.search(r'\|' + pname + r'(\||$)', node.obj_dict['attributes']['label'])
    if m:
        #print(f'## already {prop} . {node.obj_dict["attributes"]["label"]}   {node.obj_dict["name"]}')
        pass
    else:
        node.obj_dict["attributes"]['label'] += '|' + pname
        #print(f'## added {prop} . {node.obj_dict["attributes"]["label"]}   {node.obj_dict["name"]}')
    

def print_bottom_up(seen, sofar, indent):
    if sofar not in seen:
        seen.add(sofar)

        # highlight things that aren't NoSelect
        if not next( (x for x in NoSelect if sofar.isDerivedFrom(x)), None): 
            Gui.Selection.addSelection(sofar)

        had_expression = False
        for property,exp in sofar.ExpressionEngine:
            if dep:=next( (x for x in seen if x.Name in exp or f'<<{x.Label}>>' in exp), None ):
                had_expression=True
                us,them = ensure_nodes( sofar, dep )
                ensure_property(  them , property )
                ensure_edge( { them : property}, us )
                print("  " * indent, f'{property} = {exp}') 

        was_external = False
        if sofar.isDerivedFrom('Sketcher::SketchObject'):
            # ExternalGeometry
            i=-1
            for eobj,elements in sofar.ExternalGeometry:
                if eobj in seen:
                    was_external=True
                    us,them = ensure_nodes( sofar, eobj )
                    for e in elements:
                        i+=1
                        ensure_property( them, e )
                        ensure_property( us, f'External{i+1}' )
                        ensure_edge( {us : f'External{i+1}'} , {them : e} ) # we use them
                    print("  " * indent, f'External {eobj.Label} {elements}') 

        if was_external or had_expression:
            indent += 1
                
        print("  " * indent, f'[{sofar.TypeId}]    {f"{sofar.Document.Label} . " if sofar.Document != App.ActiveDocument else ""}{sofar.Label}')
            
        # reversed tends to give the expected "later things depend on earlier thing"
        #   e.g. body<-pad<-binder
        for p in reversed(sofar.InList):
            if p==sofar:
                continue
            if SingleDoc and p.Document != App.ActiveDocument:
                continue
            us,them = ensure_nodes(sofar, p)
            ensure_edge( them, us )
            print_bottom_up(seen, p, indent+1)

def main():
    global Dot
    sel = Gui.Selection.getSelection()

    try:
        seen = set()
        dotfile = os.path.dirname( sel[0].Document.getFileName() ) + "/" + sel[0].Label
        Dot = pydot.Dot( 
            sel[0].Document.Label, 
            graph_type='digraph',
            rankdir='RL',
            )
        print('---')
        for obj in sel:
            #print_top_down(obj)
            print('---Dependants')
            print_bottom_up(seen, obj, indent=0)

        # App.listDocuments().values()
        # App.ActiveDocument.getDependentDocuments() includes self
        Dot.write_raw( dotfile + ".dot" )
        Dot.write_png( dotfile + ".png" )
        Dot.write_svg( dotfile + ".svg" )
        os.system(f'gio open {dotfile}.png')
        Gui.Selection.clearSelection()
        Gui.Selection.addSelection( sel[0] )
    except Exception as e:
        Gui.Selection.clearSelection()
        Gui.Selection.addSelection( sel[0] )
        raise e
main()
