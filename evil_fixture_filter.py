import itertools
import Path
import null_filter

class EvilFixtureFilter(null_filter.NullFilter) :
    # FIXME:
    # src/Mod/Path/PathScripts/PathFixture.py ~ line 94 .execute()
    # Inserts an egregious G0(z=stock.bound.zmax)
    # That's nasty. At best, it should be safeheight!
    # And it should be a jog probably.
    # We are going to drop it, by G0 command in "operations" whose name is 'Fixture'
 
    def __init__(self,objectslist, filename, argstring):
        # same args as a postprocessor.export()
        # could do some init here based on args
        print(f'New! {self}')

    def filter(self, path_obj, path_commands):
        # .Name = comment|Gxxx|Mxxx|etc, .Parameters = { k:v...} for the remaining "words": Kvvvvv
        # Path.Command('G41 X5...')
        # Path.Command() .Name=,.Parameters=

        if path_obj.Name.startswith('Fixture'):
            # apply patterns to the path_commands
            # note that the G54 Fixture op is some fake class with only Label,Name,and Commands
            # which means we can't find the Job, and can't be any smarter than this (during this op)
            f = lambda c : ( Path.Command(f"(Fixture egregious G0 dropped (it assumed stock.bound.zmax was safe) {c} ({self.__class__.__name__})") if c.Name=='G0' else c)
            path_commands = (f(x) for x in path_commands)

        return path_commands
