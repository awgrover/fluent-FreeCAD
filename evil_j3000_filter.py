from null_filter import *

class EvilJ3000Filter(NullFilter) :
    """
    Notably Ramp generates evil G0's: 0,0,safeheight
        safeheight at 0,0 can be problematic as well as wasteful
    (ramp also loses speeds)
    So, we drop the first G0 0,0 -> G0 ,,z at the beginning of an op
    If we see some other command, then we don't need to drop it anymore

    JS,100.000000,100.000000
    J2,0.0000,0.0000
    and
    G0 0,0,xxx with speeds
    G0 F0.000000 X0.000000 Y0.000000 Z25.000000

    """

    def __init__(self,objectslist, filename, argstring):
        # same args as a postprocessor.export()
        # could do some init here based on args
        print(f'New! {self}')

    def filter(self, path_obj, path_commands):
        # .Name = comment|Gxxx|Mxxx|etc, .Parameters = { k:v...} for the remaining "words": Kvvvvv
        # Path.Command('G41 X5...')
        # Path.Command() .Name=,.Parameters=


        found = False
        pre = []
        for i,c in enumerate(path_commands):
            # we don't look for the G0 0,0 until after initial comments and any "speed only" commands
            if c.Name.startswith('(') or c.Parameters.keys()==['F',]:
                pre.append(c)
            elif eq_gcode(c, Path.Command('G0',{'X':0,'Y':0})):
                found = True
                print(f'## found! {c}')
                if 'Z' in c.Parameters:
                    insert = (
                        Path.Command(f"(Unsafe first G0 home dropped, Z kept [{i}]:{c} {self.__class__.__name__})"),
                        Path.Command('G0',{ 'Z' : c.Parameters['Z'] } )
                    )
                else:
                    insert = (
                        Path.Command(f"(Unsafe first G0 home dropped, No Z [{i}]:{c} {self.__class__.__name__})"),
                    )
                path_commands = itertools.chain(pre, insert, list(path_commands)[i+1:] )

            else:
                """
                path_commands = itertools.chain(
                    (Path.Command( f"({self.__class__.__name__} Not Found)"),),
                    path_commands
                )
                """
                break

        return path_commands
