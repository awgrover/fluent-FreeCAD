# put in text-document in a freecad, run via macro_executor

import re
from copy import copy
from contextlib import contextmanager

import fluent3; import importlib; importlib.reload(fluent3) # reload
from fluent3 import * # import all

print("\n\nStart TextDocument: " + Script.Label)
print(f"Group <<{Group.Label}>>" )

# limit steps to this list
# choose by key at end of {}
only= { 
'list' : [None], # just lists all the steps
'all' : [], # does CLEAN, builds all (except "_*"), make's stl
'clean' : [ 'CLEAN' ], # an explicit clean removes all that we built last time. an explicity list deletes each step & rebuilds it
'yourset' : [
    # 'CLEAN', # default is to only delete & rebuild each explicit step
    '_grungeit', # _* steps can only be run explicitly
    'step1',
    'step9',
],
'otherset' : [ ... ],
}['all'] # choose

# GLOBAL stuff

# standard start point
fluent = (Fluent(Script, clean= (only == [] or 'CLEAN' in only) )
            .using("ss-pentablock") """.cam()""" # default spreadsheet and maybe cam mode
            .using(Group)
            .forking()
)

# cad = 'pentablock3-above-CAD' # the cad document that we will use

cambatch = 'hardwood'
Subbatch=None
@contextmanager
def subbatch(s):
    global Subbatch
    was=Subbatch
    Subbatch=s
    try:
        yield
    finally:
        Subbatch=was

Steps=[] # possible
DidSteps=['CLEAN'] if only==['CLEAN'] else [] # actually did
Unconsumed=[] if only==['CLEAN'] else set(copy(only))
def Step(step):
    # for _ in Step('xxxx'):
    #   # will do NONE if only==None
    #   # will do the block
    #       # if only==[] ("do all")
    #           # will not do the block if '_*'
    #       # if 'xxxx' in only
    #   fluent().....
    # should we do this step?
    # delete old emissions
    # .marker new ones
    # but we also should keep track of the possible steps for sanity later
    # NB: steps with prefix '_' are "test" steps, and not counted as the normal "possible"
    global fluent
    was_fluent = fluent

    # note for sanity
    if not step.startswith('_'):
        Steps.append(step) # note it
    
    # skip _* if we are doing default-all
    if step.startswith('_') and only == []:
        return

    if step in Unconsumed:
        Unconsumed.remove(step)

    if only == None or (only and step not in only):
        print(f'Skip {step}')
        return

    else: 
        DidSteps.append(step)

        print(f'Do {step}')

        # delete old
        fluent().select(marker=step, owned=True, allow_empty=True).delete()

        # dynamic scope of a new fluent
        fluent=fluent().marker(step).forking()

    try:
        yield
    finally:
        fluent=was_fluent

    return not Steps or step in (step)
    
def folder_name(grouping, subbatch=None):
    # `subbatch` is optional
    return '-'.join( x for x in (grouping, subbatch or Subbatch,cambatch) if x != None)

## MAIN

# your steps, in order, here, as a block in the `for ...`.
for _ in Step('_test'):
    (fluent()
        .select(within=Group, match='face-', map=(lambda f,obj: obj.Model.Group)).show() # doesn't work
    )

# SUMMARY
print(f'Did {"all" if Steps==DidSteps else "none" if DidSteps==[] else "only"} {DidSteps}')
if only==[None]:
    print(f'All {Steps}')
else:
    if 'CLEAN' in Unconsumed:
        Unconsumed = [ x for x in Unconsumed if x != 'CLEAN' ]
    if Unconsumed:
        App.Console.PrintError(f'`only` had some steps ({list(Unconsumed)}) not in valid steps ({Steps})\n')

fluent().todos()

# re-select ourselves for convenience
Gui.Selection.addSelection(Script)
