__all__ = ['Fluent', 'App','Gui', 'debug', 'MM_IN']

import io,os.path,re,time,inspect
import math
from copy import copy
from contextlib import contextmanager
from collections.abc import * # Sequence, Iterable, etc. https://docs.python.org/3/library/collections.abc.html
from numbers import Number
from lazy_loader.lazy_loader import LazyLoader
import os
from os import path
import numpy

import FreeCAD, FreeCADGui
import Part,Sketcher,Draft,Mesh
from FreeCAD import Base # for exceptions...
from FreeCAD import Units
import draftobjects
import Mesh

App=FreeCAD
Gui=FreeCADGui

FluentOwned = 'FluentOwned'
FluentMarker = 'FluentMarker'
MM_IN = 25.4

clean_ct = 0
gui_update = time.monotonic()
watchdog = time.monotonic()
watchdog_timeout = 0

def debug(msg,up=0):
    # if lambda...
   
    frame = inspect.currentframe()
    for i in range(0,up+1):
        poss_frame = frame.f_back
        if poss_frame: # prevent too far
            frame = poss_frame

    frame_code = frame.f_code
    #App.Console.PrintWarning( f"FRAME {frame.f_lineno}]\n" )
    fname = frame_code.co_filename
    if fname.startswith(App.getUserAppDataDir()):
        # trim app dir, esp for in a macro
        fname = fname[ len(App.getUserAppDataDir()) : -1]
    App.Console.PrintWarning( f"[{fname}:{frame.f_lineno}] {msg}\n" )

    let_gui_update()
    check_watchdog()
    return True # useful in lambdas: debug(...) and ...

def let_gui_update(force=False):
    # let report-view & 3d update
    global gui_update
    if force or (time.monotonic() - gui_update >= 1):
        Gui.updateGui()
        gui_update = time.monotonic()

def check_watchdog():
    # watchdog
    if watchdog_timeout and time.monotonic() - watchdog >= watchdog_timeout:
        let_gui_update(force=True)
        raise Exception(f"Timeout {watchdog_timeout} secs")

def islisttype(candidate):
    # is a tuple, list, etc (not str)
    # using abc.Sequence
    # aka is_sequence
    return not isinstance(candidate, str) and isinstance(candidate, Sequence)

def aslist(x):
    # make sure x is a listtype, forcing to (x,) if not
    # but leave as None if None
    if x==None:
        return None

    return x if islisttype(x) else (x,) 

def flatten(listtuple):
    # to a list
    # recursive
    sofar=[]
    for x in listtuple:
        if islisttype(x):
            sofar.extend( list( flatten(x) ) )
        else:
            sofar.append( x )
    return sofar

class class_or_instance_method(object):
    # decorator for class & instance method
    def __init__(self, f):
        self.f = f

    def __get__(self, instance, owner):
        if instance is not None:
            wrt = instance
        else:
            wrt = owner

        def newfunc(*args, **kwargs):
            return self.f(wrt, *args, **kwargs)
        return newfunc

UsingMap = {
    # for setting pushdown context of some things
    # and shortcuts for using()
    # TypeId : .context key
    'last' : '_last' # hack
    }

class NoArg:
    pass # marker for nothing

"""ObjectCopies
    Nobody can do "on datum/lcs"
        except shape-binder + sketch + external-geometry
    DraftClone .draft_clone() -> Draft.make_clone
        A clone that can have positioning
        Used by cam to layout pieces
    ShapeBinder
        Basic "view" of the object, 
        Can go in partdesign bodies: gets around 'out of scope'
        Relative to origin location (TraceSupport=False)
        or Relative to ThisBody.Origin (TraceSupport=True)
            So, can "erase" original's positioning
        Can not cross documents.
        No other properties
    SubShapeBinder
        Weirdly smarter "view"
        Better (supposedly) than SubShapeLink re: topo-naming-problem
        Has Position, relative to ThisBody.Origin
        Can go in partdesign bodies: gets around 'out of scope'
        Can cross documents.
        Seems especially oriented for 2d/wire-things...
            Weirdly has an "offset" function
    Link
        A light-weight view of the object.
            in fact, you can edit it at the link
        Can not go inside partdesign body
            even boolean can't (will grab original)
        Has additional Position
            but, that's relative to original position
            must set LinkTransform=False
    SubShapeLink
        A view of a step in a partdesign-body
        Can not link to a body!, only a sub-step
        otherwise it is Link
    Sketch Binder + Extern Geometry
        "traceable geometry" from another sketch
        bring in a sketch-geom-object
            essentially as construction, but non-editable
        that you can constrain to
    PartDesign Clone
        Clones bodies:
            makes new body
            makes a "clone" as the base object in it
            has Position (relative to body?)
    Draft Clone
        ?
    Part x-copy
        looks like a link?
        Can't go inside a part-design body directly
        Can be booleaned
        simple-copy
            no position
        part transformed copy
            has position
        part element-copy
            can copy a sub-element like an edge
    CarbonCopy Sketch
        Reuse a sketch (but poorly)
        Duplicates each sketch geom object
        Creates their constraints parametrically == to source constraint
            i.e. length =sourceSketch.Constraint[55]
        But doesn't track deleting/adding
        Relative to sketch "support"
    Scenarios
        Use a foreign sketch for external-geom: 
            shape-binder w/relative to us
        Re-use geom inside a sketch:
            sketch carbon-copy
        re-use a sketch (to pad, etc.)
            shape-binder
        re-use a padded-sketch
            don't. reuse the body (shape-binder?)
        Use a body in a boolean
            if the body is where you want
            uh. sometimes boolean eats the other body, sometimes not
            a link makes a copy
        Use a body in a boolean
            if you need to position the body
            ???
            sub-shape-binder and then boolean using it
Positioning
    partdesign bodies have an absolute position
    everything inside is relative to that

    Links and binders can erase the global position

    LCS/Datum
        relative to body of course
        Can be outside a body
            so "global" positioning
            not "out of scope"?
        only some things can be "supported" by them
            sketches mostly

"""

""" DRESSUP order
    ramp|leadinout : realop # must operate on a realop
    tag : dogbone : ramp : realop # maybe others too?
        and tag breaks dogbone

    Adding dressups
    try something like
        ..job .ramp then:
        .select(within='^job.Operations.Group', match='Ramp-WL-offset')
        .tags()
        .select(within='^job.Operations.Group', match='Tag-Ramp-WL-offset') # fixme: annoying
        .dogbone()
"""

class FluentGroup:
    """mixin for group
    """

    UsingMap['App::DocumentObjectGroup'] = '_group' # for .using()
    UsingMap['App::Part'] = '_group' # for .using()

    def __init__(self):
        super().__init__()

    def group(self, name=None, translate=None, rotate=None, reuse=True):
        """
        Make the group and set as current _group
        Things will get made in that (if appropriate)
        Will not create the group if it already exists
        Will reuse group if `name` is a group object (not string)
        `reuse`=False, and `name` is a string, 
            will always create a group, possibly with ...001 etc suffix
        `translate` and `rotate` create a StdPart
            which ought to behave like a group
            but has position
        FIXME: should we always just make it an app::part?, no, cam'ing treats app:part differently
        """

        already = self.find(name, fail=False)
        if already:
            name = already
            typeid = already.TypeId
        else:
            typeid = "App::Part" if (translate or rotate) else "App::DocumentObjectGroup"

        if isinstance(name,str):
            obj = self.add_object(self._group, typeid, label=name)
            if translate or rotate:
                self.transform(source=obj,translate=translate,rotate=rotate)
        elif name.isDerivedFrom(typeid):
            obj=name
        else:
            raise Exception(f"Push into existing group expects a group-object ({typeid}), saw {self.fullname(name)}")

        if isinstance(self,FluentRoot):
            group_context = SubFluentGroup.fork( self, _group = obj, _last=obj )
        else:
            group_context = self.__class__.fork( self, _group = obj, _last=obj )
        return group_context

    def temp_copies(self):
        """Fork, then make folder in our root (sibling to start)
        where temp stuff goes, so if we crash you can find it.
        You probably ought to .fork('^root') for this.
        Will not be cleaned by next Fluent!
        Will be deleted by various things (if empty), e.g. temp_copies_unlink(...)
        """
        debug(f"TempCopies()...")
       
        fluent = Fluent(self._root,clean=False)
        #fluent.describe()

        temp_work_folder = fluent.find_child('TEMP-work', with_suffix=True)
        if temp_work_folder and not temp_work_folder.isDerivedFrom('App::DocumentObjectGroup'):
            raise Exception(f"Expected {self.fullname(temp_work_folder)} to be a Group (App::DocumentObjectGroup)")

        if not temp_work_folder:
            debug("  New Group")
            fluent = fluent.group('TEMP-work').dont_clean()
        else:
            debug("  Extant Group")
            fluent._last=temp_work_folder
            fluent._group=temp_work_folder

        return fluent

    def temp_copies_push_link(self, obj):
        """link the object into temp_copies group
        """
        self.temp_copies().link(obj).dont_clean()
        return self
    
    def temp_copies_pop_link(self, name):
        """remove the _link_, and delete the TEMP-work dir if empty.
        Accepts name/label-of-original, obj-original, obj-link.
        Removes the _last_ matching link, cf .pop()
        Considers its job done, if there is no TEMP-work, or no obj-original/obj-link in it
        """

        thing = self.find(name)
        #debug(f"Unlink {self.fullname(thing)}")
        if not thing:
            raise Exception(f"Expected an existing original, to find its link in TEMP-work, but no such original: '{name}'")

        temp_work_folder = self.find_child('TEMP-work', parent_name=self._root, with_suffix=True)

        if not temp_work_folder:
            debug(f"  DONE: no TEMP-work")
            return self

        # see if this is a link in temp-work
        in_temp = next((x for x in reversed(temp_work_folder.Group) if x == thing), None)
        if in_temp:
            if not in_temp.isDerivedFrom('App::Link'):
                raise Exception(f"Expected a thing in TEMP-work to be a link, this wasn't: {self.fullname(in_temp)}")
            #debug(f"  Found exact obj (link) in temp-work: {self.fullname(thing)}")

        # see if there is a link to this
        else:
            in_temp = next((x for x in reversed(temp_work_folder.Group) if x.getLinkedObject() == thing), None)
            #debug(f"  Found link to thing in temp-work: {self.fullname(in_temp)}")

        if not in_temp:
            debug(f"  DONE: not in TEMP-work")
            return self

        #debug("  remove obj")
        self._doc.removeObject( in_temp.Name )

        # we remove the temp folder if empty
        if not temp_work_folder.Group:
            #debug("  remove folder")
            self._doc.removeObject( temp_work_folder.Name )

        self.recompute()
        return self

class FluentUsing:
    """Mixin for .using()
    Does a push of the context
    """

    def __init__(self):
        #debug(f"INIT {self}")
        super().__init__()

    def using(self, name=None, **key_obj):
        """Make the thing part of the current context
            like "which spreadsheet" 
            Or explicity body=obj e.g
            Can use shortcuts, like '^lcs'
        """

        if key_obj:
            k=tuple(key_obj.keys())[0]
            name=tuple(key_obj.values())[0]
            context_key = f"_{k}"
            if context_key not in self._context.keys():
                raise Exception(f"Don't know how to set a context for {k}={self.fullname(name)}, only know {[x[1:] for x in sorted(self._context.keys())]}=...")

            poss = self.find(name)

            # NB: we don't check that it is the right kind of thing!
            debug(f"Using {context_key} = {self.fullname(poss)} in { self.__class__.__name__}")
            return self.__class__.fork(self, **{ context_key : poss} )
        else:
            poss = self.find(name)
            debug(f"Using '{name}'->{self.fullname(poss)}")
            if not poss:
                self.describe(f"Couldn't find a '{name}'")
                raise Exception(f"Couldn't find a '{name}'")
        
            return self._using(poss, fail=True)

    def _using(self, poss, fail):
        # factored simple using
        # fail=True will throw if it can't be "used"

        #debug(f"### _using: {self.fullname(poss)} {self.fullname(self.effective(poss))}...")

        context_key = UsingMap.get( self.effective(poss).TypeId, None)
        #debug(f"  in usingmap {self.effective(poss).TypeId} ? {context_key}")
        if context_key:
            debug(f"Using {context_key} = {self.fullname(poss)} in { self.__class__.__name__}")
            return self.__class__.fork(self, **{context_key : poss} )
        elif fail:
            raise Exception(f"Don't know how to set a context for {self.fullname(poss)}, as effective {self.effective(poss)}\n\tonly know {list(UsingMap.keys())}. Try something like .using(body='{poss.Label}')")
        else:
            debug(f"Not a thing: {self.fullname(poss)} AKA {self.fullname(self.effective(poss))}")
            return self

class FluentSpreadsheet:
    """mixin for spreadsheet
    allows using()
    """

    # fixme: factor the pattern of UsingMap + __init__
    UsingMap['Spreadsheet::Sheet'] = '_ss' # for .using()

    def __init__(self):
        #debug(f"INIT {self}")
        if '_ss' not in self._context.keys():
            self.context(_ss = None)
        super().__init__()

class FluentSelect:
    """Mixin for .select()
    Does a push of the context
    """

    # hack: just want it to show in describe
    UsingMap['select'] = '_selection' # for .using()

    def __init__(self):
        #debug(f"INIT {self}")
        if '_selection' not in self._context.keys():
            self.context(_selection = [])
        super().__init__()

    def sort(self, key=None):
        """Sort the selection by Label or key(obj)"""
        if key==None:
            key = lambda o: o.Label

        self._selection.sort( key = key )
        return self

    def select(self,
            # From what set?
            name=NoArg, # the name, names, or None to force an empty selection, default type
            refine=False, # if true, use last _selection
            within=None, # in the specified group|document
            document=None, # in the specified document file name
            # Filter that set
            filter = None, # lambda x: t|f # to include
                # also determines the exact object in the selection: True for whole object, some specific x like (Body, "Edge1")
            allow_empty = False, # throws on empty select if False
            more=False, # if true, add to the previous selection
            match=None, # re.match on label/name (i.e from beginning)
            regex=None, # re.search on label/name (i.e first match anywhere)
                # NB: be careful with "not", like [^_], becase we match on Label OR Name
            type=None, # A::B for TypeId using isDerivedFrom('A::B'), A.B for str(obj.Proxy.__class__)=='A.B'
                        # sad, not isinstance()
            # Change  the results
                # nb: expression should be "relative" to each object, so start expression with "."
            expression=None, # expression from each-object-in-result to an object, e.g. .Model.Group[0]
            map=None, # and map/filter the results, map(fluent, [sofar...]) (and flatten)
            # Limit the results
            inrange=None, # limit the result to this range, e.g (0,1)
            marker=None, # things matching the string, usu after a .marker('xxx')
            owned=False, # true to limit to owned by this script
            ):
        """Sets the .selection
            Throws if nothing selected
            document="filename" if you don't have an external document-object
            within=group|document # limit
            The arguments are exlusive, except `document`, `within`, and `inrange` is last
        """
        debug(f"Select...") 
        no_filter = False

        if document and isinstance(document,str):
            document = self.get_document(document) 

        # name==None means use selection (same as `more`), now redundant
        if name != NoArg and name==None:
            # use selection
            return self.__class__.fork(self, _selection = [] )

        elif name != NoArg:
            # FIXME: shouldn't be a filter? or a select DSL...w/optimization
            name = [ self.find(x, document=document or self._doc) for x in aslist(name) ]
            fn = (lambda obj : obj in name or obj.Label in name or obj.Name in name)

        elif match:
            fn = (lambda obj :
                re.match(match, obj.Label) or re.match(match, obj.Name)
                )
        elif regex:
            fn = (lambda obj :
                re.search(regex, obj.Label) or re.search(regex, obj.Name)
                )
        elif marker:
            fn = (lambda obj : marker in getattr(obj, FluentMarker, []) )

        elif type:
            if '::' in type:
                fn = (lambda obj : 'isDerivedFrom' in dir(obj) and obj.isDerivedFrom(type) )
            elif '.' in type:
                # fixme: is there a way to get the full class name?
                fn = (lambda obj : 'Proxy' in dir(obj) and str(obj.Proxy.__class__) == f"<class '{type}'>")
            else:
                raise Exception(f"Expected a string, A::B for TypeId isDerivedFrom, or A.B for class-name=='A.B'. Saw {type.__class__.__name__} {type}")
        
        else:
            # i.e. All
            fn = (lambda obj : True)
            no_filter = True
        
        if owned:
            _fn = fn # scope copy
            fn = (lambda x: _fn(x) and FluentOwned in x.PropertiesList and x.FluentOwned == self._root.Name)

        if filter:
            _fn = fn # scope copy
            fn = (lambda x: _fn(x) and filter(x))

        if refine:
            debug(f"#  within _selection {len(self._selection)}")
            within = self._selection
            
        elif within:
            orig_within = within
            debug(f"#  within {within}")
            within = self.find(within)    
        else:
            if no_filter:
                raise Exception("There was no filter, and no limit to group: won't select everything")
            within = document if document else self._doc

        if islisttype(within):
            poss = within
        elif within.isDerivedFrom('App::DocumentObjectGroup') or within.isDerivedFrom('App::Part'):
            poss = within.Group
        elif within.isDerivedFrom('App::Document'):
            poss = within.Objects
        else:
            raise Exception(f"Expected a Group or Document for `in`, saw {self.fullname(orig_within)}")

        if expression:
            if not expression.startswith('.'):
                expression = "." + expression
            change = (lambda obj: obj.evalExpression(expression))
        else:
            change = lambda x: x

        if inrange:
            inrange = inrange if isinstance(inrange,range) else range(*inrange) 

        debug(f"  within {len(poss)}")

        rez = []
        for x in poss:
            pred = fn(x)
            if pred:
                pred = [ pred ] if not isinstance(pred, list) else pred

                for y in pred:
                    if not filter:
                        y = x
                    if change:
                        #debug(f"Change {y}...")
                        y = change(y)
                        #debug(f"   -> {y}")
                    rez.append(y)

        if map:
            debug(f'    map from {[self.fullname(x) for x in rez]}\n\t{map}')
            rez = flatten( [ map(self.fork(self), x) for x in rez ] )
            debug(f"mapped {[self.fullname(x) for x in rez]}")
            # remove None
            rez = [ x for x in rez if x!=None and x!=[] ]
            debug(f"none-removed {[x.Label for x in rez]}")

        if inrange:
            rez = rez[ (inrange.start) : (inrange.stop) ]
        if not rez and not allow_empty:
            tried = []
            if name != NoArg:
                tried.append( f"name='{name}'" )
            if match:
                tried.append( f"re.match='{match}'" )
            if regex:
                tried.append( f"re.regex='{regex}'" )
        
            if within:
                tried.append( f"within='{self.fullname(within)}' {within.TypeId if hasattr(within,'TypeId') else within.__class__.__name__ }" )
            if document:
                tried.append( f"document='{self.fullname(document)}'" )
            if marker:
                tried.append( f"marker='{marker}'" )
            if inrange:
                tried.append( f"inrange='{inrange}'" )
            raise Exception( f".select found nothing ({ ', '.join(tried) })" )

        expected=""
        if name != NoArg:
            if len(name) != len(rez):
                expected += f" Expected {len(name)}, but got only {len(rez)}, because you gave that many names: {name}."
        debug(f"Selected {len(rez)} {[self.fullname(x) for x in rez]}")
        if expected:
            App.Console.PrintError( expected + "\n" )

        #if len(rez) == 0:
        #    print(self.arguments())

        if more:
            rez = [] + self._selection + rez

        return self.__class__.fork(self, _selection = rez )

    def arguments(self):
        # doesn't work that well, grabs lots of locals
        """Returns tuple containing dictionary of calling function's
           named arguments and a list of calling function's unnamed
           positional arguments.
        """
        from inspect import getargvalues, stack
        posname, kwname, args = getargvalues(stack()[1][0])[-3:]
        posargs = args.pop(posname, [])
        args.update(args.pop(kwname, []))
        return args, posargs

    def elements_not_none(self, alist, msg):
        if next( (x for x in self._selection if x==None), []) == None:
            raise Exception(msg)

    def is_selection_all(self, typeid, fail=None):
        typeid=aslist(typeid)
        bad = next( (x for x in self._selection if x.TypeId not in typeid and self.effective(x).TypeId not in typeid), None)
        if bad:
            if fail:
                if fail==True:
                    fail = f"Expected only {typeid} in .select"
                raise Exception(f"{fail}, saw {self.fullname(bad)}")
            else:
                return False

        return True
            

class FluentLink:
    """Mixin for links, shapebinder, etc like things"""

    def __init__(self):
        #debug(f"INIT {self}")
        super().__init__()

    def binder(self, source, name=None, trace_support=True):
        """
        See note ObjectCopies
        Binders make a copy, inside a Body
            So, it is not out of scope to refer to it. useful for sketches for external-geom, padding, etc
            Only has 1 property: TraceSupport
            No positioning, etc. (see subshapebinder)
        We'll set a default name
        and update the context!
        Because, making a binder to something like an LCS is like making an LCS
        So, that should be in the context.
        `TraceSupport`
            False : keep the global position of the source
            True : position at ThisBody.Origin NB: not LCS's!
            NOTE: our default is opposite Freecad's
        # FIXME: for the Part-as-build-space pattern, tracesupport should be false, this is all confusing
        """

        # App.getDocument('pentablock3_above_CAM').getObject('Body001').newObject('PartDesign::SubShapeBinder','Binder')

        source = self.find(source)
        debug(f"Binder {name} from {self.fullname(source)}")

        if not name:
            name = source.Label

        if not self._body:
            raise Exception("Expected a body to put binder in")
        obj = self.add_object(self._body, "PartDesign::ShapeBinder", label=name)
        obj.Support = [ (source,('',)) ]
        obj.TraceSupport = trace_support

        self._last = obj

        # see if we should set the context
        #debug(f"Usingmap {source.TypeId} : {UsingMap}")

        return self._using(obj, fail=False)

    def link(self, sources=None, name=None):
        """
        Operates on: an-object|[objects]|selection|_last
        New selection is the list of created links
        we'll set a default name
        and update the context!
        Like binder, it is tantamount to making an object, so set context.
        """

        orig_source = sources
        if not sources:
            sources = self._selection or [self._last]
        else:
            sources = sources if islisttype(sources) else [self.find(sources)]
        if not sources:
            why = f'source was: {orig_source}' if not orig_source else f'selection was {len(self._selection)}'
            raise Exception(f"Nothing to link to, {why}")

        new_selection = []
        is_multiple = len(sources) > 1
        ct = 0
        for source in sources:
            ct += 1
            if not name:
                this_name = "link" + (f"-{source.Document.Name}" if source.Document.Name != self._doc.Name else '') + f"-{source.Label}" + (f"-{ct}" if is_multiple else "")
            else:
                this_name = name + (f"-{ct}" if is_multiple else "")

            debug(f"Link {this_name} from {self.fullname(source)}")

            obj = None
            # put it in a group or the doc
            for poss in (self._group, self._doc):
                if poss:
                    debug(f"  created in {self.fullname(poss)}")
                    obj = self.add_object(poss, "App::Link", label=this_name)
                    break
            if not obj:
                raise Exception(f"Expected last to be group, or doc, but it was {self.fullname(self._last)}")

            obj.LinkedObject = source
            self.recompute(obj)

            new_selection.append( obj )
            self._last = obj

        # the selection would be confusing if nothing is made, and seems unlikely intent
        if ct == 0:
            raise Exception("Didn't make any links")

        self._selection = new_selection
        return self._using(self._last, fail=False)

    def local_only(self, objects):
        # Are the objects in this document?
        # Return them
        # fail if not

        non_local = []
        for obj in objects:
            if obj.Document.Name == self._doc.Name:
                continue
            
            # already linked?
            link_obj = next((x for x in self._doc.findObjects(Type='App::Link') if x.LinkedObject == obj),None)
            if link_obj:
                continue

            non_local.append( obj )

        if non_local:
            raise Exception(f"Expected the selection to all be local. Try .group()+.external() ? {[x.Label for x in non_local]}")

        return objects

    def clone(self, source=None, name=None, basename=None):
        """we'll set a default name and base's name
        From the `source` | _selection | _last
        and update the context!
        updates selection
        """

        if source == None:
            sources = self._selection or [self._last]
        else:
            sources = [ self.find(source) ]

        new_selection = []
        for source_i,source in enumerate(sources):
            if not name:
                this_name = f"clone-{source.Label}"
            else:
                this_name = name if len(sources)==1 else f'{name}-{source_i}'
                
            debug(f"clone {this_name} from {self.fullname(source)}")

            # Like boolean, this doesn't like to be created inside a group
            # so, make at top level then move
            obj = self.add_object(self._doc, 'PartDesign::Body',label=this_name)
            this_basename = basename if basename else f"base-clone-{source.Label + ('' if len(sources)==1 else f'-{source_i}')}"
            clone = self.add_object(obj, 'PartDesign::FeatureBase',label=this_basename)
            # clone's get a baseFeature
            clone.BaseFeature = source
            clone.Placement = source.Placement
            clone.setEditorMode('Placement',0)

            # the body with a clone does not get a base-feature, but gets the tip
            obj.BaseFeature = None
            obj.Tip = clone

            self.recompute(obj)
            self.move(obj, self._group)

            self._last = obj
            self._using(obj, fail=True)
            new_selection.append(obj)

        self._body = self._last
        self._selection = new_selection
        return self

    def effective(self, obj):
        # return the effective object
        # for shapebinder, link, clone, etc
        known = { 
        'PartDesign::ShapeBinder' : (lambda obj : obj.Support[0][0]),
        'App::Link' : (lambda obj : obj.LinkedObject),
        # might be wrong:
        'Part::FeaturePython' : (lambda obj : obj.Objects[0] if isinstance(obj.Proxy, draftobjects.clone.Clone) else obj),
        'Part::Part2DObjectPython' : (lambda obj : obj.Objects[0] if isinstance(obj.Proxy, draftobjects.clone.Clone) else obj),
        }
        #debug(f'    Effective of {self.fullname(obj)} VIA {obj.TypeId}')
        poss = known.get(obj.TypeId,None)
        # recurse for chain of links
        #debug(f'      try/recurse {poss(obj) if poss else "nope"}')
        if poss:
            pointee = poss(obj)
            return obj if pointee == obj else self.effective(pointee)
        else:
            return obj

    def draft_clone(self, source=None, name=None):
        """ see _draft_clone
        """
        debug(f"## clone sublang from {self} {self.__class__.__name__}")
        self = self.fork(self)
        self = self._draft_clone(source, name)
        self._body = self._last
        debug(f"## clone sublang {self} {self.__class__.__name__}")
        return self

    def _draft_clone(self, source=None, name=None):
        """
        Returns the clone, not a fluent method.
        from source(s) | selection | last
        make a draft-clone
        Name is "clone-{originalname}" or name{i} or (lambda obj,i: return name)
        Sets _last, _selection, but NOT _body
        Positionable
        """

        objects = aslist(source) or self._selection or self._last
        if name==None:
            naming = (lambda x,i: f"clone-{x.Label}")
        elif callable(name):
            naming=name
        else:
            naming = (lambda x,i: f"{name}-{i}")

        new_selection=[]
        for i,obj in enumerate(objects):
            debug(f'Clone {obj.Label}')
            clone = Draft.make_clone(obj, delta=None, forcedraft=False)
            self.own_it(clone, True)
            self.label_it(clone, naming(obj,i))
            if self._group:
                self.move( clone, self._group )
            self._last = clone
            new_selection.append(clone)
        self.recompute()
        self._selection = new_selection

        return self

    def external(self, name=None):
        """Are the selected objects in this document?
         probably want to be in a group just for links...
         name will be {document.Label}-{name or obj.Label}
        """

        debug(f"External(s)")
        if not self._selection:
            raise Exception("Expected something from .select()")

        if not self._group or self._group == self._root:
            raise Exception(f"Expected the current group to not be the root or the document: because it would be messy ({self.fullname(self._group)}). Do .group(....) first.")

        where = self._group

        ct=0
        was = self._last
        for obj in self._selection:
            if obj.Document.Name == self._doc.Name:
                debug(f"  Is from this doc (skipping): {self.fullname(obj)}")
                continue
            
            # already linked?
            link_obj = next((x for x in self._doc.findObjects(Type='App::Link') if x.LinkedObject == obj),None)
            if link_obj:
                debug(f"  Already is a link (skipping): {self.fullname(link_obj)}")
                #continue

            # not here already, and in other document
            # so link in us

            self._last = was
            self.link(obj, name=f"link-{obj.Document.Name}-{name or obj.Label}")
            ct += 1

        debug(f"  Linked {ct} into this doc")
        return self

class FluentVisual:
    """Visual stuff like hide, transparency"""

    def __init__(self):
        #debug(f"INIT {self}")
        super().__init__()

    def highlight(self, name=None, clear=False):
        """Gui select
        """
        if clear:
            Gui.Selection.clearSelection()
        for x in aslist(name) or self._selection or [self._last]:
            #debug(f"hi {x}")
            if isinstance(x,tuple):
                Gui.Selection.addSelection(*x)
            else:
                Gui.Selection.addSelection(x)
        return self

    def hide(self, name=None):
        if name:
            obj = name if islisttype(name) else [self.find(name)]
        else:
            obj = self._selection or [self._last]
        #debug(f"hide {self.fullname(obj)}")
        for x in obj:
            x.ViewObject.Visibility = False

        return self

    def show(self, name=None):
        """
        Set to "show"
        Operates on name=|_selection|_last
        """

        if name:
            obj = name if islisttype(name) else [self.find(name)]
        else:
            obj = self._selection or [self._last]
        debug(f"show {[self.fullname(x) for x in obj]}")
        for x in obj:
            x = self.find(x)
            x.ViewObject.Visibility = True

        return self

    def transparency(self, percent, name=None):
        """Set the transparency of name | ._last if a body, or specific"""
        
        if not name:
            obj = self._last
        else:
            obj = self.find(name)

        if not obj.isDerivedFrom('PartDesign::Body'):
            raise Exception(f"Expected a partdesign body ({self.fullname(obj) if name else '._last'}), saw {obj.TypeId}")

        self.recompute(obj)
        obj.ViewObject.Transparency = percent
        self.recompute(obj)

        return self

class FluentLCS:
    """Stuff about lcs"""

    UsingMap['PartDesign::CoordinateSystem'] = '_lcs' # for .using()

    OriginElements = {
        # the Origin.? for the support
        # In order of elements of Origin
        # shorthand:canonical
        'x':'X_Axis','y':'Y_Axis','z':'Z_Axis',
        'xy':'XY_Plane','xz':'XZ_Plane','yz':'YZ_Plane'
    }
    LCSElements = {
        # the MapMode for lcs
        # In order of elements of Origin
        # shorthand:canonical
        'x':'ObjectX','y':'ObjectY','z':'ObjectZ',
        #'xy':'XY_Plane','xz':'XZ_Plane','yz':'YZ_Plane'
    }

    def __init__(self):
        #debug(f"INIT {self}")
        if '_lcs' not in self._context.keys():
            self.context( _lcs = None )
        super().__init__()

    def shorthand_to_origin_lcs_element(self, origin_lcs, geom ):
        # geom: things like, 'z'->Z_Axis, xy -> XY_Plane
        # returns 
        #   (axis,None) for origin
        #   (None,mapmode) for lcs
        # 
        if origin_lcs.isDerivedFrom('PartDesign::Body'):
            # Origin
            if geom in self.OriginElements.values():
                # is what it is
                pass
            elif geom in self.OriginElements.keys():
                geom = self.OriginElements[geom]
                debug(f"    []{origin_lcs} -> {geom}")
            else:
                raise Exception(f"Shorthand for an body axis/plane should be in {self.OriginElements.keys()},{self.OriginElements.values()} saw '{geom}'")

            # Now get the actual object
            # For an Origin part: [..., (<origin.something>, ('',))]
            origin_i = list(self.OriginElements.values()).index( geom )
            geom = self._body.Origin.OutList[origin_i]
            debug(f"    [] -> Origin.OutList[{origin_i}] -> {self.fullname(geom)}")
            return (geom,None)

        elif origin_lcs.isDerivedFrom('PartDesign::CoordinateSystem'):
            # lcs
            if geom in self.LCSElements.keys():
                geom = self.LCSElements[geom]
            elif geom in self.LCSElements.values():
                pass
            else:
                raise Exception(f"Shorthand for an lcs axis/plane should be in {self.LCSElements.keys()},{self.LCSElements.values()} saw '{geom}'")
            debug(f"    [] -> {self.fullname(origin_lcs)} mapmode {geom}")
            return (None,geom)

        else:
            raise Exception(f"Expected a Body or LCS, don't know how to support a line on a {self.fullname(origin_lcs)}")

    def lcs(self, support=None, map_mode=None, flip_sides=False, translate=None, rotate=None, name=None):
        """
        add CoordinateSystem to current body
        translate is applied before rotate (create 2 lcs for other order)
        rotate can be in [x,y,z] degrees
        support .... default is body's origin xy "FlatFace". ie. same orientation
        """
        debug(f"lcs {name}")

        if not self._body:
            raise Exception("Must have a body")

        obj = self.add_object(self._body, 'PartDesign::CoordinateSystem', label=name)

        # FIXME: the named args should be extensible (plugins)
        if support or map_mode:
            # another lcs, origin xyz, or [ obj, obj, ojb...]
            # [] shorthands are: x,y,z -> X_Axis..., xy,xz.. XY_Plane...
            
            # freecad Support =
            #   (
            #       ( obj, ('property',...) ) # at least one property, '' for "self"
            #       e.g. (<Sketcher::SketchObject>, ('Vertex5',))
            #       e.g. (<Part::PartFeature>, ('',)) # for an lcs
            #       ...
            #   )
            def canonical_first( shorthand ):
                # first shorthands are: datum_plane, lcs, body.origin/body/none -> the xy plane
                #   or "Name.prop.prop...", e.g. "Sketch001.Vertex5"
                #debug(f"  first: {shorthand}")
                orig_shorthand = shorthand

                # to obj
                if '.' == shorthand:
                    # (explicit) default
                    #debug(f"    default -> ...")
                    shorthand = None # as if, so it can find recent support
                elif isinstance(shorthand,str):
                    # path or name
                    if '.' in shorthand:
                        (obj, prop) = self.path(shorthand)
                        #debug(f"    first:obj.prop -> {self.fullname(obj)} .. {prop}")
                        return (obj, (prop,))

                    shorthand = self.find(shorthand)
                    #debug(f"    found -> {self.fullname(shorthand)}")

                # find a base object
                if shorthand == None:
                    shorthand = self._datum_plane or self._lcs or self._body
                    debug(f"    default -> {self.fullname(shorthand)}")
                if shorthand.isDerivedFrom('PartDesign::Body'):
                    shorthand = shorthand.Origin
                    debug(f"    Origin -> {self.fullname(shorthand)}")

                # actual plane/face
                if shorthand.isDerivedFrom('App::Origin'):
                    # [3,4,5] = xy,xz,yz planes
                    shorthand = shorthand.OriginFeatures[3]
                    debug(f"    XY_Plane -> {self.fullname(shorthand)}")
                elif self.effective(shorthand).isDerivedFrom('PartDesign::CoordinateSystem'):
                    # lcs
                    debug(f"    LCS -> {self.fullname(shorthand)}")
                    
                else:
                    raise Exception(f"First thing for support should have Origin or LCS, saw {orig_shorthand} -> {self.fullname(shorthand)}")
                #debug(f"  -> {self.fullname(shorthand)}")
                return ( shorthand, ('',)) # "expanded"

            def canonical_geom( geom ):
                orig_geom = geom
                debug(f"  []'nth: {geom}")

                # shorthands
                if isinstance(geom,str):
                    if '.' in geom:
                        (obj, prop) = self.path(geom)
                        debug(f"    first:obj.prop -> {self.fullname(obj)} .. {prop}")
                        return (obj, (prop,))

                    origin_lcs = self._lcs | self._body
                    if self._lcs:
                        (dumy,mapmode) = self.shorthand_to_origin_lcs_element(self._lcs, geom )
                        raise Exception("NOT IMPL: for lcs we need to rework this to indicate mapmode")

                    elif self._body:
                        (geom,dumy) = self.shorthand_to_origin_lcs_element(self._body, geom )
                        return ( geom, ('',))

                    else:
                        raise Exception("Expected a body or lcs")
                else:
                    descr = self.fullname(geom) if isinstance(geom, Part.Feature) else f"{geom.__class__}:{geom}" 
                    raise Exception(f"2nd... geom objects for Support, haven't implemented handling {descr}")


            # uniformly as a list
            if isinstance(support,str) or not isinstance(support, Sequence):
                support = [support]

            actual_support = [ canonical_first( support[0] ) ]
            actual_support.extend( [ canonical_geom(x) for x in support[1:] ] )

            #debug(f"  .Support = {actual_support}")
            #debug(f"  .MapMode = {map_mode}")
            obj.Support = actual_support
            obj.MapMode = map_mode
            obj.MapReversed = flip_sides

            if not map_mode:
                map_mode = 'FlatFace'

        else:
            if self._lcs:
                #debug(f"  support xy lcs {self.fullname(self._lcs)}")
                obj.Support = [(self._lcs,'')]
                obj.MapMode = 'ObjectXY'
            else:
                # None -> body origin
                # [3] is the 1st plane, xy
                #debug(f"  support xy body {self.fullname(self._body)}..{self._body.Origin.OutList[3]}")
                obj.Support = [(self._body.Origin.OutList[3],'')]
                obj.MapMode = 'ObjectXY'

        self.transform(obj, translate, rotate)

        #debug(f"    AttachmentOffset: {obj.AttachmentOffset}")

        self.recompute(self._body)

        self._last = obj
        sublang = self.__class__.fork(self, _lcs=obj )
        return sublang

    def transform(self, source=None, translate=None, rotate=None):
        """
        # Prefer using a lcs or datum plane as the location,
        # body transforms don't play as nice ("different coordinate system")
        # Modifies Attachment|Placement, first that it has
        Operates on source Or each _selection, or _last
        translate is applied before rotate (create 2 lcs for other order)
        rotate can be in [x,y,z] degrees
        Leaves selection unchanged
        FIXME: add 'relative', then do like position_relative, requires reworking some stuff?
        """
        debug(f"Transform")

        if not source:
            source = self._selection or [self._last]
        elif not islisttype(source):
            source = [source]

        for obj in source:
            obj = self.find(obj)
            changed = False
        
            if (
                obj.TypeId == 'Part::FeaturePython' and isinstance(obj.Proxy, draftobjects.clone.Clone)
                or obj.TypeId == 'Part::Part2DObjectPython' and isinstance(obj.Proxy, draftobjects.shapestring.ShapeString)
                ):
                # A Clone HAS an AttachmentOffset, but don't use it
                # how many other exceptions are there?
                which_operation = 'Placement'
            else:
                which_operation = 'AttachmentOffset' if hasattr(obj,'AttachmentOffset') else 'Placement'

            if translate:
                # [x,y,z]
                debug(f"  translate: {self.fullname(obj)} {translate}")
                # maybe a Base.Vector?
                if not( isinstance(translate, Base.Vector) or ( isinstance(translate, Sequence) and len(translate) == 3 )):
                    raise Exception(f"'Translate' should be [x,y,z], saw {translate.__class__.__name__}:{translate}")
                # each arg may be an expression, so have to do each separately
                self.set_transform(obj, 'Base', translate, which_operation)
                changed = True

            if rotate:
                debug(f"  rotate: {self.fullname(obj)} {rotate}")
                # maybe a Base.Vector?
                if not isinstance(rotate, Sequence) or len(rotate) != 3:
                    raise Exception(f"'Rotate' should be [x,y,z] angles, saw {rotate}")

                self.set_transform(obj, 'Rotation', rotate, which_operation)
                changed = True

            if changed:
                self.recompute(obj)

        return self

    def set_transform(self, obj, which, xyz, placement_or_attachment='AttachmentOffset'):
        # which is Rotation or Base, simply the vector
        # Rotation is [roll,pitch,yaw], i.e. around-x,around-y,around-z
        #   Note that reading back the Rotation may not match, because of equivalent RPY's
        # can't keep expressions that are in a create(...)
        # so, create, then do setExpression('.AttachmentOffset.Base.z', u'101.6mm')
        # lcs has Hidden Rotation properties for yaw,pitch,roll
        # placement_or_attachment='AttachmentOffset'|'Placement'

        # To rotate around a point:
        # obj.Placement = App.Placement( obj.Placement.Base, App.Rotation(yaw,pitch,roll), App.Vector(thepoint x,y,z) )

        obj_get = lambda : getattr(obj, placement_or_attachment)
        obj_set = lambda v: debug(f'  set {obj.Label}.{placement_or_attachment}={v}') and setattr(obj, placement_or_attachment, v)

        if which=='Rotation':
            axis = ('Yaw','Pitch','Roll')

            # set the values first, use 0 if it would be an expression
            yaw_pitch_roll = (xyz[2],xyz[1],xyz[0]) # canonical order

            debug(f"   #### .Rotation = {yaw_pitch_roll}")
            values = []
            expr = []
            for i,v in enumerate(yaw_pitch_roll):
                (is_expr, cleaned) = self._expression( v )
                values.append( 0 if is_expr else v )
                debug(f"          ## {values.__class__} {values}")
                debug(f"          .{placement_or_attachment}.Rotation.{axis[i]} = '{values[-1]}' {'but expression: {cleaned}' if is_expr else ''}")
                # save expressions
                if is_expr:
                    expr.append( (lambda : debug(f"          .{placement_or_attachment}.Rotation.{axis[i]} = expr: '{cleaned}'") and obj.setExpression(f'.{placement_or_attachment}.Rotation.{axis[i]}', cleaned) ) )

            # set it
            # it's always constructed by App.Placement, even if AttachmentOffset
            obj_set( App.Placement( obj_get().Base, App.Rotation(*values), App.Vector(0,0,0)) )
            debug(f"       values {obj_get()}")

            # then set expression
            for x in expr:
                x()
            if expr:
                debug(f"       w/expr {obj_get()}")
            return

        # Base, position

        # I've had cases where .z refuses to calculate untill .x.clear_expression & redo
        # or, do .x last
        #xyz_names = { 'x':0,'y':1,'z':2 } # overkill, but allows changing order
        xyz_names = { 'z':2, 'y':1,'x':0} # overkill, but allows changing order
        debug(f'  set_transform order: {xyz_names}')

        # FIXME: bug. ignores some expressions unless something (which) is recomputed
        #   I thought this was an bug about the order of setting expressions in xyz, 
        #   but that only fixes it sometimes
        self.recompute()

        for axis,i in xyz_names.items():
            (is_expr, cleaned) = self._expression( xyz[i] )
            if is_expr:
                debug(f"   #### .{which}.{axis} = expr {cleaned}\n\t{xyz[i]}")
                self.set_property(obj, f'{placement_or_attachment}.{which}.{axis}', '=' + cleaned)
                self.recompute(obj)
                #obj.setExpression(f'.{placement_or_attachment}.{which}.{axis}', cleaned )
            elif xyz[i] != None:
                debug(f"  #### .{which}.[{i}]{axis} = {xyz[i]}")
                obj.clearExpression( f'{placement_or_attachment}.{which}.{axis}' )
                setattr( getattr(obj_get(),which), axis, xyz[i])
                self.recompute(obj)
                debug(f"       {obj_get()}")


    def multitransform(self, name=None, **operations):
        """really Part::MultiTransform
            Operates on _last
            Adding to _body
            Give a dict of operations (ordered):
                linear : { PartDesign::LinearPattern kv }
                mirror : { PartDesign::Mirrored }

            FIXME: don't bother. auto detect these patterns and consolidate to multi-transform

        """
        shapes = aslist(self._last)
        if not shapes:
            self.describe("Error")
            raise Exception("Expected a '_last' object to operate on")
        if not self._body:
            self.describe("Error")
            raise Exception("Expected a '_body' object to operate on")

        Possible = {
            'linear' : { 
                'typeid' : 'PartDesign::LinearPattern',
            },
            'mirror' : {
                'typeid' : 'PartDesign::Mirrored',
            }
        }
        Possible['mirrored'] = Possible['mirror'] # alias

        extra = set(operations.keys()) - set(Possible.keys())
        if extra:
            raise Exception(f"Expected only known ops ({Possible.keys()}), saw {operations.keys()}")
            
        for shape in shapes:
            obj = self.add_object(self._body, 'PartDesign::MultiTransform', name)

            obj.Transformations = []
            obj.Originals = [shape]
            obj.Shape=shape.Shape
            self.recompute(obj)
           
            transforms = []
            for opshort, op in operations.items():
                debug(f'## For {opshort}, {op}')
                trans = self.add_object( self._body, Possible[opshort]['typeid'], label=f'{obj.Label}-{opshort}' )
                transforms.append(trans)
                for k,v in op.items():
                    if k=='Direction':
                        which = {'X':0,'x':0,'Y':1,'y':1,'Z':2,'z':2}[v]
                        trans.Direction = ( self._body.Origin.OutList[which], [''] )
                        self.recompute()
                        continue
                    elif k=='MirrorPlane':
                        which = {'XY':3,'XZ':4,'YZ':5}[v]
                        trans.MirrorPlane = ( self._body.Origin.OutList[which], [''] )
                        self.recompute()
                        continue
                        
                    self.set_property( trans, k, v )

            obj.Transformations = transforms
            self.recompute(obj)

            self._body.Tip = obj
            self.recompute(self._body)
        
        return self

class FluentDatum:
    """Datum stuff"""

    UsingMap['PartDesign::Plane'] = '_datum_plane' # for .using()

    def __init__(self):
        #debug(f"INIT {self}")
        if '_datum_plane' not in self._context.keys():
            self.context( _datum_plane = None )
        super().__init__()

    def datum_plane(self, name=None):
        """You should Rotate the LCS, not the datum-plane"""
        if not self._body:
            raise Exception("Must have a body")

        debug(f"DatumPlane {name}")
        obj = self.add_object(self._body, 'PartDesign::Plane', label=name)

        if self._lcs:
            obj.Support = [(self._lcs,'')]
            obj.MapMode = 'ObjectXY'
            
        else:
            #if self._lcs:
            #    App.Console.PrintWarning( f"sketch goes on datum-plane, ignoring current lcs {self.full_name(self._lcs)}\n" )

            # [3] is the 1st plane, xy
            obj.Support = [(self._body.Origin.OutList[3],'')]
            obj.MapMode = 'ObjectXY'

        self.recompute(self._body)

        self._last = obj
        sublang = self.__class__.fork(self, _datum_plane=obj)
        return sublang

    def datum_line(self, support, length=None, name=None):
        """Rotate the LCS, not the datum-plane
            support is an axis: x,y,z
            length will switch to 'manual' with that length
        """
        if not self._body:
            raise Exception("Must have a body")

        debug(f"DatumLine {name}")
        obj = self.add_object(self._body, 'PartDesign::Line', label=name)

        if self._lcs:
            (dumy,mapmode) = self.shorthand_to_origin_lcs_element(self._lcs, support)
            debug(f"  @ LCS,{mapmode}")
            obj.Support = [(self._lcs,'')]
            obj.MapMode = mapmode
            
        elif self._body:
            (geom,dumy) = self.shorthand_to_origin_lcs_element(self._body, support)
            debug(f"  @ {self.fullname(geom)},ObjectX")
            obj.Support = [(geom,'')]
            obj.MapMode = 'ObjectX'

        else:
            raise Exception("Expected a body or lcs to support on")

        if length != None:
            obj.ResizeMode = 'Manual'
            self.set_property(obj,'Length', length)

        self.recompute(self._body)

        self._last = obj
        return self

class FluentPosition:
    """stuff about positioning"""

    def __init__(self):
        #debug(f"INIT {self}")
        super().__init__()

    def position_relative(self, relative_to, position, obj=None, margin=0, correction=(None,None,None)):
        """position the obj relative_to another one in x,y,z
            obj is [obj] | _selection | _last
            relative_to has to have .Shape
            See _position_relative()
        """
        # None means origin
        relative_to = self.find(relative_to) if relative_to != None else None
        for mover in aslist(obj) or self._selection or self._last:
            mover = self.find(mover)
            self._position_relative(mover, position, relative_to, margin=margin, correction=correction)
        return self

    def _position_relative(self, obj, position, relative, margin='0 mm', correction=(None,None,None)):
        # position relative
        # `relative` a geom-obj or None='origin' or an lcs
        # for each (x,y,z)
        #   any of: | < > 0 = None
        # |<> align same edges
        # 0 sets to absolute position 0
        # = sets to same offset as relative
        # None leaves alone
        #
        # allows "|someexpression", like "|-4" or "|+$height" etc
        #   (for any of < > | =)
        #   if `someexpression` starts with one of < > |
        #       then the obj's edge|middle is aligned with the relative's edge|middle
        #       e.g. '<>' means obj's min aligned to relative's max, i.e. tile going +-wise
        # FIXME: allow align local-origins to each other, like "|" but origins not .Center
        #   ...

        debug(f"PositionRelative {obj.Label} @ {position} vs {relative.Label if relative != None else 'origin'}")

        # need to make indexable
        position = { 'x':position[0], 'y':position[1], 'z':position[2] }

        self.recompute(obj)

        new_translate = []

        if margin==0 or margin==None:
            margin='0 mm'
        self.must_have_units(margin, '`margin` must have units')
        if margin.startswith('='):
            margin = margin[1:]

        def relative_bb_expression(bb_terminal):
            # a full parametric expression for
            # terminals of a boundbox
            #   .Center.x etc
            #   .XMax etc
            # for a geom object, or 'origin' or a LCS
            
            if not( re.search(r'^Center.([xyz])', bb_terminal) or re.match(f'([XYZ])(Min|Max)', bb_terminal) ):
                raise Exception(f"Expected [XYZ](Min|Max) or Center.[xyz] for the relative={self.fullname(relative)}, saw '{bb_terminal}'")

            # collapse origin/lcs to the center
            if relative==None:
                return '0 mm' # duh
            elif relative.isDerivedFrom('PartDesign::CoordinateSystem'):
                if m:= re.search(r'^Center.([xyz])', bb_terminal):
                    return f'<<{relative.Label}>>.Placement.Base.{m.group(1)}'
                elif m:= re.match(f'([XYZ])(Min|Max)', bb_terminal):
                    return f'<<{relative.Label}>>.Placement.Base.{m.group(1).lower()}'
            # use actual boundbox
            else:
                return f'<<{relative.Label}>>.{self._shape(relative)}.BoundBox.{bb_terminal} * 1mm'
                
        """ TEST
        blah = []
        for x in [ 'X','Y','Z' ]:
            for expr in [ f'{x}Min', f'{x}Max', f'Center.{x.lower()}' ]:
                resolved= relative_bb_expression(expr)
                blah.append( f"{expr}: {resolved} -> { self.eval_expression('='+resolved)}")
        print('did\n\t' + "\n\t".join(blah) )
        return None
        """

        for which_i, which in enumerate(['x','y','z']):
            which_u = which.upper()
            actual_margin = margin if which != 'z' else '0 mm'
            rel = position[which]
            debug(f'  # which {which} : {rel}')

            if rel==None:
                new_translate.append(None)
            elif m := re.match(r'(.)(.*)', str(rel)):
                # |<>andsomeexpression
                obj_dir = m.group(1)
                extra_expr = m.group(2)
                debug(f"  full extra[{which}]: {rel} -> obj's: {obj_dir} extra: {extra_expr}")

                # allow dual alignment: my center to it's left: "|<"
                #   |<>= gets the expression for obj's:
                #       | is center, < is min, > is max
                #       NB: NOT doing '=' (relative offset)
                #   second gets the expression for relative
                #   thus: base + relative - obj
                if m := re.match('([|<>])(.*)', extra_expr):
                    extra_expr = m.group(2)
                    rel_dir = m.group(1) # < > | =
                    debug(f"  dual-align rel's: {rel_dir}")
                else:
                    rel_dir = obj_dir
                    debug(f"  simple-align obj's: {rel_dir}")
                debug(f"  rel: {rel_dir}")

                elem_expr = '' # relative to self...

                obj_offset = {
                    '>' : f' - ( .{self._shape(obj)}.BoundBox.{which_u}Max * 1mm )',
                    '<' : f' - ( .{self._shape(obj)}.BoundBox.{which_u}Min * 1mm )',
                    '|' : f' - ( .{self._shape(obj)}.BoundBox.Center.{which} * 1mm )',
                    '=' : ' + 0 mm',
                    '0' : ' + 0 mm',
                }
                if obj_dir not in obj_offset.keys():
                    raise Exception(f"Expected | < > 0 = for object's position {position}['{which}'], saw '{obj_dir}'")

                rel_offset = {
                    '>' : ' + (' + relative_bb_expression(f"{which_u}Max") + ')',
                    '<' : ' + ( ' + relative_bb_expression(f"{which_u}Min") + ')',
                    '|' : ' + (' + relative_bb_expression( f"Center.{which}" ) + ')',
                    '=' : f' + <<{relative.Label}>>.Placement.Base.{which}',
                    '0' : ' + 0 mm',
                }
                if rel_dir not in rel_offset.keys():
                    # Does not catch dual-mode w/bad relative, because we parse that as obj_dir,expression
                    raise Exception(f"Expected | < > 0 = for relative's position {position}['{which}'], saw '{rel_dir}'")

                # margin direction depends on obj's & rel's edge
                actual_margin = {
                    '<<' : f"{actual_margin}",
                    '<>' : f"{actual_margin}",
                    '>>' : f"- ({actual_margin})",
                    '><' : f"- ({actual_margin})",
                }.get( obj_dir+rel_dir, '0 mm') # all others 0mm, i.e. no margin

                if relative != obj:
                    if not( rel_dir == '=' or rel_dir == '0' ):
                        # actual relatives need to "zero out" the current base position
                        elem_expr += f'.Placement.Base.{which} '

                    elem_expr += (
                        obj_offset[ obj_dir ] +
                        rel_offset[ rel_dir ] +
                        ' + ' + actual_margin
                    )

                    
                debug(f'#*#*#    base: {elem_expr}')
                if correction[which_i]:
                    elem_expr += " + " + correction[which_i]
                    debug(f'    correction: {elem_expr}')
                if extra_expr:
                    # allow $fromspreadsheet
                    dumy, extra_expr = self._expression('=' + extra_expr)
                    elem_expr = f'({elem_expr}) {extra_expr}'
                    debug(f'    extra: {extra_expr}')
                new_translate.append( '=' + elem_expr )

            else:
                raise Exception(f"Expected | < > 0 = for {position}['{which}'], saw '{rel}'")

        debug(f'  as {new_translate}')
        zero_out = [ (0 if x != None else None) for x in new_translate ]
        self.transform(source=obj, translate=zero_out, rotate=None)
        self.recompute(obj)
        self.transform(source=obj, translate=new_translate, rotate=None)
        self.recompute(obj)
        return self
             
class FluentExpression:
    """stuff about expressions"""

    def __init__(self):
        #debug(f"INIT {self}")
        super().__init__()

    def property(self, property, first=False):
        # list of the value for each _selection.$property
        # (or only the first)
        rez = ( o.evalExpression(property) for o in self._selection )
        if first:
            return next(rez)
        else:
            return rez

    def set_property(self, obj, property, value_or_expression, eval=False, substitutions={}, nodot=False):
        # sets a static value, or an expression
        # expressions start with "="
        # See _expression() for magic substitutions
        # If `eval`==True, then evaluate the expression first, and use that value
        # If expression starts with '==' then clean the expression and eval against obj -> expression
        # `nodot` is to force no leading dot

        if isinstance( value_or_expression, str) and value_or_expression.startswith('=='):
            value_or_expression = value_or_expression[1:]
            eval = True
        (is_expr,expr) = self._expression( value_or_expression, substitutions )

        # most things want .$property, but FeaturePython wants $property
        # and any x.x wants a leading .
        is_fp = obj.isDerivedFrom('Path::FeaturePython')
        # FIXME: I think my reasoning is wrong. '.x' should be for "thisobj.x", otherwise some global object
        debug(f'SET {obj.Label}.{property} dot? {("." in property or not is_fp) and not nodot}')
        expr_path = ("." if ("." in property or not is_fp) and not nodot else '') + property

        try:
            if is_expr:
                obj.clearExpression(expr_path)
                self.recompute(obj)
                if is_fp and not "." in property:
                    # FIXME I only saw a hint that you have to set the property,
                    # and this only correctly does a direct property
                    # And, for a path-op-pocket, StepOver refuses to use the expression
                    #   if we do setattr() without the `if`
                    if not hasattr(obj, property):
                        setattr(obj, property, 0)
                if eval:
                    expr = str(self.eval_expression('=' + expr, against=obj)) # str() gaaaa
                    debug(f'## EVAL {expr} to {expr}')
                debug(f'  setexpr {expr_path}: {expr}')
                self.recompute(obj)
                obj.setExpression(expr_path, expr)
            else:
                if True or is_fp: # prob do it always
                    debug(f'  clear {expr_path}')
                    obj.clearExpression(expr_path)
                setattr(obj, property, value_or_expression)

        except (Base.ParserError, TypeError) as e:
            App.Console.PrintWarning( f"\nFor {obj.Label}.{property}='{value_or_expression}'\n")
            if is_expr:
                App.Console.PrintWarning('\t-> ' + expr + "\n")
            raise e
        except (RuntimeError, TypeError) as e:
            App.Console.PrintWarning( f"For {obj.Label}.{property}={value_or_expression}\n\t{expr}\n")
            raise e

    def expression(self,expr):
        """convert to valid FreeCAD expression, = removed, $ to <<currentspreadsheet>>.path property resolution w/extra help.
            Magic: .SketchVertexN|.SketchVertex[n] -> .Geometry[correct-index]
        """
        isexpr,expanded = self._expression(expr)
        # don't care about isexpr
        return expanded

    def eval_expression(self, expr, against=None):
        """
        `expr` is a value or a '=expr'
        to calculate things in context
        using against | _last
        returns the _evaluated_ expression
        possibly as a Quantity
        """
        isexpr,expanded = self._expression(expr)
        if isexpr:
            debug(f'  eval {expr} -> {expanded} Against {(against or self._last or self._doc).Label}')
            v = (against or self._last or self._doc).evalExpression( expanded )
            debug(f'  evaled: {expr} -> {v} # will try Units.Quantity...')
        else:
            try:
                v = Units.Quantity(expr)
            except (ValueError,ArithmeticError) as e:
                # ok, no units
                v = expr

        return v

    def _expression(self, expr, substitutions={}):
        # if expr is a string, then "expand" expressions, else return it
        # (true, cleaned) if expression, else (false, expr)
        # Magic:
        # if startswith '=' (i.e. if an expression)
        #   $x is shorthand for x in current-spreadsheet
        #       $x -> <<$current-spreadsheet.Label>>.x
        #       use '\$' to escape a $
        #   substitutions is regex : replacement | lambda(m)
        #       i.e. use '#' as f"<<{job.Label}>>"
        

        if not isinstance(expr,str) or not expr.startswith('='):
            return (False, expr)

        else:
            #debug(f"    EXP: {expr.__class__} '{expr}'")
            # "expressions" must begin with =
            cleaned = expr[1:]

            def ss_sub(m):
                if not self._ss:
                    raise Exception(f"Expected a current .using('somespreadsheet') because an expression had '$': {expr}")
                # `m` should be "$", and m.group() "$xxxx"
                debug(f"  SS subst {m} on: {m.string}")
                return m.group(1) + f"<<{self._ss.Label}>>."

            our_magic = {
                # $x -> <<current-spreadsheet>>.x
                r'([^\\]|^)\$' : ss_sub,
            }

            full_magic = { **our_magic, **substitutions }
            full_pattern = '(?:' + ')|(?:'.join( full_magic.keys() ) + ')'
            debug(f'FullPattern r"{full_pattern}"')

            def do_magic(m):
                debug(f'  try magic {m} on "{m.group()}" of "{m.string}"')
                for pattern,action in full_magic.items():
                    debug(f'    candidate "{pattern}"...')
                    if actual_match := re.match(pattern,m.group()):
                        debug(f'      doit m:{actual_match} {action}')
                        if callable(action):
                            return action(actual_match)
                        else:
                            return action
                raise Exception(f"Weird: expected to find a pattern,action entry for the match '{m.group()}' in '{cleaned}' with entries {full_magic}")

            cleaned = re.sub( full_pattern, do_magic, cleaned)

            #debug(f"    exp:\n  '{expr}'\n  -->\n  '{cleaned}'")
            return (True, cleaned)

    def expression_map(self, some_seq):
        return [self.expression(x) for x in some_seq]

class FluentUtil:
    
    def __init__(self):
        super().__init__()

    def parent_type_of(self, obj, typeid, fail_not_found=True):
        typeid = aslist(typeid)
        is_derived = lambda o : next((x for x in typeid if o.isDerivedFrom(x)), False)
        rez = next((x for x in obj.InListRecursive if is_derived(x)), None)
        if rez==None and fail_not_found:
            raise Exception(f"Internal: expected to find '{typeid}' as parent of {self.fullname(obj)}")
        return rez
    def group_of(self,obj):
        return self.parent_type_of(obj,('App::DocumentObjectGroup','App::Part'))
    def body_of(self,obj):
        return self.parent_type_of(obj,'PartDesign::Body')

    @contextmanager
    def support_at_origin(self, supportable):
        """Set the support/mode to its body's origin xy, mode XY. i.e. zero
        """
        was_support,was_mapmode = (supportable.Support,supportable.MapMode)
        #debug(f"  with support-at-origin {self.fullname(supportable)} was {self.fullname(was_support)} mapmode {was_mapmode}")
        target_body = self.body_of(supportable)
        try:
            supportable.Support = [(target_body.Origin.OutList[3],'')]
            supportable.MapMode = 'ObjectXY'
            debug(f"  --> {self.fullname(target_body.Origin.OutList[3])}, {supportable.MapMode}")
            self.recompute(supportable)

            yield (was_support,was_mapmode) # don't really care
        finally:
            #debug(f"  with support-at-origin, put back {self.fullname(was_support[0][0])} mapmode {was_mapmode}")
            supportable.Support = was_support
            supportable.MapMode = was_mapmode
            self.recompute(supportable)

    def dont_clean(self):
        """mark the last item as not-to-be-cleaned on next Fluent()"""
        debug(f"Won't clean on next {self.fullname(self._last)}")
        # FIXME: several of these should probably be selection().foreach...
        self._last.FluentOwned=''
        return self

    def must_have_units(self, q, msg):
        # can't be comprehensive, because q could be an expression...
        if isinstance(q, Number):
            raise Exception(f"Must have units for {msg}, saw {q.__class__.__name__}: {q}")
        if isinstance(q, str):
            if q.startswith('='):
                return # punt
            
            # test
            try:
                x=Units.Quantity(q)
            except (ValueError,ArithmeticError) as e:
                raise Exception(f"Expected a quantity with units for {msg}, saw: {q}") from e

            if x.Unit == Units.Quantity('0').Unit:
                raise Exception(f"Expected a quantity with units for {msg}, saw: {q}")

    def shape(self,obj):
        # the obj.Shape, via LinkedObject if necessary
        return self._shape(obj, resolve=True)

    def _shape(self,obj, resolve=False):
        # return 'Shape' or 'LinkedObject.Shape' as appropriate
        # If resolve=True, then return the shape
        how=[ "LinkedObject" ] if self.find(obj).isDerivedFrom('App::Link') else []
        how.append('Shape')

        if resolve:
            rez = obj
            for m in how:
                rez = getattr(rez,m)
            return rez
        else:
            return '.'.join(how)

class FluentConditional:
    """conditional stuff"""

    class FluentSkip:
        """consume chained calls till ct (default == forever).
        A sublang, but noops everything
        """

        def __init__(self, hometown, skip=-1, silent=False):
            self._ct = skip;
            self._silent = silent
            self.hometown = hometown

        def __getattr__(self,m):
            def consumeone(*args, **kwargs):
                self._ct -= 1
                if self._ct == 0:
                    return self.hometown
                else:
                    if not self._silent:
                        debug(f"SKIP [{self._ct}] {m}(...)")
                    return self

            return consumeone

    def __init__(self):
        super().__init__()

    def onlyif(self, expr, fn=None, *args, **kwargs): # like def if()
        """If true, do fn() or the next method.
            if false, skip fn() or next method.
            fn(fluent, *args, **kwargs)
        """
        if expr:
            if callable(fn):
                return fn(self, *args, **kwargs) 
            return self
        else:
            return self if callable(fn) else self.FluentSkip(self, 1)

    def nop(self, *args, **kwargs):
        # ignore these args
        return self

    def skip(self, skip=-1, silent=False):
        """Skip the next N operations, none/-1 means forever"""
        if not silent:
            debug(f"SKIP {skip}")
        if skip==0:
            return self
        return self.FluentSkip(self, skip, silent)

    def set_last(self, v):
        """hack to set _last, probably in a call()
        """
        self._last = v
        return self

    def repeat(self, n, fn, *args, **kwargs):
        """ call fn n times with fn(fluent, i, *args, **kwargs)
            just convenience on call()
        """
        sofar = self
        for i in range(0,n):
            sofar = self.call( fn, i, *args, **kwargs )
        return self

    def call(self, fn, *args, **kwargs):
        """run fn(fluent,*args...)
            Without forking(), so you might want to .fork()
            needn't return anything.
            Does NOT change context
        """
        fn(self, *args, **kwargs) 
        return self

    def foreach(self, fn, *args, **kwargs):
        """call the lambda(forked-fluent, each-from-selection, i, *args, **kwargs)
        ONLY on _selection
        update the _selection of THIS fluent with the _last of each from the lambda
        """

        debug(f'Foreach ')
        new_selection = []
        for i,obj in enumerate(self._selection):
            debug(f'  each {obj.Label} as [{i}]')
            try:
                subf = fn(self.forking()(), obj, i, *args, **kwargs)
            except TypeError as e:
                if 'positional arguments' in str(e):
                    App.Console.PrintWarning( f"Called lambda with ( <fluent>, obj={self.fullname(obj)}, i={i}, *{args}, **{kwargs} )\n" )
                raise e

            if subf._selection:
                new_selection.extend(subf._selection)
            else:
                new_selection.append( subf._last )

        self._selection = new_selection
        return self

class FluentSibling:
    """Stuff about parent, child, sibling""" 

    def __init__(self):
        super().__init__()

    def find_child(self, child_name, parent_name=None, with_suffix=False):
        """freecad object: child of label or name or parent. of last if no name. None if not found
        beware, with_suffix has to match from beginning to a suffix of \d+
        """

        return next( self._find_children( child_name, parent_name, with_suffix ), None )

    def find_children(self, child_name, parent_name=None, with_suffix=False, prefix=True):
        """freecad object: matching children of label or name or parent. of last if no name. None if not found
        prefix is a .match(regex, eachobjname) (prefix defaults to true)
        beware, with_suffix has to match from beginning to a suffix of \d+
        """

        return list( self._find_children( child_name, parent_name, with_suffix, prefix ) )

    def _find_children(self, child_name, parent_name=None, with_suffix=False, prefix=False):
        # return a generator (.find_children)
        
        #debug(f"Find child {child_name} in {self.fullname(parent_name)}")
        if not isinstance(child_name,str) and (child_name.isDerivedFrom('Part::PartFeature') or child_name.isDerivedFrom('App::Document')):
            if with_suffix:
                raise Exception(f"You probably don't mean to find_child using an object ({self.fullname(child_name)}) AND with_suffix")
            child_name = child_name.Name

        if parent_name:
            parent = self.find(parent_name) if isinstance(parent_name,str) else parent_name
            if not parent:
                raise Exception(f"Can't find child of a non-existent parent '{parent_name}'")

            #debug(f"     find in parent {self.fullname(parent)}")
            #debug(f"    by name, so find in { parent.InListRecursive }")
            poss = parent.OutListRecursive
        else:
            if self._last:
                poss = self._last.OutListRecursive
            else:
                #debug(f"  parent is doc")
                poss = self._doc.Objects

        if not poss:
            return []

        if with_suffix:
            pattern = re.escape( child_name ) + r"(\d+)?$"
            #debug(f"  with_suffix, so: {pattern}")
            #simple = [x for x in poss if x.Label.startswith(child_name)]
            #debug(f"  in {[x.Label for x in simple]}")
            #debug(f"  re: {[x.Label for x in simple if re.match(pattern,x.Label)]}")
            return (x for x in poss if re.match(pattern,x.Name) or re.match(pattern,x.Label))
        elif prefix:
            debug(f"## by prefix re.match('{child_name}',x.Name)")
            return (x for x in poss if re.match(child_name,x.Name) or re.match(child_name,x.Label))
        else:
            #simple = [x for x in poss]
            #debug(f"  {child_name} in {[x.Label for x in simple]}")
            #debug(f"  re: {[x.Label for x in simple if x.Label==child_name]}")
            return (x for x in poss if x.Name == child_name or x.Label==child_name)

class FluentBoolean:

    def __init__(self):
        super().__init__()

    def boolean(self, bodies=2, into=None, common=False,fuse=False,cut=False, name=None):
        """Boolean 
            with (into): into | _body
            list of `bodies` | ct bodies | _selection
            ct is last bodies in this group (i.e. 2 includes current body)
            Workflow
                Main is last: create cutters... -> create main body -> boolean(ct=numberofcutters+1, cut=True)
                Main earlier: 
                    create main body -> create cutters 
                    Explicit other-bodies: -> boolean( into=main, bodies=[names...] )
                    last other-body: -> boolean(bodies='^last',...)
                    select other-bodies: -> select(cutters) -> boolean(bodies=None)
            FIXME: allow into=x,bodies=N; which uses last n bodies excepting `into`?
        """
        which = (common and 'Common') or (fuse and 'Fuse') or (cut and 'Cut')
        debug(f"boolean {which} '{name}' with { bodies }")
        if not which or len( [x for x in (common,cut,fuse) if x]) > 1:
            raise Exception("Expected one of common=True,fuse=True,or cut=True")

        if not self._group:
            raise Exception("Expected a current group")

        into = self.find(into) if into != None else self._body
        if not into:
            raise Exception("Expected a current body, or `into`")

        self.recompute(into)

        # `ct` is ignored if `sections` is provided
        if isinstance(bodies, Number):
            ct=bodies
            bodies = []
            # FIXME: a find by type in something:
            for s in self._group.OutList:
                # Boolean only tolerates a body (which may have been made by partdesign-clone)
                if not self.effective(s).isDerivedFrom('PartDesign::Body'):
                    # only them
                    debug(f'  Skip group member, not a Body {self.fullname(s)}')
                    continue
                if s == into:
                    #that was last one
                    debug(f'  found into: {into}')
                    break
                bodies.append(s)

            if len(bodies) < ct - 1:
                raise Exception(f"Could not find ct={ct}-1 bodies, saw only {len(bodies)} : {[x.Label for x in bodies]}")
            # last n
            bodies = bodies[-(ct-1) : ]
            debug(f"  ct: {len(bodies)}/{ct} : {[x.Label for x in bodies]}")

        if bodies:
            bodies = [ self.find(x) for x in aslist(bodies) ]
        elif self._selection and len(self._selection) >= 1:
            bodies = self._selection

        debug(f"  bodies: {len(bodies)} : {[x.Label for x in bodies]}")
        for b in bodies:
            # FIXME: consider some magic: if the body is not in our root, then clone it
            if True or common:
                # boolean 'common' doesn't work unless b is immediate child of doc: "links go out of scope"
                self.move(b, self._doc)

            self.recompute(b) # so boolean has complete info
            # FIXME: this works just like the gui, i.e. if you add a clone, you get the clone+original body (wrong?)
            #obj.addObject(b)
        self.recompute()

        if not bodies:
            raise Exception("Expected bodies=ct>=2 or bodies=names")

        obj = self.add_object(into, 'PartDesign::Boolean', name)
        #debug(f"  init w/victims {[x.Label for x in obj.Group]}")

        # This does not add the original-body of a clone
        # the test suite uses setObjects(), but I get extra bodies added if I do that,
        # .Group = xxx seems to do the right thing
        debug(f"  will setvictims: {[x.Label for x in bodies]}")
        obj.Group = bodies
        #debug(f"  is setvictims: {[x.Label for x in obj.Group]}")
        obj.Type = which

        # OMG, even though it already is 'Result', there must be some bug:
        # it will show the added-body(s), not the result, unless you do this
        # AND even then, when you reload the document, it shows the "Tools"
        # If you do boolean manually, it works though
        obj.ViewObject.Display='Result'
        obj.Refine=True # fails to respect Position if False
        # FIXME: write up bug for forum. 2bodies. script for boolean. 2nd w/display

        self._last = obj
        self.recompute(obj)

        #debug(f"  group: {[x.Label for x in obj.Group]}")
        return self.__class__.fork( self, _body = obj)

class FluentSketch:
    """sketch stuff"""

    UsingMap['Sketcher::SketchObject'] = '_sketch' # for .using()

    def __init__(self):
        #debug(f"INIT {self}")
        if '_sketch' not in self._context.keys():
            self.context( _sketch = None )
        super().__init__()

    def sketch(self, name=None):
        """make a sketch: no fluent inside of a sketch yet
        """
        debug(f"sketch {name}")

        if not self._body:
            raise Exception("Must have a body")

        obj = self.add_object(self._body, "Sketcher::SketchObject", label=name)

        self.set_support(obj)

        self._last = obj
        # Probably this forks...
        sketch_context = self.__class__.fork( self, _sketch = obj)
        return sketch_context

    def set_support(self, sketch, body=None):
        # to body | current support'ing object
        if body:
            sketch.Support = [(body.Origin.OutList[3],'')]
            sketch.MapMode = 'ObjectXY'
        elif self._datum_plane:
            sketch.Support = [(self._datum_plane,'')]
            sketch.MapMode = 'ObjectXY'
        elif self._lcs:
            sketch.Support = [(self._lcs,'')]
            sketch.MapMode = 'ObjectXY'
        else:
            # [3] is the 1st plane, xy
            sketch.Support = [(self._body.Origin.OutList[3],'')]
            sketch.MapMode = 'ObjectXY'

    def pad(self, distance, reversed=False, length2=None, subtract=False, name=None):
        """pad last sketch
            also Pocket if subtract=True
            `distance` can be: a distance or ThroughAll
        """
        debug(f"pad {name or self._sketch.Label} for {distance}")
        if not self._sketch: # FIXME: or a binder, or other 2d thing
            raise Exception("Must have a sketch")

        if subtract:
            obj = self.add_object(self._body, "PartDesign::Pocket", label=(name or f"pocket-{self._sketch.Label}"))
        else:
            obj = self.add_object(self._body, "PartDesign::Pad", label=(name or f"pad-{self._sketch.Label}"))

        if distance == 'ThroughAll':
            obj.Type = 'ThroughAll'
        else:
            self.set_property(obj, 'Length', distance)

        if reversed:
            obj.Reversed = True
        if length2:
            obj.Type = 'TwoLengths'
            self.set_property(obj, 'Length2', length2)
        obj.Profile = self._sketch
        self.recompute(obj)

        self._sketch = None

        self._last = obj
        return self

    def copy_pad(self, source_pad, name=None):
        """Copy the pad and it's sketch
        """
        debug(f"copy_pad {name} from {self.fullname(source_pad)}")

        if not self._body:
            raise Exception("Must have a body")

        obj = self.find(source_pad)

        source_sketch = obj.Profile
        # usually a tuple
        if isinstance(source_sketch, tuple):
            source_sketch = source_sketch[0]

        # context is pushed to new sketch
        self = self.carbon_copy_sketch( source_sketch, name=f"sketch-{name}-cc" if name else None )
        new_sketch = self._last
        self.recompute(new_sketch)

        self.pad( obj.Length, name=name) # fixme: assumes always a length
        new_pad = self._last

        # always "link" the value, so it always follows
        # FIXME: only handles the Type=Length case
        properties = ['Type', 'Midplane', 'Reversed'] # fixme: probably do the others...
        if obj.Type == 'Length':
            properties.append('Length')
        else:
            raise Exception("We only copy pads that are Type==Length (aka 'Dimension'). Fix the code _here_")
        for p in properties:
            #setattr(new_pad, p, getattr(obj,p))
            #debug(f"setexpression {new_pad.Label}.{p} = '{obj.Name}.{p}'")
            # FIXME: normally, you do ".Prop", but that doesn't work here:
            new_pad.setExpression(f"{p}", f"{obj.Name}.{p}")

        self.recompute(new_pad)
        self.recompute(self._body)

        return self

    def carbon_copy_sketch(self, source_sketch, as_construction=False, merge=False, origin=True, name=None):
        """
        If merge, add to last sketch
        else create a new one
        Do carbonCopy into the sketch
        Handles external-geometry by making extra shape-binders.
        The app's carbonCopy does some stuff to align the support planes,
        if origin = True
            We map origin to origin, so it doesn't matter
            (by temporarily setting our support to the body's origin, which should be 0)
            Not needed?
        Pushes context with new sketch
        """
        debug(f"carboncopy {name} <-- {self.fullname(source_sketch)}")

        if not self._body:
            raise Exception("Must have a body")

        source = self.find(source_sketch)

        source_external_geometry = source.ExternalGeometry
        # external geometry requires building in source-body & moving
        # and remembering the target_body
        build_and_move = source_external_geometry and self.body_of(source) != self._body
        target_body = None

        # https://wiki.freecadweb.org/Sketcher_scripting
        if merge:
            if not self._sketch:
                raise Exception(f"You said to 'merge', but no current sketch (for new sketch {name}) NOT IMPLEMENTED")
            if name != None:
                raise Exception(f"You said to 'merge', supplying a name doesn't make any sense: {name}")
            if build_and_move:
                raise Exception("Can't 'merge' if source is in other body and has external geometry (yet). Would require: cc w/o merge, then somehow copying geometry into target sketch (position issues?)")
            debug(f"  merge!")
        else:
            new_name = name if name else f"{source.Label}-cc"

            # External geometry requires building a binder and the new-sketch in the source-body
            # then moving it
            #debug(f"  build_and_move? {build_and_move}")
            if build_and_move:
                with self.recomputing(False):
                    self._last = self.carbon_copy_sketch_with_external(self._body, source, source_external_geometry, as_construction=False, name=new_name)
                    # FIXME? push a context? prob
                    self._sketch = self._last
                    # "part has null shape" is side-effect, but recomputes should prevent it?
                    self.set_support(self._sketch)
                    #debug(f"  made with external {self.fullname(self._sketch)}")
            else:
                # .sketch is a push'd context
                self = self.sketch(name=new_name) # sets last
                self.recompute(self._sketch)
            
        if not build_and_move:
            was_support=None
            was_map_mode=None
            if origin:
                with self.support_at_origin(source):
                    with self.support_at_origin(self._sketch):

                        debug(f"   native carbonCopy...")
                        self.recompute(source)
                        try:
                            # copy into self._sketch, from source
                            rez = self._sketch.carbonCopy(source.Name, as_construction) # second arg is: make all construction
                        except ValueError as e:
                            if 'Not able to add the requested geometry' in str(e):
                                raise ValueError( f"Try a manual carbon-copy operation: hover over the copy-from sketch, with combinations of ctr and ctr-alt.\n\toriginal error: {e.__class__} {str(e)}" ) from e
                                
                        #debug(f"  count {rez}") # not actually count apparently
                        #raise Exception("support at 0")
        
        self.recompute(self._sketch)
        self.recompute(self._body)
        return self

    def carbon_copy_sketch_with_external(self, target_body, source_sketch, source_external_geometry, as_construction, name):
        # all the machinations to deal with cc when the source-sketch is in a diff body, and has external-geometry
        # (recursively cc's for external-geom-sketches)
        # Assumes you only call this for the "when" condition above.
        # returns the new_sketch, which has been moved to the target-body
        # Does make a new context for the new sketch, but discards it
        # The caller should probably self._sketch=thisreturn

        debug(f"  ExternalGeom requires...")

        # cc a new-sketch in source-sketch body, origin.XY
        source_body = self.body_of( source_sketch)
        #debug(f"### source_body {self.fullname(source_body)}")

        # We need to cc using the same support and map_mode
        # But, we need to be fully in the context of the source_body
        #   which is too hard FIXME
        # we need at least body, lcs/datum context to be right for support
        #   this is a hack...
        on_source_body = self.__class__.fork(self, _body= source_body, _lcs=None, _datum_plane=None )
        source_support = source_sketch.Support[0][0]
        if not source_support.isDerivedFrom('App::Plane'): # we assume xy plane
            on_source_body.using( source_support )

        # give a temp-name for if we crash
        # does not recurse on external-geom, because its external-geom is in the same body
        debug(f"   recurse, but no externalgeom... {source_body.Label} ccSKETCH {source_sketch.Label} as TEMP-{name}")
        # pushes context
        on_source_body = on_source_body.carbon_copy_sketch(source_sketch, name="TEMP-" + name, merge=False, origin=True)
        new_sketch = on_source_body._sketch

        # note it if we crash
        self.fork('^root').temp_copies_push_link( new_sketch )

        external_sketches = {} # newname : newsketch
        new_geometry=[]
        for external_geometry in new_sketch.ExternalGeometry:
            # [ (<Part::PartFeature>, ('Edge8', 'Edge7', 'Edge9')) ... ]
            external_sketch, geom_list = external_geometry
            #debug(f"  external: {self.fullname(external_sketch)} extgeom: {geom_list}")

            # build/clone a shape-binder for it's ext-geom-sketch
            if external_sketch.isDerivedFrom('PartDesign::ShapeBinder'):
                if len(external_sketch.Support) > 1 or external_sketch.Support[0][1] != ('',):
                    raise Exception(f"Expected a ShapeBinder to have only 1 support, with no sub geom, saw {self.fullname(external_sketch)}'s {external_sketch.Support}")
                external_sketch = external_sketch.Support[0][0]
            new_external_sketch_name = f"Sketch-externgeom-{external_sketch.Label}-cc"

            if new_external_sketch_name in external_sketches.keys():
                new_ext_sketch = external_sketches[new_external_sketch_name]
            else:
                #debug(f"### Dependant sketches, Make a CC {self.fullname(external_sketch)} --> {new_external_sketch_name}")
                # separate Fluent to isolate the context
                # will recursively cc external geometry sketches!
                f = self.fork()
                f.carbon_copy_sketch(external_sketch, name="TEMP-"+new_external_sketch_name, origin=True, merge=False)
                new_ext_sketch = f._last

                # back to real name, in target-body
                new_ext_sketch.Label = new_external_sketch_name 
                external_sketches[new_external_sketch_name] = new_ext_sketch

            # rebuild a ( new-sketch, [same geom name list] )
            new_geometry.append( (new_ext_sketch, geom_list) )

            new_ext_sketch.ViewObject.Visibility = False
            self.recompute(new_ext_sketch)

        # collect the constraints, because we have to rebuild them
        GeoUndef = Sketcher.Constraint().First # no direct access to GeoEnum::GeoUndef

        new_constraints=[]
        for constraint_i, constraint in enumerate(new_sketch.Constraints):
            # not all are used by each constraint, have to filter
            geomvertex = []
            for idx,pos in (
                    (constraint.First, constraint.FirstPos),
                    (constraint.Second, constraint.SecondPos),
                    (constraint.Third, constraint.ThirdPos),
                    ):
                # Collect up to GeoUndef
                if idx == GeoUndef:
                    break
                geomvertex.append(idx)
                geomvertex.append(pos)

            constraint_info = { 
                'dump' : constraint.dumpContent(),
                'type' : constraint.Type,
                'geomvertex' : geomvertex,
                'properties' : {},
                'expressions' : []
            }
            # misc properties
            #for k in ('Name',): # not accessible: 'LabelPosition'):
            #    constraint_info['properties'][k] = getattr(constraint, k)
            for expression_path,expression in new_sketch.ExpressionEngine:
                # ('Constraints[9]', '<<ss-pentablock>>.end_stock_width')
                # carefully create a new tuple!
                if expression_path.startswith( f"Constraints[{constraint_i}]" ):
                    constraint_info['expressions'].append( (expression_path,expression) )
            new_constraints.append(constraint_info)

        with self.recomputing(False):
            # All the external_sketches should be in the new body
            while new_sketch.ExternalGeometry:
                new_sketch.delExternal(0)

            # Move the new-sketch
            self.temp_copies_pop_link( new_sketch ) # before it leaves the doc
            source_body.removeObject( new_sketch )
            #debug(f"  ## move sketch to {target_body.Label}")
            target_body.addObject( new_sketch )
            new_sketch.FluentOwned = target_body.FluentOwned
            new_sketch.ViewObject.Visibility = False

            self.set_support(new_sketch)

            # Critical: need everybody to know which body they are now in, else can't add geom,etc
            self.recompute()

            # and rename to correct
            new_sketch.Label = name

            #debug(f"###   new geometry fixup {self.fullname(new_sketch)} = {self.fullname(new_geometry[0][0])},{new_geometry[0][1]}")

            for obj,geom_list in new_geometry:
                self.recompute(obj)
                for geom in geom_list:
                    #debug(f"###   addext {self.fullname(obj)} {geom}")
                    new_sketch.addExternal( obj.Name, geom )

            # rebuild constraint list
            # because external constraints can not survive a delExternal/addExternal, nor moving to a new body
            while new_sketch.Constraints:
                new_sketch.delConstraint(0)

            for constraint_i, constinfo in enumerate(new_constraints):
                constraint = Sketcher.Constraint()
                constraint.restoreContent( constinfo['dump'] )
                new_sketch.addConstraint( constraint )

                # and it's expressions
                for expression_path_expression in constinfo['expressions']:
                    #debug(f"### re-add expression {expression_path_expression}")
                    new_sketch.setExpression(*expression_path_expression)
                    self.recompute(new_sketch)
                self.recompute(new_sketch)

        self.recompute() # to catch the target_body, and the source-body where we made new_sketch

        debug(f"  Done ExternalGeom {self.fullname(new_sketch)} {[x.Label for x in external_sketches.values()]}")

        return new_sketch

    def loft(self, sections=None, ct=2, name=None):
        """Loft from current sketch
            to `sections`
            or to last ct-1 sections (i.e. 2 includes current sketch)
        """
        debug(f"loft {name} to { sections or (ct - 1)}")

        if not self._body:
            raise Exception("Expected a current body")
        if not self._sketch:
            raise Exception("Expected a current sketch for loft")

        obj = self.add_object(self._body, 'PartDesign::AdditiveLoft', name)
        obj.Profile = self._sketch

        # `ct` is ignored if `sections` is provided
        if not sections and ct > 1:
            sections = []
            for s in self._body.OutList:
                if not s.isDerivedFrom('Sketcher::SketchObject'):
                    # only sketches
                    continue
                if s == self._sketch:
                    #that was last one
                    break
                sections.append(s)

            if len(sections) < ct - 1:
                raise Exception(f"Could not find ct={ct}-1 sections, saw only {len(sections)} : {[x.Label for x in sections]}")
            # last n
            sections = sections[-(ct-1) : ]
            debug(f"  ct: {len(sections)}/{ct} : {[x.Label for x in sections]}")

        if sections:
            if isinstance(sections,str) or not isinstance(sections, Sequence):
                sections = [sections]
            sections = [ self.find(x) for x in sections ]
            debug(f"  sections: {len(sections)} : {[x.Label for x in sections]}")
            obj.Sections = [ (x,('',)) for x in sections ]

        else:
            raise Exception("Expected sections, or ct>=2")

        self._last = obj

        self.recompute(self._body)

        return self

class FluentPrimitive:
    """primitives like cylinder"""

    def cylinder(self, height, diameter=None, radius=None, name=None):
        """Cylinder, specify a diam or radius"""
        
        debug(f"Cylinder {name} d {diameter} h {height}")

        obj = self.add_object(self._body, "PartDesign::AdditiveCylinder", label=name)

        if diameter:
            if diameter.startswith('='):
                radius = f"=({diameter[1:]})/2"
            else:
                radius = f"={diameter}/2"

        self.set_support(obj)
        self.set_property( obj, 'Height', height )
        self.set_property( obj, 'Radius', radius)

        self._last = obj

        self.recompute(obj)

        return self
    
    def box(self, length, width, height, name=None, from_zero=False):
        """box.
        '^selection` for length/width/height will compute the selections l/w/h
            which will also  reposition the box to be a bounding box
        from_zero=True # will prevent the repositioning for ^selection, and use 0,0,0 as the bounding minimums
            i.e. bounding box from zero
        """
        
        self.recompute()
        debug(f"Box {name}\n\tl={length}\n\tw={width}\n\th={height}")

        delta = None
        if next( (True for x in [ length, width, height ] if '^selection' in x ), False ):
            debug(f"## lwh ^ fixup")
            selection_bb = self._selection[0].Shape.BoundBox
            delta = App.Vector() # serves as flag below
            for selected_obj in self._selection:
                selection_bb.XMin = min( selection_bb.XMin, selected_obj.Shape.BoundBox.XMin )
                selection_bb.YMin = min( selection_bb.YMin, selected_obj.Shape.BoundBox.YMin )
                selection_bb.ZMin = min( selection_bb.ZMin, selected_obj.Shape.BoundBox.ZMin )
                selection_bb.XMax = max( selection_bb.XMax, selected_obj.Shape.BoundBox.XMax )
                selection_bb.YMax = max( selection_bb.YMax, selected_obj.Shape.BoundBox.YMax )
                selection_bb.ZMax = max( selection_bb.ZMax, selected_obj.Shape.BoundBox.ZMax )
            debug(f"  selection_bb: { selection_bb }")

            if '^selection' in length:
                vlength = selection_bb.XMax - (selection_bb.XMin if not from_zero else 0)  # FIXME: make expression based on minx-object
                length = re.sub(r'\^selection',str(vlength), length)

                if not from_zero:
                    delta.x = selection_bb.XMin
            debug(f"#### SW? {width}")
            if '^selection' in width:
                debug(f"#### SW {width}")
                vwidth = selection_bb.YMax - (selection_bb.YMin if not from_zero else 0)  # FIXME: make expression based on minx-object
                debug(f"#### vW {vwidth}")
                width = re.sub(r'\^selection',str(vwidth), width)
                debug(f"#### W {width}")

                if not from_zero:
                    delta.y = selection_bb.YMin
            if '^selection' in height:
                vheight = selection_bb.ZMax - (selection_bb.ZMin if not from_zero else 0)  # FIXME: make expression based on minx-object
                height = re.sub(r'\^selection',str(vheight), height)

                if not from_zero:
                    delta.z = selection_bb.ZMin

        obj = self.add_object(self._body, "PartDesign::AdditiveBox", label=name)

        self.set_support(obj)
        self.recompute()
        # FIXME: some kind of bug.
        #   kind of like the set_transform one,
        #   which I thought was order.
        #   this sometimes ignores the Width expression if it is second
        #   so, make it first
        #   but that doesn't seem reliable
        # Also, have to specify `nodot`, which is odd
        self.set_property( obj, 'Width', width, nodot=True)
        self.set_property( obj, 'Length', length, nodot=True)
        self.set_property( obj, 'Height', height, nodot=True)

        self._last = obj

        self.recompute(obj)

        # position it if using selection
        if delta:
            self.transform(source=obj, translate=delta, rotate=None)
            self.recompute(obj)

        return self
    
class FluentBody(
        FluentBoolean,
        FluentSketch,
        FluentPrimitive
    ):
    """mixin
        and sub language
    """
    UsingMap['PartDesign::Body'] = '_body' # for .using()

    def __init__(self):
        if '_body' not in self._context.keys():
            self.context(_body = None)
        super().__init__()

    def body(self, name=None):
        """Creates a context with the new body
            has its own sub-langauge
        """
        debug(f"Body {name}")

        obj = self.add_object(self._group, "PartDesign::Body", label=name)

        sublang = self.__class__.fork(self, _body = obj, _last=obj )

        return sublang

class FluentBase(
    # FIXME: The idea of a sublang doesn't work this way.
    # Sublang should be based on the current context,
    # e.g. if there is a _body, then you can do body things, else not.
    # and, if you do a .group(), then children of the context hierarchy should be set to None
    # good luck with that.
    # The .fork() mechanism is wrong. Should fork to a sublang based on current-context setting/using.
    # So, make a sublang type hierarchy. and on .body() or using that includes ._body, add the body sublang, etc.
    # i.e. the context hierarchy is the same as the sublang hiearchy... dynamic superclass-mixins? maybe...
    # watch out for the "want to use _something before it was made" situation.
    FluentBody,
    FluentGroup,
    FluentPosition,
    FluentUsing,
    FluentSpreadsheet,
    FluentSelect,
    FluentLink,
    FluentVisual,
    FluentLCS, # lcs first
    FluentDatum,
    FluentConditional,
    FluentExpression,
    FluentUtil,
    FluentSibling
    ):
    """Common stuff for the sub-languages"""

    def __init__(self):
        #debug(f"INIT {self}")
        if '_todo' not in dir(FluentBase):
            # Not a context, but "global", at least to this stack and below
            FluentBase._todo=[]
        super().__init__()

    @class_or_instance_method
    def fullname(self, obj=NoArg):
        # not using self
        if not obj:
            return 'None'
        if obj == NoArg:
            obj = self._last
        if hasattr(obj,'Label'):
            return f"{obj.TypeId}:{obj.Label}({obj.Name})"
        elif hasattr(obj,'TypeId'):
            return f"{obj.TypeId}:{obj})"
        else:
            return f"{obj.__class__.__name__}:{obj}"

    def context(self, **kv):
        # creates getter/setter for each k, 
        # and sets initial value to corresponding v
        # So, this is really "add context"
        # fork() should create the initial context (a copy of previous in stack)

        if '_context' not in dir(self):
            self._context = {}

        #debug(f"Add Context {self.__class__.__name__}")
        def prop(k,v):
            def setter(s,v):
                #if k=='_selection':
                #    debug(f"{k}={self.fullname(v)}",1)
                s._context[k] = v
            # FIXME: the Property is added to the class, but the context is per object
            # so, every Group has ._selection, but no actual entry for it in _context
            # and, ._selection wants to be [] not None
            setattr(self.__class__, k, property( (lambda s: s._context[k] if k in s._context.keys() else None), setter ) )

        for k,v in kv.items():
            # you remember that python doesn't have block context, right?
            # so, force another context for closures
            #debug(f"  context[{k}] = {v}",1)
            if k not in dir(self):
                # only once
                prop(k,v)
            setattr(self,k, v)

    def add_object(self, where, freecad_typeid, label=None, owned=True):
        # owned sets the flag for cleaning
        # freecad_typeid can be the TypeId string or an actual object:
        # allow creation by something else, e.g. pythonobjects, like in the Path workbench

        # try FreeCAD.ActiveDocument.supportedTypes()

        if 'TypeId' in dir(freecad_typeid):
            obj = freecad_typeid
            self.move( obj, where )
        
        else:
            if not isinstance(freecad_typeid,str) :
                raise Exception(f"type-id should be an object, or a string like 'PartDesign::Body', saw {freecad_typeid.__class__}:{freecad_typeid}")

            debug(f"Start {freecad_typeid} '{label}' in {self.fullname(where)}")

            default_name = freecad_typeid.split('::')[-1]
            #debug(f"  default name: {default_name}")

            if where.isDerivedFrom('PartDesign::Body'):
                #debug(f"  add to body {self.fullname(where)}")
                self.recompute(where)
                obj = where.newObject(freecad_typeid,default_name)
            elif where.isDerivedFrom('App::DocumentObjectGroup') or where.isDerivedFrom('App::Part'):
                #debug(f"  add to group {self.fullname(where)}")
                self.recompute(where)
                obj = where.newObject(freecad_typeid,default_name)
            elif where.isDerivedFrom('App::Document'):
                obj = self._doc.addObject(freecad_typeid, default_name) # increments .Name
            else:
                self.describe("Error")
                raise Exception(f"Don't know how to add to {self.fullname(where)}")
        
        # Mark the objects we made, so we can whack them!
        self.own_it(obj, owned)

        self.label_it(obj, label)

        self.recompute(where)

        debug(f"Done {freecad_typeid} '{label}'=={self.fullname(obj)} in {self.fullname(where)}")
        return obj
    
    def own_it(self,obj=None, owned=True, marker_override=None):
        # Mark the objects we made, so we can whack them!
        # cf .marker()
        if obj==None:
            if '_selection' in dir(self):
                obj=self._selection
            if not obj:
                obj = [self._last]
        else:
            obj = [obj]

        for one_obj in obj:
            if FluentOwned not in dir(one_obj):
                one_obj.addProperty('App::PropertyString',FluentOwned,'Fluent','Made by a Fluent sequence from within that group. On next run, will be deleted if in that group. If empty, will not be removed.')
                #debug(f"OWN {one_obj.Label} {one_obj.TypeId} {one_obj.Proxy.__class__ if 'Proxy' in dir(one_obj) else one_obj.__class__}")
            one_obj.FluentOwned = self._root.Name if owned else ''
            if self._markers:
                for amarker in self._markers:
                    if FluentMarker not in dir(one_obj):
                        one_obj.addProperty('App::PropertyStringList',FluentMarker,'Fluent','Made by a Fluent .marker(xxx)')
                    setattr(one_obj, FluentMarker, copy(self._markers))

        return self

    def name(self, *args):
        """
            name( objects, naming )
            name(naming) # for selection | last
            `naming` = "literal" | lambda obj,i: new name...
        """
        objects = None
        if len(args) == 2:
            objects, naming = args
        elif len(args) == 1:
            naming = args
        else:
            raise Exception(f"Expected (objects,naming) or (naming), saw {args}")
        if objects:
            objects = [self.find(x) for x in aslist(objects)]
        objects = objects or self._selection or self._last
        debug(f"Rename {[x.Label for x in objects]}")

        new_name = naming if callable(naming) else (lambda obj,i: naming)
        for i,obj in enumerate(objects):
            self.label_it( obj, new_name(obj,i) )
        
        return self

    def label_it(self, obj, label):
        # prevent dups
        if label:
            # prevent dups
            ct=0
            new_label = label
            while( self._doc.getObjectsByLabel( new_label ) ):
                ct += 1
                new_label = "%s%0.3d" % (label,ct)
            debug(f"  labeled {new_label}")
            obj.Label = new_label

        #debug(f"  {self.fullname(obj)}")

    def recompute(self, obj=None):
        """object recompute, or full document"""

        args=()
        if not obj:
            obj = self._doc
            args = (None,True,True)
        else:
            obj = self.find( obj )

        with self.recomputing(True):
            #debug(f"RECOMPUTE (should? {not self._doc.RecomputesFrozen}) {self.fullname(obj)}", 1)
            obj.recompute(*args)
        let_gui_update(force=obj==self._doc)
        check_watchdog()

        return self

    def find(self, name, document=None, fail=True):
        # implements the x.property... part

        if isinstance(name,str):
            path = name.split('.',1) # instances of splitter, not count of results
            obj = self._find( path.pop(0), document=document, fail=fail)
            if obj:
                path = path[0].split('.') if path else [] # instances of splitter, not count of results
                for p in path:
                    obj = getattr(obj,p)

            return obj
        else:
            return self._find( name, document=document, fail=fail)

    def _find(self, name, document=None, fail=True):
        # find by alreadyobj|^somecontext|label|name
        #   also allow x.property...
        #   context is same name as thing that makes it: e.g. ^lcs, ^body
        # [ objs... ] multiple is possibly by label w/"duplicate labels" preference enabled
        debug(f"  find {name.__class__}:{name}")

        if not isinstance(name,str):
            if name != None and (name.isDerivedFrom('App::DocumentObject') or name.isDerivedFrom('App::Document')):
                return name
            else:
                raise Exception(f"Can only find by str or some freecad object, saw {name.__class__}:{name}")

        # ^ are context keys
        if name.startswith('^'):
            for key in ( "_" + name[1:], name[1:] ):
                if key in self._context.keys():
                    debug(f"## should be {key}={self.fullname(self._context[key])}")
                    return self._context[key]
            poss_keys = [ k[1:] if k.startswith('_') else k for k in self._context.keys()]
            raise Exception(f"Not an allowed context key ('^{name}'), use one of {poss_keys}")

        # by label|name
        document = document or self._doc
        if document != self._doc:
            debug(f'find in {document.Label}')
        objs = document.getObjectsByLabel(name)
        if not objs:
            obj = document.getObject(name)
            objs = [obj] if obj else []
            if fail and not objs:
                raise Exception(f"Did not find any name/label: '{name}' in {document.Label}")
            if not obj:
                return None
            return obj
        else:
            return objs[0]

    def get_document(self,document):
        # return a document for the document file-name
        # extension not required
        # absolute, or relative to this document

        debug(f"Find document {document}")
        if not isinstance(document,str) and document.isDerivedFrom('App::Document'):
            return document

        if not re.search('.FCStd$', document):
            document += '.FCStd'

        if not re.match('/', document):
            document = path.dirname( self._doc.FileName ) + "/" + document
            document = path.realpath(document)

        if not path.exists(document):
            raise Exception(f"Expected document to exist, {orig_name} -> {document}")

        debug(f"  by path {document}")
        document = next((x for x in App.listDocuments().values() if path.realpath(x.FileName) == document),None)
        if document:
            debug(f"Extant {self.fullname(document)}")
            return document
        else:
            debug(f"Load {document}")
            return App.openDocument(document)
        
            
    @contextmanager
    def recomputing(self, onoff):
        """for the block, turn recomputing on/off for the entire document
        """
        was = self._doc.RecomputesFrozen
        self._doc.RecomputesFrozen = not onoff
        try:
            #debug(f"recomputing? {onoff} -> not {self._doc.RecomputesFrozen}")
            yield onoff # don't really care
            #debug(f"<recomputing? {onoff} -> not {self._doc.RecomputesFrozen}")
        finally:
            self._doc.RecomputesFrozen = was

    def move(self, obj, destination):
        """
            Move the object to the destination, 
                if `obj`==None, use _selection | _last
            does not update last

        """
        objs = [self.find(x) for x in aslist(obj)] if obj else (self._selection or self._last)
        destination = self.find(destination)
        debug(f"Move {[x.Label for x in objs]}\n\t-->  {self.fullname(destination)}")
        
        for obj in objs:
            debug(f'  moving {self.fullname(obj)}')

            remove_ct = 0
            # A nesting gives the full inlist sequence: [ grand, parent ]
            # So, remove in reverse order (and a copy to escape changes during removals)
            # somethings, like Path workbench, forgets to set the inlist?
            in_list = reversed(obj.InList) if obj.InList else [ self._doc ]
            for an_owner in in_list:

                if not (
                        an_owner.isDerivedFrom('PartDesign::Body')
                        or an_owner.isDerivedFrom('App::DocumentObjectGroup')
                        or an_owner.isDerivedFrom('App::Part')
                        or an_owner.isDerivedFrom('App::Document')
                        ):
                    raise Exception(f"Can only move from a PartDesign-body, a Group/Part, or Document: we=({self.fullname(obj)}) we seem to be in {[self.fullname(x) for x in obj.InList]}")

                if an_owner == destination:
                    already_in_destination = True
                    continue
                remove_ct += 1
                if an_owner.isDerivedFrom('App::Document'):
                    # shouldn't remove from App::Document, groups seem to do the right thing
                    continue 
                debug(f'  removing from {self.fullname(an_owner)}')
                an_owner.removeObject(obj)
                self.recompute(an_owner)

            # `remove` automatically puts it at Document level, can't add it again

            if remove_ct == 0:
                App.Console.PrintWarning( f"Move was useless, {self.fullname(obj)} already in {self.fullname(destination)}\n" )
            elif not destination.isDerivedFrom('App::Document'):
                debug(f'  moving to {destination.Label}')
                # we don't seem to need to add to the nesting hierarchy, just the destination
                destination.addObject(obj)
            self.recompute()

        return self

    def find_ancestor(self, obj, typeid=None):
        orig_obj = obj
        obj = self.find(obj, fail=False)
        if not obj:
            raise Exception("Can't find ancestor of non-existent obj '{orig_obj}'")
        if typeid:
            return next((x for x in obj.InListRecursive if x.TypeId==typeid),None)

    def throw(self, message=None):
        """Throw an exception to stop things for debugging"""
        raise Exception("User: " + (message if message else "Abort"))

    def todo(self, msg):
        """Record a todo string. Print out later with .todos()"""
        FluentBase._todo.append(msg)
        debug(f"TODO {FluentBase._todo}")
        return self

    def todos(self):
        print(f"## TODOS {'' if FluentBase._todo else 'None'}")
        for x in FluentBase._todo:
            print(x)
        return self

    def marker(self, new_string):
        """set a marker flag on subsequent objects
        usable as .select(marker=yourstring).delete()
        """
        debug(f"Marker {new_string}")

        markers = copy(self._markers)
        markers.append(new_string)
        debug(f"  so _markers : {markers}")
        sublang = self.__class__.fork(self, _markers=markers )

        return sublang

    def delete(self, obj_list=None, filter=None):
        """Delete
            the selection
            or the given list
            optionally filtering
        """

        def still_extant(obj):
            # deleting something, like a body, could delete children (like Origin's)
            # Have to check everything, because there is no test for "is deleted"
            # Also, an obj can appear more than once in OutListRecursive
            try:
                # triggers: <class 'ReferenceError'>: Cannot access attribute 'Name' of deleted object
                # or sometimes: <class 'RuntimeError'>: This object is currently not part of a document
                obj.Name
                return True
            except ReferenceError:
                debug(f"  already gone")
                return False
            except RuntimeError as e:
                if "This object is currently not part of a document" in str(e):
                    debug(f"  already gone")
                    return False
                raise e

        if not obj_list:
            obj_list = self._selection

        to_delete = []

        # Collect them first
        for x in aslist(obj_list):
            if isinstance(x,str):
                x=self.find(x,fail=False)
            if x==None or not still_extant(x):
                debug(f"Gone")
                continue

            if not filter or filter(x):
                #debug(f"path {self._root.Label} -> {x.Label} :  {[ [x.Label for x in apath] for apath in self._root.getPathsByOutList( x )]}")
                debug(f"del {x.Label} if {x.Label} in {[x.Label for x in self.recursiveParents(x)]}")
                # inlistrecursive is EVERY link, so, are we in self._root?
                to_delete.append(x.Name)

        # Now delete known list
        changed = False
        if to_delete:
            with self.recomputing(False):
                for x in to_delete:
                    # debug(f"  remove: #{self.fullname(x)}")
                    try:
                        self._doc.removeObject(x) # doesn't seem to throw
                    except ValueError as e: 
                        if "No document object found with name" in str(e):
                            # this doesn't seem to happen
                            # ok, must have removed it already
                            pass
                        else:
                            raise e

                    changed=True

        if changed:
            self.recompute() # important

        return self

    def recursiveParents(self, obj):
        # tree view parents
        #debug(f"  parents of {obj.Label}")
        sofar = [ x[0] for x in obj.Parents] # parents->[(parent, path)]

        for p in obj.Parents:
            # things inside a body
            #debug(f"    try {p[0].Label}")
            sofar.extend( self.recursiveParents(p[0]) )
        pgroup = obj.getParentGroup() # for body
        if pgroup:
            sofar.append( obj.getParentGroup() )
        #debug(f"  -> {[x.Label for x in sofar]}")
        return sofar

    def xdebug(self, msg): # instead of def debug()
        debug(msg, up=1)
        return self
        
class FluentSubLang:
    # abstract base for sub langs

    def __init__(self, **context):
        # the sublang should do .context( **context) w/any extra keys
        # but super().__init()
        self._describe_indent = 0
        #debug(f"INIT {self.__class__.__name__}")
        super().__init__()

    @class_or_instance_method
    def fork(kls, parent=None, **context):
        """On a class, forks to a sublanguage, for internal use.
        On an object, forks to a sub context
        """
        
        if isinstance(kls,type):
            # class
            debug(f"FORK {parent.__class__.__name__} -> {kls.__name__} w/{context}")
            #debug(f"  <-FROM",1)
            new_context = { **(parent._context), **context, 'context_parent' : parent }
            #debug(f"  newcontext {new_context}")
            sublang = kls( **new_context )
            #debug(f"  iscontext {sublang._context}")
            #sublang.describe("FORK")
            return sublang

        else:
            # instance
            new_context = { **(kls._context), **context, 'context_parent' : parent or kls }
            forked = kls.__class__(**new_context)
            #forked.describe("FORKi")
            return forked

    def forking(self):
        """return a function that will fork each time"""
        new_context = { **self._context, 'context_parent' : self }
        return lambda : self.fork(self, **new_context)

    def describe_lang(self,indent=0):
        print(("  "*indent) + f"Lang {self.__class__.__name__}")
    
    def __getattr__(self,m):
        """Try to help the user realize they are in a sub-langauge
        We only get here if the attribute doesn't exist
        """
        def in_parent(obj,m):
            if 'context_parent' in dir(obj):
                #debug(f"in parent {obj.context_parent}?")
                if m in dir(obj.context_parent):
                    raise AttributeError(f"You are in the {self.__class__.__name__} sub-language, and `.{m}` is in {obj.context_parent.__class__.__name__}.")
                else:
                    in_parent(obj.context_parent,m)
            else:
                return

        in_parent(self,m)
        # If we get this far, we didn't find it
        try:
            self.describe("oops? (see below)")
        except Exception as e:
            App.Console.PrintWarning( f"(and failed during describe with {e})" )
            
        raise AttributeError(f"'{self.__class__.__name__}' object has no attribute '{m}'")

    def describe(self,msg=None,ancestors=False):
        #debug(f"Cont {self._context}")

        if ancestors:
            self.context_parent.describe(msg)
            self._describe_indent = self.context_parent._describe_indent
        else:
            self._describe_indent = 0
            if msg:
                print("## " + msg)
            else:
                print("## Describe:")

        self.describe_lang(self._describe_indent)

        self._describe_indent += 1
        for k in self._context.keys():
            #debug(f"consider {k} ? {k in dir(self)}")
            if k in dir(self):
                if k=='context_parent':
                    continue
                v = getattr(self,k)
                if v != None and not (isinstance(v,list) and len(v) == 0):
                    if isinstance(v,list):
                        print(("  " * (self._describe_indent)) 
                            + f"{k} : { [(x.Label if 'Label' in dir(x) else x) for x in v]}"
                        )
                    else:
                        print(("  " * (self._describe_indent)) + f"{k} : {self.fullname(v)}")

        return self

class SubFluentBody(
    FluentSubLang,
    FluentBase
    ):
    
    def __init__(self, **context):
        #debug(f"INIT {self} w/{context}")
        self.context( **context )
        super().__init__()

PathScripts = LazyLoader("PathScripts", globals(), "PathScripts")
import PathScripts.PathDressupHoldingTags
import PathScripts.PathDressupTagGui
import PathScripts.PathDressupTag
import PathScripts.PathJobGui
import PathScripts.PathPost
import PathScripts.PathPostProcessor
import PathScripts.PathGeom
from BOPTools import SplitAPI
#import PathScripts.PathPost.CommandPathPost

class FluentCamLayout:
    """Stuff to do the layout of a bunch of bodies"""

    def __init__(self):
        super().__init__()

    def in_xy(self, copies=1, flip=False, xy_plane=None, **spread_args):
        # max_width=48*MM_IN, max_x_count=None, dx=0,dy=0, gutter=('0 mm','0 mm'), xy_plane=None, copies=1, flip=False):
        """orient the selection to the global xy plane
            via their sketches plane
            OR to xy_plane
                xy_plane='ab' maps a->x-axis, b->y-axis. e.g. 'yx'->xy rotates
        Makes n `copies`
        `flip` after transforming to sketch plane, rotates 180deg around the given axis, e.g. 'y' or 'Y'
            False to flip none
            a str axis to flip all
            a function(clone,original) 
                returning False to not flip the clone
                returning an axis to flip the clone

        and then spread(**spread_args) out,
        fixme: `flip` also needs to pick which one's to flip
        """

        # FIXME dy,dx should be Quantities at least, Expressions at best

        self.is_selection_all(['PartDesign::Body','Sketcher::SketchObject'], fail=".layout expects Body's (or links to them)")
        if flip and not ( (isinstance(flip,str) and flip in 'xyzXYZ') or callable(flip) ):
            raise Exception(f"Expected `flip` to be callable or one of x y z, saw: {flip}")

        # need some special handling for spread args:
        # various "relative to last spread" args 
        # refer to the context BEFORE we generate the new "links"
        for arg in ['after','stack_after']:
            if arg in spread_args:
                spread_args[arg] = self.find( spread_args[arg] )

        debug(f"in_xy: rotate sketch to xy plane, move to 0,0, copies {copies}")

        # find the sketch, or a boolean-cut/add?
        # get the trans/rotate
        #   detect if 2 sketch faces
        #       only rect other faces for other sides?
        # {'body':body, 'sketch':sketch.Label, 'placement':sketch.Placement, 'translate':sketch.Placement.Base, 'rotate':sketch.Placement.Rotation}
        if xy_plane:
            # explicit plane to use 
            obj_xy=[]
            for body in self._selection:
                rotate = {
                    'XY' : [0.0, 0.0, 0.0], # none
                    'YX' : [00,00,-90],
                    'XZ' : [90,00,00],
                    'ZX' : [90,00,90],
                    'YZ' : [-90,90,00],
                    'ZY' : [00,90,00],
                }[ xy_plane.upper() ]
                if rotate:
                    obj_xy.append( {'body':body, 'xy_plane':xy_plane, 'placement': body.Placement, 'rotate':rotate} )
                else:
                    obj_xy.append( {'body':body, 'xy_plane':xy_plane, 'placement': body.Placement} )
        else:
            obj_xy = [self.cam_find_xy_plane(x) for x in self._selection]

        def ddxy(msg, obj_xy):
            # debug about cam_find_xy_plane() result
            info=[f"# {msg}: cam_find_xy_plane"]
            for x in obj_xy:
                info.append(f"\t{self.fullname(x['body'])}")
                info.extend([ f"\t\t{k} = {x[k]}" for k in x.keys() if k!='body'])
            debug("\n".join(info))
        ddxy('find xy',obj_xy)
        self._selection = []

        # if needs trans/rotate, do it in a link
        #   so that the sketch face is +z
        # needs= each obj_xy that has a rotate/translate.
        # is_in_xy = each obj_xy['body'] that doesn't
        needs_positioning, is_in_xy = self.cam_partition_is_in_xy(obj_xy)
        ddxy('Needs', needs_positioning)

        forked = self.forking()

        def do_flip(original, new_link):
            if flip:
                # we need the axis for flip_rot()
                flip_axis = flip(new_link, original) if callable(flip) else flip
                if flip_axis:
                        self.flip_rot(flip_axis, obj=new_link)
                        self.recompute(new_link)
                        # you would expect new_link.Placement.Base[2] to be the delta-z
                        # which seems to work in the python console, but not during this script
                        debug(f'  flipping {self.fullname(new_link)}')
                        wrong_z = new_link.Shape.BoundBox.ZMin
                        new_link.Placement.move(App.Vector( 0,0, -wrong_z) )

        # add a link for each object to our group
        # we then use all objects in the group to do layout
        debug(f'  is_in_xy {[x.Label for x in is_in_xy]}')
        new_links = []
        for body in is_in_xy:
            for i in range(0,copies):
                new_link = forked()._draft_clone( body, name=body.Label)._last
                do_flip(body, new_link)
                new_links.append(new_link)
        debug(f'  needs_positioning {[x["body"].Label for x in needs_positioning]}')
        for an_obj_xy in needs_positioning:
            for i in range(0,copies):
                new_link = forked()._draft_clone( an_obj_xy['body'], name=f'clone-{an_obj_xy["body"].Label}' )._last
                new_obj_xy = copy( an_obj_xy )
                new_obj_xy['body'] = new_link
                debug(f'  camfix for {new_link.Label}')
                self.cam_fix_xy(new_obj_xy)
                do_flip(an_obj_xy['body'], new_link)
                new_links.append(new_link)

        self.recompute(self._group)

        # fork
        self=self.fork(_selection=new_links, _last=new_links[-1])
        self.describe('forked')

        # Then spread out the objects in xy via bounding box
        debug(f'  to spread: {[x.Label for x in new_links]}\n\t{spread_args}')
        self.spread(**spread_args)

        self.recompute()

        return self

    def flip_rot(self, rotation_axis, obj=None):
        """180deg rotation around an axis
            Operates on obj | _selection | _last
            Does not change selection
        """

        # convert `obj` into list of objects
        if obj:
            if not islisttype(obj):
                objs = [obj]
        else:
            objs = self._selection or [self._last]
            debug(f'# flip_rot {[x.Label for x in objs]}')

        for obj in objs:
            #4x4 rot/trans where 4th column is diag: 0,0,0,1

            # 4th row/col is translate, so identity
            # negate the row
            # don't negate the row for the axis to rotate around
            rot = App.Matrix(
            (1 if rotation_axis in 'Xx' else -1),  0,  0,  0,
            0,  (1 if rotation_axis in 'Yy' else -1),  0,  0,
            0,  0,  (1 if rotation_axis in 'Zz' else -1),  0,
            0,  0,  0,  1
            )
            obj.Placement.Matrix = obj.Placement.Matrix.multiply(rot)

        return self

    def cam_fix_xy(self, obj_xy):
        body = obj_xy['body']
        debug(f'orient in xy {body.Label}')

        # I ferget why this is
        debug(f'  unposition(?) {obj_xy["placement"].Base}')
        body.Placement = obj_xy['placement'].inverse()

        if obj_xy['rotate']:
            debug(f'  to xy from {obj_xy["placement"].Rotation} via {obj_xy["rotate"]}')
            self.transform(source=body, translate=None, rotate=obj_xy['rotate'])
        self.recompute(body)

        # move to bottom=z:0
        # NB: can't use parameteric because ZMin goes to 0 on the next recompute, then to SomeNum, etc
        debug(f'  to 0,0 {obj_xy["placement"].Base}')
        dz = f'{self._shape(body)}.BoundBox.ZMin'
        dz = self.eval_expression(f'=-{dz}', body)
        self.transform(source=body, translate=(
            None,
            None,
            dz
            )
        )
        
    def cam_find_xy_plane(self,body):
        # find the xy-plane of the sketch
        # We'll use the first sketch's support XY
        # return {body:body, translate:xx, rotate:xx} of where it is at
        # FIXME: we could do a smart guess for plywood-type cutouts:
        #   2 of the *Length's should be > 1 of them. the 1 is the normal

        effective_body = self.effective(body)

        sketch = None
        if effective_body.isDerivedFrom('Sketcher::SketchObject'):
            sketch = body
        else:
            if effective_body.Group and effective_body.Group[0].isDerivedFrom('PartDesign::FeatureBase'):
                # a boolean operation looks like this.
                # body->Group[0] is a featurebase clone -> BaseFeature is the actual body
                effective_body = effective_body.Group[0].BaseFeature

        debug(f'  find sketch in body...')
        sketch = next( (x for x in effective_body.OutList if self.effective(x).isDerivedFrom('Sketcher::SketchObject')), None)

        if not sketch:
            raise Exception(f"To find the XY, expected a sketch in {self.fullname(body)}, effective-body {self.fullname(effective_body)}")

        return {'body':body, 'sketch':sketch.Label, 'placement':sketch.Placement, 'translate':sketch.Placement.Base, 'rotate':sketch.Placement.Rotation.getYawPitchRoll()}

    def spread(self, dx=0, dy=0, after=None, stack_after=None, gutter=('1mm','1mm'), max_x_count=None, max_width=48*MM_IN ):
        """spread the selection out in the xy
        dx,dy is the offset of the starting position
        OR `after` is an object to position after (in x direction), taking gutter into account
        OR `stack_after` is an object.XMax to set dx to, taking gutter into account
        x first, up to max_x_count (-1 means no limit) or max_width
        gutter is between, see in_xy()
        Leaves selection
        NB:
            FIXME: App::Links will not tell us their true boundbox
                getLinkedObject(transform=True) gives the original, ignoring our transform
            SO: The source body needs to be a geo-part that is already in the xy plane
        NB:
            in_xy() needs special handling for some args: .find() before it creates a new_selection
        """

        max_width = Units.Quantity(self.eval_expression(max_width)).Value
        debug(f"Spread sel={len(self._selection)} d({dx}, {dy}) max_width {max_width} max_x_count {max_x_count}")

        if (gutter[0]=='0 mm' or gutter[1]=='0 mm') and len( self._selection) > 1:
            raise Exception(f"Unlikely gutter of {gutter}, leaves no room for tool")
        
        x_i,y_i = (0,0)

        if stack_after:
            # in the X direction
            after_obj = self.find(stack_after)
            debug(f'Stackafter {after_obj.Label}')
            dx = after_obj.Shape.BoundBox.XMax + Units.Quantity(gutter[0]).getValueAs('mm')

        x,y=(dx,dy)

        if after:
            # in the X direction
            after_obj = self.find(after)
            debug(f'After {after_obj.Label}')
            x += after_obj.Shape.BoundBox.XMax + Units.Quantity(gutter[0]).getValueAs('mm')
            y += after_obj.Shape.BoundBox.YMin

        if max_x_count == None:
            max_x_count = -1

        y_max_height = 0
        for body_i, body in enumerate(self._selection):
            #debug(f"Body [{body_i}]%{max_x_count != -1 and body_i % max_x_count} {self.fullname(body)}")
            debug(f"Body [{body_i}] {body.Label}")
            # sometimes, e.g. a Path Model ("clone") hides .Shape, but does have it
            bb = body.LinkedObject.Shape.BoundBox if body.isDerivedFrom('App::Link') else body.Shape.BoundBox
            debug(f'  bb {bb}')
            y_max_height = max(bb.YLength,y_max_height)
            debug(f"  height {bb.YLength} max {y_max_height}")
            let_gui_update(True)

            # up one row (count)?
            if max_x_count != -1 and body_i % max_x_count == (max_x_count-1):
                # move to next Y row
                #debug(f"  Next row max_count {body_i} vs {max_x_count}")
                y = y_max_height + Units.Quantity(gutter[1]).getValueAs('mm')
                y_max_height = 0
                x = dx

            # Up one row (width)?
            new_max_x = x + bb.XLength
            debug(f"MARK {new_max_x} > {max_width}"); let_gui_update(True)
            if new_max_x > max_width:
                debug("MARK"); let_gui_update(True)
                # move to next Y row
                debug(f"  ### Next row max_width {new_max_x} vs {max_width}")
                let_gui_update(True)
                y += y_max_height + Units.Quantity(gutter[1]).getValueAs('mm') # fixme, as user units?
                y_max_height = bb.YLength
                x = dx
                new_max_x = x + bb.XLength
            debug("MARK"); let_gui_update(True)
            debug(f"Checked max-x, next row")
            let_gui_update(True)

            # place the current body's botleft, at x,y
            m_x = x - bb.XMin
            m_y = y - bb.YMin

            debug(f'  body to move ({m_x}, {m_y}) {self.fullname(body)}')
            let_gui_update(True)
            body.Placement.move(Base.Vector(m_x,m_y,0))
            self.recompute(body)
            let_gui_update(True)

            # updated bb
            bb = body.LinkedObject.Shape.BoundBox if body.isDerivedFrom('App::Link') else body.Shape.BoundBox
            #debug(f"  0@ ({bb.XMin}, {bb.YMin})")

            # next position
            try:
                debug(f'  next x {x} +xmax {bb.XMax} + gutter {Units.Quantity(gutter[0])}')
                x = new_max_x + Units.Quantity(gutter[0]).getValueAs('mm')
            except ValueError as e:
                App.Console.PrintWarning( f"\nFor gutter[0] = '{gutter[0]}'. Maybe: Expressions are unitless, with suffix unit e.g. '1/4 *2 +4 in'.")
                raise e

            """ Need to do at top of loop for `after` to work
            # up one row (count)?
            if max_x_count != -1 and body_i % max_x_count == (max_x_count-1):
                # move to next Y row
                #debug(f"  Next row max_count {body_i} vs {max_x_count}")
                y = y_max_height + Units.Quantity(gutter[1]).getValueAs('mm')
                y_max_height = 0
                x = dx
            """

        debug("MARK exit"); let_gui_update(True)
        return self

    def cam_partition_is_in_xy(self,objs_xy):
        # need is the objs_xy that have a rotate or translate
        # ok is the objs_xy['body'] that don't
        need=[]
        ok=[]
        for info in objs_xy:
            if info['rotate'] != (0.0,0.0,0.0,0.0) or info['translate'][2] != 0.0:
                need.append(info)
            else:
                ok.append(info['body'])

        return (need,ok)

class FluentCam:
    """mixin
    """

    def __init__(self):
        super().__init__()

    def cam(self):
        """Starts a CAM fluent sub-language
            I.e. stuff for the Path workbench
            Slightly different model: trying .select() based
        """
        debug(f"Start .cam...")

        sublang = FluentCam.SubFluentCam.fork(self, _job=None)

        return sublang


    class FluentCamExport:
        # STL etc

        def export_stl(self, filename=None, bodies=None, union=False):
            """ bodies | selection
                filename is 
                    'some/' -> doc-relative subfolder, name using body.Label
                    '/some/' -> abs folder, name using body.label
                    '/some' -> abs name
                    None -> doc-relative, name using body.Label
                `union`=True puts them in one file, False in separate
            """

            def one_filename(body,i):
                rez = ""
                if filename:
                    # strip the .stl, because we might want to -{i}.stl
                    rez = re.sub('\.stl','',filename)

                    if filename.startswith("/"):
                        # some kind of absolute
                        rez = filename
                    else:
                        # some kind of relative
                        rez = os.path.dirname( self._doc.FileName) + "/" + filename
                    if rez.endswith('/'):
                        # subfolder
                        rez = rez + body.Label
                    else:
                        # actual name
                        pass
                else:
                    rez = os.path.dirname( self._doc.FileName) + "/" + body.Label

                if i != None:
                    rez += f'-{i:03}'

                # and put .stl back
                rez = rez + '.stl'
                return rez

            if bodies:
                bodies = [self.find(x) for x in bodies]
            else:
                bodies = self._selection

            if union:
                debug(f'Export stl: {[x.Label for x in bodies]}')
                afilename = one_filename(bodies[0],None)
                Mesh.export(bodies,afilename)
                debug(f'Wrote {afilename}')
            else:
                debug(f'Export stl: {bodies}')

                for i,body in enumerate(bodies):
                    afilename = one_filename(body,i)
                    Mesh.export([body],afilename)
                    debug(f'Wrote {afilename}')

    class SubFluentCam(
        FluentSubLang,
        FluentBase,
        FluentCamLayout,
        FluentCamExport
        ):
        
        def __init__(self, **context):
            #debug(f"CAM INIT")
            new_context = { '_path_template' : None, '_job' : None, **context}
            self.context( **new_context )
            #debug(f"CAM w/{self._context}")
            super().__init__()

        def job(self, name=None, template=None, onejob=False, model=None, stock={}, setup={}, **job_properties):
            """Construct a job for each model.
                Model is model || _selection || _body
                `stock` is either a Shape for the .Stock, or dict-of-properties for the default .Stock box
                You probably want to set properties like:
                    job = stock : default for extend model's bound box
                    stock={
                        ExtXneg=..., ExtXpos=...
                        ExtYneg=..., ExtYpos=...
                        ExtZpos=..., # but not ExtZneg
                    }
                    setup={
                        'somepropof-SetupSheet' : value | '=expr'
                            and `expr` can have something like '#SetupSheet'
                            which substitutes thisjob.SetupSheet.Name
                            e.g. see SafeHeightExpression
                    }
                Actually, a job per model, because some Path stuff really only can deal with 1 model at a time
                `onejob` can force a single job for all the models
                So, you probably want to do multi-model jobs in a group
                `template` loads a saved path-template file (path : job export template)
                Will use the current path-template(), if any
            """

            #debug(f"  ## stock {self.fullname(stock)}")
            template = template or self._path_template
            if template:
                if not template.startswith('/'):
                    template = path.dirname( self._doc.FileName ) + "/" + template
                    template = path.realpath(template)
                #debug(f"## template '{template}'")
            debug(f"CamJob.job {name} w/{template if template else 'No Template'}")

                
            bodies = (aslist(model) or self._selection or [self._body])
            self.elements_not_none(bodies,f'Did not expect a None. Did you select something first? {bodies}')

            models = self.local_only( [self.find(x) for x in bodies] )
            debug(f"  models {[x.Label for x in models]}")

            if isinstance(stock,str):
                orig_stock = stock
                stock = self.find(stock)
                if not stock:
                    self.describe("State:")
                    raise Exception(f'Expected `stock` to be something but saw None, you gave {self.fullname(orig_stock)}')

            self._selection = []

            self.recompute()

            new_selection = []


            def create_job( model_list, fullname ):
                # model_and_safe_list = [ [model,safe_model], ...]
                #   where safe_model might be a list of wires
                # FIXME: won't work unless recomputing is true
                with self.recomputing(True):
                    # from ./src/Mod/Path/PathScripts/PathJobGui.py
                    #obj = PathScripts.PathJobGui.Create([one_model], None, openTaskPanel=False) # works but?
                    # But, Seems more canonical:
                    # This can crash, leaving un-owned, un-nested crap at the top level
                    debug(f'  create job from: {[x.Label for x in model_list]}')
                    obj = PathScripts.PathJob.Create('Job', model_list, template)
                    obj.ViewObject.Proxy = PathScripts.PathJobGui.ViewProvider(obj.ViewObject)
                    obj.ViewObject.addExtension("Gui::ViewProviderGroupExtensionPython")

                if not obj:
                    raise Exception("Failed to create job, error above?")

                self.add_object( self._group, obj, fullname)
                debug(f"  Created {self.fullname(obj)}")

                # mark things that were made as owned
                for x in self.job_children(obj):
                    self.own_it(x)

                # set extra properties
                for k,v in job_properties.items():
                    self.set_property(obj,k,v)

                # set .Stock or stock properties
                if isinstance(stock,dict):
                    # properties for .Stock.xxx
                    debug(f'  Stock is Model {f"with properties {stock}" if stock else ""}')
                    for k,v in stock.items():
                        self.set_property(obj.Stock,k,v)
                elif self.effective(stock).isDerivedFrom('PartDesign::Body'):
                    # actual object
                    debug(f'  Stock is actual {self.fullname(stock)}')
                    obj.Stock = Draft.make_clone(stock, delta=None, forcedraft=False)
                    self.own_it(obj.Stock)
                    self.label_it(obj.Stock, f'Stock-{stock.Label}')
                    #self.transparency(80, obj)
                    self.hide(obj.Stock)
                else:
                    raise Exception(f"Expected a dict of Stock.x properties, or a PartDesign::Body as the stock shape, saw {self.fullname(stock)}")

                if setup:
                    # for SetupSheet
                    debug(f'  setup={setup}')
                    setup_sheet = obj.SetupSheet
                    self.recompute(setup_sheet)
                    for k,v in setup.items():
                        # do spreadsheet substitution (if expression)
                        (is_expr,v) = self._expression( v )

                        # special fixups: #X is thisjob.X.Name
                        if is_expr and '#' in v:
                            debug(f'    Has # {k}{v}')
                            v = re.sub(r'#(\w+)', (lambda m: getattr(obj, m.group(1)).Name), v)
                            debug(f'            ->{v}')

                        if not k.endswith('Expression'):
                            # then keep it as an string-as-expression, put '=' back for set_property

                            v = '=' + v

                        self.set_property(setup_sheet,k,v)

                    self.recompute(obj)
                    debug(f'  recomputed job. stock is {obj.Stock.Label}')

                # dressups fail to set the rapid speeds
                # so we add move & job speeds before any "Real" operation as a hack
                # fixme: this is useless/redundant if there are no dressups
                # xopensbp_post.py now forces a VS at every 'Tool', so this isn't necessary for shopbot
                self.add_path_speeds(obj) # fixme: tool is just defaulting to [0]

                self._last = obj
                self._job = obj
                new_selection.append(obj)
                self.recompute()

            if onejob:
                create_job( models, fullname=name )
            else:
                for one_model in models:
                    fullname = f"{name}-{self.effective(one_model).Label}" if len(models)>1 else name
                    create_job([one_model], fullname=fullname )
            debug(f'DONE JOB')

            # FIXME: Should start a new sublang "cam-job", "job" isn't in that!
            self._selection = new_selection

            return self

        def position_at(self, x=None,y=None,z=None, jobs=None, rapid=False, zfirst=True, name=None):
            """
            Generate GCode to position at xyz
            Foreach jobs or _selection or _last
            Does Z first! then xy.
            x,y,z can be:
                None    : don't change that axis
                '=...'  : an expression, but it is resolved immediately, so not parametric
                lambda job : return value # calculate a value
                    e.g. for @.SetupSheet.SafeHeightExpression
            """

            def resolve(job, op, v):
                if callable(v):
                    try:
                        v = v(job,op)
                        debug(f'  value from fn->{v}')
                    except RuntimeError as e:
                        App.Console.PrintError( f"For callable {v.__class__} {v}\n" )
                        raise e
                        
                elif isinstance(v,str) and v.startswith('='):
                    (is_expr, cleaned) = self._expression(v)   
                    debug(f'  going to eval {v} -> {cleaned}')
                    v = job.evalExpression( cleaned )

                if v==None:
                    debug(f'  v==None')
                    return None

                try:
                    v = Units.Quantity(v)
                except TypeError as e:
                    App.Console.PrintError( f"For resolved value {v.__class__} {v}\n" )
                    raise e

                debug(f'  return as unitless {v}')
                if v.Unit == Units.Quantity('0').Unit:
                    return format(v.Value, '.4f')
                else:
                    return format(v.getValueAs('mm').Value, '.4f')

            gcode = 'G0' if rapid else 'G1'

            #debug(f'position_at {gcode} for { [x.Label for x in (jobs or self._selection or [self._last])] }')
            def _position_at(job,op):
                # callback for custom_gcode, per job, for first op
                # closes on arg z x y

                debug(f'position_at {gcode} for {job.Label}')

                gcode_list = []

                debug(f'  # for z {z}')
                rz = resolve(job, op, z)
                if rz and zfirst:
                    gcode_list.append( f'{gcode} Z{rz}' )
                
                xy = []
                debug(f'  # for x {x}')
                rx = resolve(job, op, x)
                if rx:
                    xy.append(f'X{rx}')

                debug(f'  # for y {y}')
                ry = resolve(job, op, y)
                if ry:
                    xy.append(f'Y{ry}')
                if xy:
                    gcode_list.append(f'{gcode} {" ".join(xy)}')

                if rz and not zfirst:
                    gcode_list.append( f'{gcode} Z{rz}' )

                if gcode_list:
                    debug(f'  gcode {gcode_list}')
                    return gcode_list
                else:
                    debug(f'  xyz were all None')
                    return None

            return self.custom_gcode(_position_at, name=name or 'position_at')

        def job_children(self, job=None):
            # Generator for objects owned by a Job
            # PathJobGui.Create makes a bunch of distinct objects
            # i.e. a hierarchical structure
            # Find them
            # Can't go full-recursive, because, for example, a Link has an OutList...
            # FIXME: the GUI knows when to stop recursing... well, maybe not
            if not job:
                job = self._job

            for sub_obj in job.OutList:
                #debug(f"Consider {sub_obj.Label} {sub_obj.TypeId} {sub_obj.Proxy.__class__ if 'Proxy' in dir(sub_obj) else sub_obj.__class__}")
                yield sub_obj

                # Groups seem to be fair game
                if sub_obj.isDerivedFrom('App::DocumentObjectGroup'):
                    for group_obj in sub_obj.OutList:
                        #debug(f"  Consider {sub_obj.Label} {sub_obj.TypeId} {sub_obj.Proxy.__class__ if 'Proxy' in dir(sub_obj) else sub_obj.__class__}")
                        yield group_obj

                        # The Tools group, has ToolControllers, which have a list of ToolBit's
                        if group_obj.isDerivedFrom('Path::FeaturePython') and isinstance(group_obj.Proxy, PathScripts.PathToolController.ToolController):
                            for tool_obj in group_obj.OutList:
                                yield tool_obj

        def contour(self, name=None, offset=0.0, ramp=False, **properties): # def profile
            """Create a contour (aka profile) operation (per job):
                The selection is which jobs, or self._job
                selection becomes all the contours
                Negative `offset` to inset
                FIXME: add a rough+finish option
                generating 2 operations, with offset="0.1mm" for first
            """
            # see ./src/Mod/Path/PathScripts/PathProfile.py
            # (...obj=selection?..) defaults to all models

            jobs = self._selection or [self._job]
            # FIXME: needs to be a Job sublang...

            # FIXME: probably should do tags by default?

            if not name:
                name='Contour'

            new_selection = []
            for one_job in jobs:

                # thanks to Carlo Dormeletti (and Russ4262)
                res = PathScripts.PathProfileGui.Command.res
                obj = res.objFactory(res.name, obj=None, parentJob=one_job)
                obj.ViewObject.Proxy = PathScripts.PathOpGui.ViewProvider(obj.ViewObject, res)
                obj.ViewObject.Proxy.deleteOnReject = False

                if offset:
                    self.set_property(obj, 'OffsetExtra', offset)

                default_properties = {
                    'StepDown' : '=OpToolDiameter/2',
                    #'StepOver' = 40,
                    'FinalDepth' : '-0.03mm'
                }
                # set extra properties
                for k,v in { **default_properties, **properties}.items():
                    self.set_property(obj,k,v, substitutions = {r'#(\w+)' : (lambda m: getattr(one_job, m.group(1)).Name)})

                fullname = f"{name}-{one_job.Label}" if len(jobs)>1 else name
                self.label_it(obj, fullname)
                self.own_it(obj)
                debug(f"  Created {self.fullname(obj)}")

                self.show(one_job.Model.Group[0])

                self.recompute()

                if ramp:
                    # FIXME: need args, maybe copy common ones, at least
                    obj = self.fork().ramp( operation=obj, name=None )._last
                new_selection.append(obj)
                self._last = obj
                
                debug(f"DONE contour {obj.Label}")

            self._selection = new_selection
            return self

        def tags(self, tag_count=None, name=None, **tag_args):
            """Create a tags (with ramp) dressup operation (per profile):
                The selection|last is the tags
                Ramp: Tag on top of Ramp
                `tag_args` are K=v for each property of a tag. immediate properties so far...
            """

            # FIXME: should set some default values, taking from prefs right now

            profiles = self._selection or [self._last]
            debug(f'Tag {[x.Label for x in profiles]}')

            # need the job/groups to be up to date
            self.recompute()

            # convert jobs to operations # FIXME: use for other path dressups
            _profiles=[]
            skipped=[]
            for one_profile in profiles:
                if isinstance(one_profile.Proxy, PathScripts.PathJob.ObjectJob):
                    # `one_profile` is actual a Job
                    self.recompute(one_profile.Operations)
                    debug(f'  Job -> ops... {[x.Label for x in one_profile.Operations.Group]}')
                    #debug(f'  Job -> ops... {one_profile.Operations.OutListRecursive()}')
                    for op in one_profile.Operations.Group: #one_profile.Operations.Group:
                        debug(f'  consider {op.Label} {op.Proxy.__class__.__mro__}')
                        debug(f'  {[x.Label for x in one_profile.Operations.getPathsByOutList(op)[0] ]}')

                        # FIXME: in here, I think because we just made the job & added a ramp, .Group has grand-child entries
                        # because of weird bug, double check our "group depth"
                        # FIXME: this doesn't even work all the time, I got an op inside a ramp via a certain job

                        if len( one_profile.Operations.getPathsByOutList(op)[0] ) > 2:
                            debug(f'  # didnt fool me: {op.Label}')
                            continue

                        # we can work on Ramp, a dressup
                        if isinstance(op.Proxy, PathScripts.PathDressupRampEntry.ObjectDressup):
                            _profiles.append(op)
                        # or a path op
                        elif isinstance(op.Proxy, PathScripts.PathAreaOp.ObjectOp):
                            _profiles.append(op)
                        else:
                            # I think we want to skip over inappropriate things, aka "Put tags on useful things"
                            #   especially relevant as I add a Speed op when I make a job
                            #raise Exception(f"Expected all job.Operations to be Ramp or 'PathAreaOp' (aka regular op), saw {one_profile.Label} [ {op.Label} ] == {op.Proxy.__class__.__mro__}")
                            skipped.append(op)
                    
                else:
                    # pass through other things
                    # FIXME: could do the inner block above to sanity check
                    _profiles.append(one_profile)
            profiles=_profiles
            debug(f'  targets from job: {[x.Label for x in profiles]}')
            if skipped:
                App.Console.PrintError(f'  skipped from job: {[x.Label for x in skipped]}\n')
                
            new_selection = []
            for one_profile in profiles:
                # Both a ramp and tag, Ramp must be first, then Tag on top of that

                self.recompute(one_profile)

                #if isinstance(one_profile.Proxy, PathScripts.PathDressupRampEntry.ObjectDressup):
                if not 'Active' in dir(one_profile):
                    # already are a ramp or other dressup, we can't do a ramp
                    debug(f"  can't add our own ramp, because the target (one_profile.Label) is some dressup: {one_profile.Proxy.__class__}")
                    ramp = one_profile
                else:
                    fullname = 'Ramp' + (f"-{name}" if name else '') + (f"-{one_profile.Label}" if (not name or len(profiles)>1) else '')
                    ramp = self._ramp(one_profile, name=fullname)

                job = PathScripts.PathUtils.findParentJob(one_profile)

                # Tag
                # from src/Mod/Path/PathScripts/PathDressupTagGui.py .Create
                #with self.recomputing(True):

                # So, like the Gui version:
                # FIXME: see ./BUG-magic-DRESSUP-prefix
                tag = FreeCAD.ActiveDocument.addObject("Path::FeaturePython", 'DressupTag') # omg, magic check for /^Dressup/
                dbo = PathScripts.PathDressupHoldingTags.ObjectTagDressup(tag, ramp)
                job.Proxy.addOperation(tag)
                self.recompute() # critical, more than the `tag`...
                dbo.setup(tag, True)
                tag.ViewObject.Proxy = PathScripts.PathDressupTagGui.PathDressupTagViewProvider(tag.ViewObject)
                tag.ViewObject.Proxy.deleteOnReject = False

                if tag_count:
                    if not tag.Proxy.generateTags(tag, tag_count):
                        tag.Proxy.execute(tag)

                for k,v in tag_args.items():
                    setattr(tag,k,v)

                fullname = 'Tag' + (f"-{name}" if name else '') + (f"-{one_profile.Label}" if (not name or len(profiles)>1) else '')
                self.label_it(tag, fullname)
                self.own_it(tag)
                debug(f"  Created {self.fullname(tag)}")

                self.recompute(job)

                self._last = tag
                new_selection.append(tag)
                debug(f"DONE tags {tag.Label}")

            self.recompute()
            
            # FIXME: seems to leave the Operations sub-folder un-recomputed
            debug(f"DONE tag/ramp {name}")
            return self.fork( _selection=new_selection )

        def dogbone(self, name=None, ops=None, long_edge=False,short_edge=False,horizontal=False,vertical=False, side='Left'):
            """Create a dogbone/tbone dressup operation:
                For each ops | selection
                selection becomes the dogbone
                Chose None or 1 of the dogbone directions/edges
                No direction == dogbone
                `side`='Left' works for pockets. use 'Right' for contour
                FreeCAD seems to calculate the "bones" based on the path,
                    So a profile/countour is the operation you want to select
                works on a Ramp, weirdly enough
                    but not a Tag
            """

            profiles = aslist(ops) or self._selection
            debug(f'Dogbone {len(profiles)} {x.Label for x in profiles}')

            # need the job/groups to be up to date
            self.recompute()

            t_bone_style = (
                # None==dogbone
                (horizontal and PathScripts.PathDressupDogbone.Style.Tbone_H)
                or (vertical and PathScripts.PathDressupDogbone.Style.Tbone_V)
                or (long_edge and PathScripts.PathDressupDogbone.Style.Tbone_L)
                or (short_edge and PathScripts.PathDressupDogbone.Style.Tbone_S)
            )

            new_selection = []
            for one_profile in profiles:
                self.recompute(one_profile)

                fullname = (t_bone_style if t_bone_style else 'DogBone') + (f"-{name}" if name else '') + (f"-{one_profile.Label}" if (not name or len(profiles)>1) else '')
                job = PathScripts.PathUtils.findParentJob(one_profile)
                debug(f"Dogbone in {job.Label},\n\tagainst {one_profile.Label},\n\tmake {fullname}")
                self.recompute(job)

                # from src/Mod/Path/PathScripts/PathDressupDogbone.py .Create

                with self.recomputing(False):
                    # So, like the Gui version:
                    bone = FreeCAD.ActiveDocument.addObject("Path::FeaturePython", 'DogBone')
                    self.own_it(bone)
                    dbo = PathScripts.PathDressupDogbone.ObjectDressup(bone, one_profile)
                    job.Proxy.addOperation(bone)
                    # I thougth we must NOT recompute here, but it did work. put a with no-recompute around this anyway
                    bone.ViewObject.Proxy = PathScripts.PathDressupDogbone.ViewProviderDressup(bone.ViewObject)
                    bone.ViewObject.Proxy.deleteOnReject = False
                    dbo.setup(bone, True)

                    bone.Style = t_bone_style or 'Dogbone'
                    bone.Side = side # FIXME: depends on cut direction

                self.label_it(bone, fullname)
                self.own_it(bone)
                debug(f"  Created {self.fullname(bone)}")

                self.recompute(job)

                self._last = bone
                new_selection.append(bone)
                debug(f"DONE dogbone {bone.Label}")

            self.recompute()
            
            debug(f"DONE dogbones {name}")
            return self.fork(_selection = new_selection )

        def path_boundary(self, delta_boundary, name=None, inside=True):
            """Aka path-dressup-boundary
                Using the model bound box,
                Constrain to inside|outside the +- xyz
                    in units:
                    meant to mimic task-form order, also bound-box:
                    ( dx-min, dy-min, dz-min, dy-max, dy-max, dz-max )
                The selection is which operation
                    known to work on path-pocket
                selection becomes none
            """

            profiles = self._selection

            # need the job/groups to be up to date
            self.recompute()

            dnames = ('ExtXneg','ExtYneg','ExtZneg','ExtXpos','ExtYpos','ExtZpos')
            if len(delta_boundary) != 6:
                raise Exception(f"Need a delta-boundary tuple w/ {dnames}, saw {delta_boundary}")
            for i,dname in enumerate(dnames):
                self.must_have_units(delta_boundary[i], f'delta_boundary[{i}] ({dname})')

            new_selection = []
            for one_profile in profiles:
                self.recompute(one_profile)

                fullname = 'Boundary' + (f"-{name}" if name else '') + (f"-{one_profile.Label}" if len(profiles)>1 else '')
                job = PathScripts.PathUtils.findParentJob(one_profile)
                debug(f"In {job.Label}, against {one_profile.Label}, make {fullname}")

                # src/Mod/Path/PathScripts/PathDressupPathBoundaryGui.py
                # src/Mod/Path/PathScripts/PathDressupPathBoundary.py

                # So, like the Gui version:
                obj = PathScripts.PathDressupPathBoundary.Create(one_profile, fullname)
                obj.ViewObject.Proxy = PathScripts.PathDressupPathBoundaryGui.DressupPathBoundaryViewProvider(obj.ViewObject)
                # hmmm..
                obj.Base.ViewObject.Visibility = False
                obj.Stock.ViewObject.Visibility = False

                self.own_it(obj)
                self.own_it(obj.Stock)

                """
                # from the PathDressupPathBoundary.py Create and typical pattern
                obj = FreeCAD.ActiveDocument.addObject("Path::FeaturePython", 'Boundary')
                obj.Proxy = PathScripts.PathDressupPathBoundary.DressupPathBoundary(obj, one_profile, job)

                job.Proxy.addOperation(obj, one_profile, True)
                # nb: must NOT recompute here
                obj.ViewObject.Proxy = PathScripts.PathDressupPathBoundaryGui.DressupPathBoundaryViewProvider(obj.ViewObject)
                obj.ViewObject.Proxy.deleteOnReject = False
                """

                obj.Inside = inside

                # boundary props are actually for the .Stock
                # and it can't do expressions
                # FIXME: find other .Stock setattr() and factor this
                actual_operation = self.effective_operation( one_profile )
                debug(f'  against {actual_operation}')
                for i,dname in enumerate(dnames):
                    v = self.eval_expression( delta_boundary[i], against=actual_operation )

                    debug(f"  BOUNDARY {dname}={v}")
                    setattr( obj.Stock, dname, v )

                self.label_it(obj, fullname)
                debug(f"  Created {self.fullname(obj)}")

                self.hide(one_profile)

                self.recompute(job)

                self._last = obj
                debug(f"DONE Boundary {obj.Label}")

            self.recompute()
            
            # FIXME: seems to leave the Operations sub-folder un-recomputed
            self._selection = []
            debug(f"DONE Boundary {name}")
            return self

        def effective_operation(self, apparent_op ):
            #a hack to find the actual operation, even if "this" is a dressup
            # sometimes we need the actual op to do expressions etc

            if not hasattr(apparent_op, "Base"):
                raise Exception(f"Expected an apparent_op to have a .Base: {self.fullname(apparent_op)}")

            if apparent_op.Base==None or islisttype(apparent_op.Base):
                # Op's have a shape,face list (or None)
                return apparent_op
            elif hasattr(apparent_op.Base, "TypeId") and apparent_op.Base.isDerivedFrom('Path::FeaturePython'):
                # Dressup's have a Base which is a .TypeId == 'Path::FeaturePython'
                # dressups can nest
                return self.effective_operation(apparent_op.Base)
            else:
                raise Exception(f"Expected an apparent_op.Base to be a list or Path::FeaturePython saw: {self.fullname(apparent_op)}.Base={apparent_op.Base}")
                
        def ramp( self, operation=None, name=None, **properties ):
            """Operates on
               operation | _last # FIXME: _last is always singular, I think we want _selection to catch a multiple-op
            path ramp entry dressup
            This hides the op
            """

            if operation==None:
                operation = self._last
            if isinstance(operation, str):
                operation=self.find(operation)

            ramp = self._ramp( operation, name, **properties)
            self._last = ramp
            self._selection = [ ramp ]
            return self

        def _ramp( self, operation, name=None, **properties ):
            # path ramp entry dressup
            #   return the ramp
            # works on operations like profile/pocket
            #   does not work on dressup DogBone nor tag, maybe not any dressup
            #   Likely workaround:
            #       do the other dressup AFTER ramp, seems to do the right thing
            #   possible workaround is to:
            #       make base operation like profile
            #       do ramp dressup ON that
            #       do dogbone on base operation again, -> clones the base-op
            #           which repeats the base-op, sadly
            # Apply ramp  before a tag() dressup
            # so, like contour().ramp().tag()

            # FIXME: 
            # ramp inserts an egregious G0 X0 Y0 Zsafeheight, which isn't fatal
            # but then "ramps" to the first op's location, G0 X Y Z, which is nasty
            #   and that Z is the clearanceheight, whichis doubly nasty
            # it should z-up to safe, move to X Y, then do the Z
            #   and the Z should be safeheight
            # hack is insert a goto, to drop the 1st G0 (0,0) 

            job = PathScripts.PathUtils.findParentJob(operation)
            debug(f"Going to ramp last op in job {job.Label}...")
            let_gui_update(force=True)

            # this is what most ops have as Create():
            ramp = self._doc.addObject("Path::FeaturePython", "RampEntryDressup") # /Dressup/ required
            dbo = PathScripts.PathDressupRampEntry.ObjectDressup(ramp)
            ramp.Base = operation
            job.Proxy.addOperation(ramp, operation.Name)
            dbo.setup(ramp)
            self.own_it(ramp)
            ramp.ViewObject.Proxy = PathScripts.PathDressupRampEntry.ViewProviderDressup(ramp.ViewObject)
            ramp.ViewObject.Proxy.deleteOnReject = False

            # set extra properties
            for k,v in properties.items():
                self.set_property(ramp,k,v)

            debug(f"DONE ramp {ramp.Label}")
            let_gui_update(force=True)
            self.recompute(job)

            self.label_it(ramp, name)

            self.hide(operation)
            let_gui_update(force=True)

            return ramp

        def guiselecthack(self, body, element, msg=None):
            # because I don't know a way of figuring out the actual "path" (for boolean op bodies)
            # So, by examination, the "Boolean00N"
            # element is like ".Face1"
            if msg:
                debug(f"  ({msg}: select {self.fullname(body)}.{element})")
            #Gui.Selection.addSelection(self.effective(body), f"Boolean0{job_i+15}{element}")
            Gui.Selection.addSelection(body, element)

        # FIXME: tool choice, step-over, etc. Template?

        def face_i(self, body, face, bycommon=False):
            #The [i] of the face in the body: "global" designation like Face1
            # nb, Face1 == [0]
            # Uses hashCode() ==
            # `bycommon` causes use of .common(face).Area !=0 or isCoplanar( face ) instead
            for i,x in enumerate(body.Shape.Faces):
                if bycommon:
                    # found a case where the face is 0 area, though .Area gave 1E-5
                    # Assume it is part of env
                    bb = x.BoundBox
                    if bb.XLength==0 or bb.YLength==0 or bb.ZLength==0:
                        return i
                    # sometimes common, sometimes coplanar, don't know why
                    if x.isCoplanar(face):
                        return i
                    if x.common( face ).Area > 1e-9:
                        return i
                else:
                    if x.hashCode() == face.hashCode():
                        #debug(f"   ...f actual {i}")
                        return i
            return None

        def edge_i(self, body, edge):
            #The [i] of the edge in the body: "global" designation like Edge1
            # nb, Edge1 == [0]
            # Uses hashCode() ==
            for i,x in enumerate(body.Shape.Edges):
                if x.hashCode() == edge.hashCode():
                    #debug(f"   ...f actual {i}")
                    return i
            return None

        def wire_i(self, body, wire):
            #The [i] of the wire in the body: "global" designation like Wire1
            # nb, Wire1 == [0]
            # Uses hashCode() ==
            for i,x in enumerate(body.Shape.Wires):
                if x.hashCode() == wire.hashCode():
                    #debug(f"   ...f actual {i}")
                    return i
            return None

        def balloon2d(self, offset):
            """foreach selection|last,
            Using the footprint, enlarge (2doffset) by the offset, and extrude to z-height
            Leaves the new shape as the selection
            `offset`
                a float, mm
                Don't forget a little "error" for things like tool-diameter rounding: e.g. + 1e-5
            """

            sofar = []

            self.recompute()
            for i,obj in enumerate(self._selection or [self._last]):
                debug(f"Balloon {self.fullname(obj)}")

                label = f"balloon-{obj.Label or obj.Name}"

                # an envelope (no holes)

                # guesses depth of min, I think
                depthparams = PathScripts.PathUtils.guessDepths(obj.Shape)
                # actual envelope
                contour_env = PathScripts.PathUtils.getEnvelope(
                    partshape=obj.Shape, subshape=None, depthparams=depthparams
                )

                for f in contour_env.Faces:
                    # only z-normal, flat faces
                    if not PathScripts.PathGeom.isHorizontal( f ):
                        continue
                    # skip below the model's top
                    if abs(f.BoundBox.ZMax - obj.Shape.BoundBox.ZMax) > 1e-5:
                        continue

                    #debug_part_show(f, f'debug env-face {obj.Label}', True)
                    #let_gui_update(force=True)

                    # found the face we want
                    # we extrude it down to z

                    f = f.fuse(
                        f.makeOffset2D(
                            # offset on outer by tool radius
                            offset,
                            0, # arcs, 1=tangent,2=intersection
                            True, # new faces
                            True, # closed
                            True # collective offset (?)
                        )) 
                    f = f.removeSplitter()
                    self.recompute()
                    let_gui_update(force=True)

                    # giving us ... the envelope ("blank"). 
                    # fixme: consider "conical" type solids. We want the projection of the xy plane of the "largest" area
                    solid = f.extrude( App.Vector(0,0,-obj.Shape.BoundBox.ZMax) )
                    
                    #sofar.append( self.add_object(self._group, obj, label=label) )
                    doc_obj = Part.show( solid, )
                    doc_obj.Label = label
                    self.recompute()
                    self.own_it(doc_obj)

                    if self._group:
                        self.move( doc_obj, self._group )

                    sofar.append( doc_obj )

                    let_gui_update(force=True)
                    continue

            if sofar:
                debug(f"ballooned {sofar.count}")
                self._selection = sofar
            else:
                debug(f"ballooned NONE")

            return self

        # def profile3d... FIXME:
        # like pocket3d. profile the outside of something, but it can be "stepped"
        # do the same "positives" strategy, but, the positives are used to find a zmin
        # and the actual model is sliced at each zmin, and each of those model-layers is profiled separately
        # `tag` would be hard to do if the last "step" is very-short

        def pocket3d(self, name=None, offset=None, autoExtension=0, clearance_args={},profile_args={}, debug_till=None): # FIXME: really pocket2.5D
            """For each selected job (or last job),
                a python implementation of 3d-pocket, aka waterline
                    ramp entry added automatically, of course
                generates multiple operations, ~ 1/pocket-step
                `offset` - for larger (make room for a tab), + for smaller
                `autoExtension` : 
                    0 for no attempt, 
                    # extension strategies can cause a slow Path generation:
                    # extension strategies can create operations with no effective cutting (G0 $safeheight)
                    # extension strategies can crash (e.g. offset2d for "full balloon")
                    1 for "full balloon" half-diameter-offset-enlargement strategy
                    2 for piece-wise-collar strategy (borken)
                fixme: add choice of clearance (any Path Pocket, or Adaptive, or even contour?)
                Assumes no overhangs, probably does something horrible if there is any
                Any property of the op:
                `clearance_args` is for the clearance step, a Path Pocket operation (e.g. zigzag)
                        https://wiki.freecad.org/Path_Pocket_Shape
                    =None will skip clearance step, doing a profile-cut-out
                    'OffsetPattern' : 'ZigZag',
                    'StartDepth' : '=OpStockZMax' if waterline==1 else top_face.BoundBox.ZMax,
                    'FinalDepth' : cut_depth,
                    'StepOver' : default seems to be 90 as int %
                    # nb: offset to allow a profile after zig-zag-clearance
                    'ExtraOffset' : offset
                `profile_args` is for the profile finish step
                To add a dogbone, and tags, try something like:
                    .select(within='^job.Operations.Group', match='Ramp-WL-offset')
                    .tags()
                    .select(within='^job.Operations.Group', match='Tag-Ramp-WL-offset') # fixme: annoying
                    .dogbone()
                FIXME: check that cut-depth is ok with tool: CuttingEdgeHeight, otherwise error
            """
            # FIXME: actually pocket2.5d

            # some defaults now, so we have them?

            #offset is .ExtraOffset. + means inside, - outside
            if offset != None:
                self.must_have_units(offset, 'argument `offset`')

            #rough_offset = "1mm" if clearance_args.get('OffsetPattern',None) != 'Offset' else '0mm' # mm for zigzag clearing

            # debug: if set to a string, will cut short the algorithm at that step (for each job)
            #       (this ref list is in reverse order)
            debug_till = debug_till or (
                #None, # don't debug stop
                #'recurse-cut-one' # & waterline==
                    # if doing 1st pocket-step, but no more, maybe a slice problem:
                #'move-down-lowers' # the negative, for next down: the slice-plane
                # 'move-down-slices'
                #'move-down-cut-plane' # stops at first plane down
                #'move-down-cut-planes' # doesn't stop, shows all
                    #'cut_1st_pocket' 
                    #'cut_1st_all_faces' 
                #'cut_1st_pocket' # do cut-pocket for the pocket
                #'cut_1st_next_face' # next step down (for this pocket)
                #'cut_1st_step_faces' # all bottoms of pockets
                    #'cut_1st_top_face' # the geometry at the top of the pocket-step
                #'cut-step-1' # actual 'cut-step-{n}'
                #*[ f"cut-step-{n}" for n in range(2,50) ], # could be more
                #'cut-step-1'
                #'cut-one-show-collar' # & waterline==
                #'cuts_as_positives'
                #'env-solid' 
                #'env-faces' 
                #'contour_env' 
                )

            # gui selection is for debugging
            Gui.Selection.clearSelection()

            def debug_part_show(facedgetc, name=None, select=False, transparent=False):
                # like a face or edge or something
                # transparent is True or a integer-percent
                debug('@@ ' + name, 1)
                if facedgetc == None:
                    debug('  @@ is None')
                    return

                obj = Part.show(facedgetc,)
                if name:
                    obj.Label = name
                self._markers.append('debug_part_show')
                self.own_it(obj)
                self._markers.pop()
                if select:
                    Gui.Selection.addSelection(obj)
                if transparent != False:
                    obj.ViewObject.Transparency = 80 if transparent==True else transparent
                    self.recompute(obj)

            def tool(job_member, tool_number=None):
                # util: get the tool from the parent job from some job-member
                # NB: it's the Tool "group" object. Things like Diameter are in tool(..).Tool
                job = PathScripts.PathUtils.findParentJob(job_member)
                if tool_number == None:
                    tool = job.Tools.Group[0]
                else:
                    tool = next((x for x in job.Tools.Group if x.ToolNumber == tool_number), None)

                return tool

            def project_envelope_to_solid(model_i, model):
                # give us a "blank" that envelopes the model
                #   a projection in z of the footprint
                # and for autoExtension==2, a "collar" outside the shape (like a contour/profile)
                #   for auto "extension"

                # guesses depth of min, I think
                depthparams = PathScripts.PathUtils.guessDepths(model.Shape)

                # Extrude the model-object's Z footprint from z-max to z-min. a "blank"
                # NB: env is not existing faces, but new ones from zmin extruded to zmax
                contour_env = PathScripts.PathUtils.getEnvelope(
                    partshape=model.Shape, subshape=None, depthparams=depthparams
                )
                if debug_till=='contour_env':
                    debug_part_show(contour_env, f'debug contour_env ', True, transparent=True)
                    return (None,None)

                solid=None
                extension_collar = None

                # Find the (first) top face (fixme: couldn't we sort by z?)
                # "first" should be ok, because we did an envelope, so nothing above
                for f in contour_env.Faces:
                    # only z-normal, flat faces
                    if not PathScripts.PathGeom.isHorizontal( f ):
                        continue
                    # skip below the model's top
                    if abs(f.BoundBox.ZMax - model.Shape.BoundBox.ZMax) > 1e-5:
                        continue

                    if debug_till=='env-faces':
                        debug_part_show(f, f'debug env-face {model.Label}', True)
                        continue

                    # found the face we want
                    # we extrude it down to z

                    if autoExtension==1:
                        self.recompute()
                        let_gui_update(force=True)
                        f = f.fuse(
                            f.makeOffset2D(
                                # offset on outer by tool radius
                                ( tool(model).Tool.Diameter.Value / 2 + 1e-5 ), # slop for float-precision
                                0, # arcs, 1=tangent,2=intersection
                                True, # new faces
                                True, # closed
                                True # collective offset (?)
                            )) 
                        f = f.removeSplitter()
                        self.recompute()
                        let_gui_update(force=True)

                        # giving us ... the envelope ("blank"). 
                        # fixme: consider "conical" type solids. We want the projection of the xy plane of the "largest" area
                        solid = f.extrude( App.Vector(0,0,-model.Shape.BoundBox.ZMax) )

                    elif autoExtension==2:
                        # We need a collar'd envelop for later, for our "autoExtension=2" trick

                        collar_face = f.makeOffset2D(
                            # offset on outer by tool radius
                            ( tool(model).Tool.Diameter.Value / 2 + 1e-5 ), # slop for float-precision
                            0, # arcs, 1=tangent,2=intersection
                            True, # new faces
                            True, # closed
                            True # collective offset (?)
                        )
                        collar_face = collar_face.cut(f, 1e-5) # just the outer
                        # with the model-object but smaller
                        collar_face = collar_face.fuse(
                            f.makeOffset2D(
                                # offset on inner by a little to overlap
                                - 1e-5 , # slop for float-precision
                                0, # arcs, 1=tangent,2=intersection
                                True, # new faces
                                True, # closed
                                True # collective offset (?)
                            )
                        ).removeSplitter()
                        # now we have a "collar" that overlaps the model, and is larger by 1/2 tool
                        extension_collar = collar_face.extrude( App.Vector(0,0,-model.Shape.BoundBox.ZMax) )
                        #debug_part_show( extension_collar,'x')
                        #raise Exception("bob")

                        # don't need collar if it is too small (arbitrary small volume)
                        if extension_collar.Volume < tool(model).Tool.Diameter.Value / 2:
                            extension_collar = None
                        if debug_till=='env-solid':
                            debug(f"### collar_scale_factor {collar_scale_factor}")
                            debug(f"### collar face is larger by {(max(collar_face.BoundBox.XLength,collar_face.BoundBox.YLength ) - f_max_dimension)/2} * 2")
                            debug_part_show(extension_collar, f'debug extension_collar {model.Label}', True, transparent=True)

                    elif isinstance(autoExtension, list):
                        # The list has corresponding ballooned shapes [model_i]
                        balloon = autoExtension[model_i]
                        if debug_till=='env-solid':
                            debug(f"### Balloon[{model_i}] {balloon.Label}")
                            Gui.Selection.addSelection(balloon)
                        solid = balloon.Shape

                    else:
                        solid = f.extrude( App.Vector(0,0,-model.Shape.BoundBox.ZMax) )
                        extension_collar = None # not doing autoExtension=2

                    if debug_till=='env-solid':
                        debug_part_show(solid, f'debug env-solid {model.Label}', True, transparent=True)
                    return (solid, extension_collar)

                if debug_till=='env-faces':
                    debug("### end env-faces")
                    return (None,None)

                raise Exception(f"Expected to find a horiz face as model ({model.Label}) zmax ({model.Shape.BoundBox.ZMax}), try debug_till='env-faces' or 'env-solid' (during {self.fullname(model)}")
                    # fixme: maybe I should just show the debug shapes?

            def cut_multiple_pockets(job, model, envelope_collar, cut_shapes, water_line=1 ):
                # recursed as we go deeper into each cut-shape
                # keeps track of the waterline depth, for naming and debugging
                # envelope_collar is to be unioned with each level-of-cut for auto "extension"
                debug(f"Cut pockets at wl:{water_line} ct:{len(cut_shapes)} in {model.Label}")

                # Do each piece (recursively)
                # fixme: for label/debugging keep track of hierarchical positive name: 1.1.3.1
                for positive_i, one_positive in enumerate(cut_shapes):
                    if debug_till and water_line > 100: # get runaway on some debug-till mess ups
                        raise Exception("too far")
                    if one_positive.Volume <= 1e-6: # maybe any bb height/widt/length < 1e-6
                        # we'll call that 0
                        continue
                    cut_one_pocket(job, model, envelope_collar, one_positive, water_line=water_line)

            def union_if_intersect(a,b):
                # both must be Shapes
                # returns `a` if no intersect
                # FIXME: use the `tolerances` thing?
                debug(f"## start proximity")
                s = time.monotonic()

                if b == None:
                    debug(f"## not extended { time.monotonic() - s }")
                    return a
                try:
                    com = a.common(b) # can fail
                except TypeError as e:
                    if 'PyCXX: Error creating object of type' in str(e):
                        debug(f"## not extended { time.monotonic() - s }")
                        debug_part_show(a,"union_if_intersect fail: a")
                        debug_part_show(b,"union_if_intersect fail: b")
                        #return a
                    raise e

                if not com.isNull() and com.Volume > 1e-5:
                    debug(f"## extended! { time.monotonic() - s }")
                    possible = a.copy().fuse(b).removeSplitter()
                    #debug_part_show(possible,"possible")
                    return possible
                    #except RuntimeError as e:
                    #    if 'Multi fuse failed' in str(e):
                    #        debug_part_show(a,"union_if_intersect fail: a")
                    #        debug_part_show(b,"union_if_intersect fail: b")
                    #    raise e
                else:
                    debug(f"## not extended { time.monotonic() - s }")
                    return a

            def cut_one_pocket(job, model, envelope_collar, positive, water_line):    
                # generate the cut operation for the 1st depth
                # then recurse on remaining space below (if any)
                # envelope_collar is to be unioned with each level-of-cut for auto "extension"
                debug(f'  cut_one_pocket: cut a 3d pocket for cut-step-{water_line}: {model.Label}')

                # The actual thing to face cut is:
                # union with the collar (if it intersects)
                # to get extension
                positive_with_extension = union_if_intersect( positive, envelope_collar ) if autoExtension==2 else positive
                if debug_till == 'cut-one-show-collar' and water_line==1:
                    debug_part_show(positive, f"debug cut_one_pocket[{water_line}] positive {model.Label}")
                    debug_part_show(envelope_collar, f"debug cut_one_pocket[{water_line}] envelope_collar {model.Label}")
                    debug_part_show(positive_with_extension, f"debug cut_one_pocket[{water_line}] w/extension {model.Label}")
                    return

                if positive_with_extension.isNull():
                    debug_part_show(positive, f"debug cut_one_pocket positive {model.Label}")
                    debug_part_show(envelope_collar, f"debug cut_one_pocket envelope_collar {model.Label}")
                    debug_part_show(positive_with_extension, f"debug cut_one_pocket w/extension {model.Label}")
                    raise Exception("Unexpected null shape `cut_one_pocket w/extension` for the current positive [{water_line}]")

                cut_depth = cut_1st_depth(job, model, positive_with_extension, water_line)

                debug(f'  cuts_as_positives: cut_depth={cut_depth}')
                if debug_till in [ f'cut-step-{water_line}']:
                    return

                if cut_depth == None:
                    return

                # moving down a step could hit top of a wall, thus multiple shapes
                new_cut_shapes, new_lower_collar = move_down_positive(envelope_collar, positive, cut_depth, model)
                if debug_till and new_cut_shapes == None: # if we cut short in there....
                    return

                if not new_cut_shapes:
                    return

                if debug_till == 'recurse-cut-one' and water_line==1: #  and str(model.Label).endswith('30-'):
                    for x in new_cut_shapes:
                        debug_part_show(x,f"recurse-cut-one new[{water_line+1}] {model.Label}")
                    debug_part_show(new_lower_collar,f"recurse-cut-one lower_collar[{water_line+1}] {model.Label}")
                    raise Exception('x')
                    return
                    
                # recurse on anything left
                cut_multiple_pockets(job, model, new_lower_collar, new_cut_shapes, water_line+1)
                debug(f"  done waterline {water_line}")

            def move_down_positive(envelope_collar, positive, previous_depth, model):
                # remove the top of the positive (down to previous_depth)
                # which could give multiple solids
                # don't go below model_Zmin
                # envelope_collar is to be unioned with each level-of-cut for auto "extension"

                if debug_till and previous_depth > 100:
                    raise Exception("too far")

                # plane bigger than any envelope, envelope_collar might be None
                plane_mbb = (envelope_collar if envelope_collar else positive).BoundBox

                # need the cut plane
                # Make it bigger than the model's bounding box, so we don't get "rounding" errors where it touches boundaries
                #   0.1 slop is arbitrary
                plane_pos = App.Vector( plane_mbb.Center.x - (plane_mbb.XLength + 0.1)/2, plane_mbb.Center.y - (plane_mbb.YLength + 0.1) /2, previous_depth )
                cut_plane = Part.makePlane(plane_mbb.XLength + 0.1, plane_mbb.YLength + 0.1, plane_pos ) # in xy plane
                if debug_till=='move-down-cut-plane':
                    debug_part_show(cut_plane, f'debug move-down-cut-plane {model.Label}', True)
                    return (None,None)
                if debug_till=='move-down-cut-planes':
                    debug_part_show(cut_plane, f'debug move-down-cut-planes {model.Label}', True)

                # slice to a compound Shape: does not include the plane
                # single object (Shape) with multiple pieces: .childShapes
                compound_positives = SplitAPI.slice(positive.removeSplitter(), [cut_plane], 'Standard', tolerance=1e-5)
                envelope_collar_pieces = SplitAPI.slice( envelope_collar, [cut_plane], 'Standard', tolerance=1e-5) if envelope_collar else None
                #if envelope_collar_pieces.ShapeType == 'Compound':
                #    debug(f"env_col is compound")
                #    debug_part_show(envelope_collar_pieces,"x")
                #    raise Exception("BOB")
                debug(f"Sliced to {compound_positives}")
                if debug_till == 'move-down-slices':
                    debug_part_show(cut_plane, f'debug move-down-cut-planes {model.Label}', True)
                    debug_part_show(compound_positives, f'debug move-down-slices {model.Label}', True)
                    debug_part_show(envelope_collar_pieces, f'debug envelope_collar_pieces {model.Label}', True)
                    return (None,None)

                # only keep the lower solids, and envelope_collar_pieces
                # children whose tops are at the slice (+slop)

                def _lowers(compound):
                    if compound.ShapeType == 'Compound':
                        return [x for x in compound.childShapes() if abs(x.BoundBox.ZMax - cut_plane.BoundBox.ZMax) <= 1e-5]
                    else:
                        return []

                lowers =  _lowers(compound_positives)
                debug(f"### envelope_collar_pieces? {not not envelope_collar_pieces}")
                lower_collar =  _lowers(envelope_collar_pieces) if envelope_collar_pieces else []
                debug(f"### lower_collar {lower_collar}")

                if debug_till == 'move-down-lowers':
                    debug_part_show(envelope_collar_pieces, f'debug envelope_collar_pieces {model.Label}', True)
                    debug_part_show(cut_plane, f'debug move-down-cut-planes {model.Label}', True)
                    debug_part_show(compound_positives, f'debug compound_positives {model.Label}', True)
                    for x in lowers:
                        debug_part_show(x, f'debug move-down-lowers {model.Label}', True)

                    for x in lower_collar:
                        debug_part_show(x, f'debug lower_collar {model.Label}', True)

                    # the "others" that aren't lower
                    for x in compound_positives.childShapes():
                        if abs(x.BoundBox.ZMax - cut_plane.BoundBox.ZMax) > 1e-5:
                            debug_part_show(x, f'debug move-down-_others {model.Label}', True)
                    return (None,None)

                if len(lower_collar) == 1:
                    lower_collar = lower_collar[0]
                elif len(lower_collar) == 0:
                    lower_collar = None
                else:
                    debug_part_show(envelope_collar_pieces, f'debug envelope_collar_pieces {model.Label}')
                    debug_part_show(cut_plane, f'debug move-down-cut-planes {model.Label}', True)
                    for x in lower_collar:
                        debug_part_show(x, f'debug lower_collar {model.Label}', True)
                    raise Exception(f"didn't expect more than 1 lower collar saw {len(lower_collar)}")

                return (lowers, lower_collar )
            
            def job_positives_group(job):
                # a place to put positives for a pocket's geometry
                group_name = f"_positives-{job.Label}"

                if poss := self._doc.getObjectsByLabel(group_name):
                    group = poss[0]
                else:
                   group = self.add_object(self._group, "App::DocumentObjectGroup", label=group_name) 

                # each positive will be hidden as it is made
                self.hide(group)
                return group

            def cut_1st_depth(job, model, positive, waterline):
                # pocket, using the top face's wire to the first depth

                # "top" is higher z
                horiz_faces = ( x for x in positive.Faces if PathScripts.PathGeom.isHorizontal(x) )
                top_down = sorted( horiz_faces, key = (lambda f: f.BoundBox.ZMax), reverse=True )
                if not top_down:
                    debug_part_show(positive,"positive")
                    raise Exception(f"No horizontal faces in the positive (try debug_till='cuts_as_positives') in {self.fullname(model)}")

                if debug_till=='cut_1st_all_faces':
                    for i,x in enumerate(top_down):
                        debug_part_show(x, f'debug cut_1st_all_faces @{waterline} [{i}] {model.Label}', True)
                    raise Exception('x')
                    return

                # this is the pocket "geometry"
                # the projection of the whole pocket size
                # depth is the next face down (`step_faces`)
                top_face = top_down.pop(0)
                # need a document object later
                top_face_part = self.part_show( top_face, group=job_positives_group(job), name=f"{model.Label}-cut-depth-{waterline}-" )
                let_gui_update(force=True)
                if debug_till=='cut_1st_top_face' and waterline==2: # the geometry, not the depth!
                    Gui.Selection.addSelection(top_face_part)
                    return
                else:
                    self.hide(top_face_part)

                # limit to faces not at zmax
                # (zmin face is gone)
                step_faces = ( x for x in top_down if abs(x.BoundBox.ZMax - model.Shape.BoundBox.ZMax) > 1e-5 )

                if debug_till=='cut_1st_step_faces':
                    #debug(f'# Positive ZMax {model.Shape.BoundBox.ZMax}')
                    for i,x in enumerate(step_faces):
                        #debug(f'# {model.Label} face=[{i}] ZMax {x.BoundBox.ZMax}')
                        debug_part_show(x, f'debug cut_1st_step_faces [{i}] {model.Label}', True)
                    return

                # there may be other faces at first_step.zmax, 
                # but only that 1st depth is important
                # not the list of faces
                first_step = next( step_faces, None )
                if not first_step:
                    raise Exception(f"No horizontal/non-zmax faces in the positive (try debug_till='cuts_as_positives') in {self.fullname(model)}")
                if debug_till=='cut_1st_next_face': # 1st depth
                    debug_part_show(first_step, f'debug cut_1st_next_face[{waterline}] {model.Label}', True)
                    return

                cut_depth = first_step.BoundBox.ZMax
                if abs(cut_depth - model.Shape.BoundBox.ZMin) < 1e-5:
                    cut_depth -= 0.03 # mm, over cut the bottom. FIXME: this should be a parameter

                debug(f"  cut {top_face.BoundBox.ZMax} .. {cut_depth}")
                # clearance needs to take out the inner stuff
                # before "profile", but the op does it backwards
                # so do 2 myself
                # fixme: This isn't quite right...?

                # fixme: I'm getting an apparent noop clearance
                # e.g. one single hole all the way through
                # but I don't see what to test for

                # def cut_pocket(self, name, job, body=None, face_list=None, **cut_args):

                # leave rough_offset during clearance, then inner-profile to cleanup

                debug(f'# Going to clearance cut_pocket({top_face_part}) {top_face.BoundBox.ZMax}..{cut_depth}')
                let_gui_update(force=True)

                common_args = {
                    'StepDown' : '=OpToolDiameter/2',
                    'StartDepth' : '=OpStockZMax' if waterline==1 else top_face.BoundBox.ZMax,
                    'FinalDepth' : cut_depth, # override for onion, etc. includes overcut
                }

                if clearance_args and clearance_args['CutMode']=='Adaptive':
                    # had problems
                        # only did first step-down
                        # failed to cut through slots whose width == bit-diameter
                    # want adaptive for "big" holes
                    adaptive_args = copy(clearance_args)
                    del adaptive_args['CutMode']
                    self.mill_face_adaptive(
                        job, 
                        model=top_face_part,
                        name=f'{(name + "-") if name else ""}WL-clearance-{top_face_part.Label}',
                        base=None,
                        **{
                            # for rough cut, then clean the profile, StockToLeave>0, FinishingProfile=True, and a FinalDepth
                            #'StockToLeave' : offset, # make this explicit: self.modify_expression(offset, post = ('+',rough_offset) ),
                            'FinishingProfile' : False, # True leaves 0.1mm, then profiles it with better "Tolerance"
                            'ForceInsideOut': True,
                            'OperationType' : 'Clearing', # vs Profiling
                            'Side' : 'Inside',
                            **common_args,

                            # overrides
                            **adaptive_args
                        }
                    )

                elif clearance_args != None:
                    debug(f'# doing clearance {top_face.BoundBox.ZMax}..{cut_depth}')
                    try:
                        clearance_obj = self.cut_pocket(
                            f'{(name + "-") if name else ""}WL-clearance-{top_face_part.Label}',
                            job,
                            body=top_face_part, 
                            face_list=['Face1'], # always Face1 (the only face)
                            **{
                                # defaults
                                'OffsetPattern' : 'ZigZag', # fixme: probably default to Offset pattern
                                'ExtraOffset' : offset, # make this explicit: self.modify_expression(offset, post = ('+',rough_offset) ),
                                **common_args,

                                # overrides
                                **clearance_args
                            }
                        )
                    except Exception as e:
                        if 'no X,Y path' in str(e):
                            # that's ok, probably because of rough_offset
                            # see if we can just profile it next
                            debug(f"  Couldn't do clearance (no path), so just do profile. {e}")
                        else:
                            raise e

                if offset:
                    # the profile clean up (or actual profile), if we have any offset-mm (or no clearance args)
                    # (this forks, so the selection is lost)
                    debug(f'  ## Finishing profile')
                    let_gui_update(force=True)
                    offset_obj = self.cut_pocket(
                        f'{(name + "-") if name else ""}WL-offset-{top_face_part.Label}',
                        job,
                        body=top_face_part, 
                        face_list=['Face1'], # always Face1 (the only face)
                        **{
                            # defaults
                            'OffsetPattern' : 'Profile',
                            'ExtraOffset' : offset if offset else None,
                            **common_args,

                            # overrides
                            **profile_args
                        }
                    )
                    let_gui_update(force=True)

                # FIXME: if there where no clearance_args (means profile-cut-out), then do .tags()
                
                if debug_till=='cut_1st_pocket':
                    self.recompute()
                    Gui.Selection.addSelection( job.Operations.OutList[-1])
                    return

                return cut_depth

            ## main

            jobs = self._selection or [self._job]
            debug(f' Pocket3d {len(jobs)} w/{"" if clearance_args!=None else "o"} clearance')
            for job_i,job in enumerate(jobs):
                debug(f"waterline job {job_i+1}/{len(jobs)} {job.Label}")

                # we only deal with one model per job
                # (this also filters out our synthesized geometries):
                # It would be nice if we could add our water-line models to the job
                # But, that would break contour, which takes geometry=None implies "outline of only 1 Model"
                model = self.first_model(job, debug_till)

                # Want a solid that encompasses the actual model
                env_solid, env_collar = project_envelope_to_solid(job_i, model)
                if debug_till in ['env-faces','env-solid']:
                    continue
                #debug(f"env solid {env_solid}")

                # FIXME: "positive" should be "negative" wording
                # and the "pockets" as positives (shapes):
                # this is the full positive.
                # might be discontious pieces (commonly is): .childShapes
                compound_pieces = env_solid.cut( model.Shape, 1e-9 ).removeSplitter() # regular boolean cut: envelope -> positives

                # flatten
                nested_pieces = [c.childShapes() for c in compound_pieces.Compounds ]
                cuts_as_positives = [element for sublist in nested_pieces for element in sublist]
                if debug_till == 'cuts_as_positives':
                    debug_part_show(compound_pieces, f'debug {debug_till} {model.Label}', True)
                    for i,x in enumerate(cuts_as_positives):
                        debug_part_show(x, f'debug {debug_till}-cut[{i}] {model.Label}', True)
                    continue

                # recursively cut each `cuts_as_positives` into deeper pieces
                # first one should use start-depth=stock
                cut_multiple_pockets(job, model, env_collar, cuts_as_positives )

            self.recompute()
            return self

        def modify_expression(self, base_expression, post=None):
            # given a base_expression of Value (number or like '0mm'), 
            #   or expression (like '=...')
            #   tolerates None
            # add a post operation
            #   post=($operator, $expr)
            #   so, like: =$base_expression $operator $expr
            
            #('' if isinstance(offset,str) and offset.startswith('=') else '=') + f"{offset if offset else ''}+{rough_offset}",

            new_expr = ''
            # need to make a "=..."? (i.e. doesn't start with '=')
            if not (isinstance(base_expression,str) and base_expression.startswith('=')):
                new_expr += '='
            if base_expression:
                new_expr += str(base_expression)
            new_expr += f" {post[0]} {post[1]}"
            return new_expr

        def first_model(self, job, show_model=False):
            # find the first model, with some extra behavior
            # `show_model` is for debug: un-hides it
            models = job.Model.Group
            if len(models) > 1:
                raise Exception(f"Not implemented: more than 1 model per job {self.fullname(job)}")

            model = models[0] # links/clones have .Shape, just hidden
            if show_model:
                self.show(model) # debug
            debug(f"  Model {self.fullname(model)}")
            return model

        def pocket(self, jobs=None, name=None, body=None, **cut_args):
            # one pocket
            # uses jobs | _selection | _last
            # uses the body or Model[0] top-face, or `face_list`
            # See cut_pocket() for details
            # OffsetPattern : 'Profile' will do an inner-profile not pocket
            debug(f"  Pocket job {self.fullname(jobs)} {cut_args}")

            if jobs == None:
                jobs = self._job or self._selection or [self._last]
            elif not islisttype(jobs):
                jobs = [jobs]
            jobs = [self._find(j) for j in jobs]

            new_selection = []
            for job in jobs:

                pocket = self.cut_pocket(
                    name, # f'{name}{"-" if name else ""}{top_face_part.Label}',
                    job,
                    body=body, 
                    #face_list=['Face1'], # always Face1
                    **{
                        # defaults
                        'OffsetPattern' : 'ZigZag',
                        #'StartDepth' : top_face.BoundBox.ZMax,
                        #'FinalDepth' : cut_depth,
                        #'ExtraOffset' : self.modify_expression(offset, post = ('+',rough_offset) )
                        # overrides
                        **cut_args,
                    }
                )

                self.recompute(job)
                
                new_selection.append(pocket)

            # FIXME: fork
            if new_selection:
                self._selection = new_selection
            return self

        def cut_pocket(self, name, job, body=None, ramp=True, face_list=None, **cut_args):
            # one pocket, with ramp
            # face_list is the things to cut down to(?)
            #   [(<Part::PartFeature> #Model#, ('Face7',))] # ignores body
            #   [ ( face, 'Face7' ) ] # ignores body
            #   [ 'Face3' ] # uses `body`
            #   '^selection` # which should be a list of the above
            #   defaults to body's top_face
            # `body` must be a Document object, with .Shape.Faces
            #   usually a Model object in the Job
            #   defaults to Job.Model[0]
            # `ramp`==True ads a ramp onto it
            # You can override any of the typical properties, e.g.
            #   { 
            #       'StartDepth' : blah blah, # defaults to op's default which is stock.zmax
            #       'FinalDepth' : blah blah, # defaults to op's default which is model.zmin
            #       'StepOver' : blah blah, # defaults to op's default (around 20 %?)
            #       'OffsetPattern' : blah blah, # defaults to ZigZagOffset (!!)
            #           OffsetPattern : 'Profile', does a inner-profile not pocket, cf. .countour() for outer
            #       Tolerates ExtraOffset or OffsetExtra
            #           If =None, we will try to correct for rounding errors (up to 1e-5) in "width" vs tool_diameter to allow
            #               pockets that are "exactly" the "width" of the tool diameter
            #       'StartPoint' : App.Vector or [x,y,z] or (x,y,z) # handles expressions. defaults to op's start
            #   }
            debug(f"  Pocket: {face_list or body}")

            # set default pocket properties
            # we'll add cut_args below
            pocket_args = {
                'OffsetPattern' : 'ZigZagOffset'
            }

            # FIXME: nasty
            # the path-pocket generates these first gcodes, which cause a "ramp" from 0,0,0 to first cut clearanceheight:
            # not using StartPoint.z
            # G0 Z18.377408 # The start-point.z | safeheight
            # G0 X-1.350000 Y101.600000 Z-18.622592 # start-point's x,y (or chosen), but safeheight for z. Nasty
            # hack: split that 2nd G0: g0 x y, g0 z
            #       or, insert a G0 z, g0 x y==startpoint at beginning

            # FIXME: maybe there is a Path util to find the start point of the op.Path exclusive of the .StartPoint?
            if 'StartPoint' in cut_args.keys():
                # allow explicit UseStartPoint:
                if 'UseStartPoint' not in cut_args.keys():
                    pocket_args['UseStartPoint'] = True
            else:
                # FIXME: this does not set the origin of the operation
                # but rather sets the 1st point to move to
                # and, .Z is ignored anyway
                # So, did not fix the egregious/nasty initial moves
                """ DISABLED
                pocket_args['StartPoint'] = ( 
                    '=#Stock.Shape.BoundBox.Center.x',
                    '=#Stock.Shape.BoundBox.Center.y',
                    # the Path weirdness about an expression string
                    #'=' + job.SetupSheet.SafeHeightExpression
                    '=SafeHeight'
                )
                # allow explicit, despite us adding StartPoint:
                if 'UseStartPoint' not in cut_args.keys():
                    pocket_args['UseStartPoint'] = True
                """

            # merge in the user's properties
            pocket_args = { **pocket_args, **cut_args }
            # we have to set x,y,z separately
            start_point = pocket_args.pop('StartPoint', None)

            ## face.BoundBox[2]-[5]
            #src/Mod/Path/PathScripts/PathPocketShape.py
            #src/Mod/Path/PathScripts/PathPocket.py
            #src/Mod/Path/PathScripts/PathPocketShapeGui.py
            #    PathScripts.PathOpGui
            #    PathScripts.PathPocketBaseGui
            # src/Mod/Path/PathScripts/PathPocketShape.py .Create:
            # a common pattern for an operation (seen also in Profile):
            res = PathScripts.PathPocketShapeGui.Command.res if pocket_args['OffsetPattern'] != 'Profile' else PathScripts.PathProfileGui.Command.res
            debug(f"objfact w/ parentjob={self.fullname(job)}")
            pocket = res.objFactory(res.name, obj=None, parentJob=job)
            pocket.ViewObject.Proxy = PathScripts.PathOpGui.ViewProvider(pocket.ViewObject, res)
            pocket.ViewObject.Proxy.deleteOnReject = False
            self.own_it(pocket)
            let_gui_update()

            if not body:
                body = job.Model.Group[0]

            def face_name(f):
                # allow 'Face0' or a Face object
                if isinstance(f,str):
                    return f
                else:
                    return f"Face{self.face_i(body,f)+1}"

            # Either top-face of Model[0], or the list of body.face-index|body.face-name
            #   face-name is like 'Face1'
            if face_list:
                #pb = [ (body, ( face_name(f),)) for f in face_list ]
                if face_list == '^selection':
                    # this is a little funny use of selection
                    face_list = self._selection
                # tolerate a list element of "Face1" implying `body`, or (body, "Face1") full spec
                pb = [ 
                    ( f if isinstance(f,tuple) else (body, ( face_name(f),)) ) 
                    for f in face_list 
                ]

            else:
                pb = self.find_top_face(job.Model.Group[0])
            debug(f"  pocket.Base { pb }")
            pocket.Base = pb

            # the offset has a different name in Profile and Pocket, sigh
            # tolerate either
            # Then put it back as the correct name
            offset = pocket_args.pop('OffsetExtra',None) or pocket_args.pop('ExtraOffset',None) 
            #debug(f'## cut pock From args offset {offset}')

            if offset==None:
                # if the x or y < bit diam, see if we should 'correct' for rounding error
                #   we are purposefully doing 1e-4 instead of 1e-5 to be more-likely-to-work
                # NB: this only deals with orthogonal width/length ~= tool-diameter. FIXME: make work for diagonal,etc
                # FIXME: the Path.pocket should do that rounding check
                face_shape = pocket.Base[0][0].getSubObject( pocket.Base[0][1] )[0] # really?
                if (
                    (face_shape.BoundBox.XLength - pocket.OpToolDiameter.Value) <= 1e-4
                    or (face_shape.BoundBox.XLength - pocket.OpToolDiameter.Value) <= 1e-4
                    ):
                    offset = '-0.0001 mm' # an offset
                    debug(f'  Dealing with rounding error of XLength|YLength vs tool.Diameter ({pocket.OpToolDiameter}) by offset {offset}')

            let_gui_update(force=True)

            if pocket_args['OffsetPattern'] == 'Profile':
                del pocket_args['OffsetPattern'] # not valid for a Profile op
                ## nb: actually means "do inside contour": 
                pocket.processPerimeter = True # default anyway
                pocket.Side = 'Inside'
                pocket.UseComp = True #UseCompensation# # default anyway, but does the right thing for bit size
                if 'Side' in cut_args.keys():
                    raise Exception(f"Expected `cut_args` to NOT have 'Side':x when 'OffsetPattern':'Profile', saw {pocket_args['Side']}")
                if offset:
                    pocket_args['OffsetExtra'] = offset

                if 'CutMode' in pocket_args:
                    # nb: An inside profile Climb is CW with respect to the outside cut
                    pocket_args['Direction'] = {'Climb':'CW', 'Conventional':'CCW'}[ pocket_args.pop('CutMode') ]
            else:
                if offset:
                    pocket_args['ExtraOffset'] = offset

            #debug(f'## cut pock correct name OffsetExtra {pocket_args.get("OffsetExtra",None)} ExtraOffset {pocket_args.get("ExtraOffset",None)}')

            def expression_job_magic(match_object):
                # '#x...' -> <<job.x.Label>>....  to be parametric, but not cyclic
                # For use in a operation's property expression,
                #   could be a model/stock/etc property
                #   could be a value
                # Should work for things like "#Model[0]"

                # match_object.group(0) should be the '#x' match

                # get the pieces
                debug(f'  ## Job magic on piece: {match_object.group(0)}')
                pieces = re.split(r'\.', match_object.group(0))
                pieces.pop(0) # drop '#'
                debug(f'    ## pieces {pieces}')

                # case: only '#' (no .property...)
                #   Probably doesn't make sense
                if not pieces:
                    raise Exception(f"Saw an operand of plain job (i.e. '#'), which would be a cyclic reference in an operation: {match_object.group(0)}, should be like #somejobattribute (no .)")
                else:
                    # remove first piece and eval that: job.x
                    sub_obj = self.eval_expression( '=' + pieces.pop(0), against=job )
                    debug(f'    ## sub-obj {self.fullname(sub_obj)}')

                    # case: direct job property "#x" -> eval to the <<label>>
                    #   might want .Label in some cases, user can do that
                    # case: "#x..." -> "<<x.label>>...."

                    # Append rest of pieces
                    new_operand = f"<<{sub_obj.Label}>>." + '.'.join(pieces)
                    debug(f'    ## so {new_operand}')
                    return new_operand
                
            # have to set StartPoint element by element for expressions
            for i,v in enumerate(start_point or []):
                axis = ['x','y','z'][i]
                # we have to eval if there is a "job." in the expression, because that's "cyclic"
                # Path gets around that for other things by synthesizing things like "opStockZMax"
                has_job_ref = isinstance(start_point[i], str) and re.search( "#", start_point[i]) and True # hackish
                #debug(f'#### has_job? {has_job_ref}: {start_point[i]}')
                try:
                    self.set_property(
                        pocket, 
                        f'StartPoint.{axis}', 
                        start_point[i], 
                        # '#x...' -> <<job.x.Label>>....  to be parametric, but not cyclic
                        #substitutions={ r'#(\w+)' : expression_job_mwwwagic}, 
                        substitutions = {r'#(\w+)' : (lambda m: getattr(job, m.group(1)).Name)},
                        #eval=has_job_ref 
                    )
                except Exception as e:
                    # mostly for the exception in expression_job_magic
                    App.Console.PrintError( f"In StartPoint.{axis}={start_point[i]}")
                    raise e
                    

            for k,v in pocket_args.items():
                self.set_property(pocket,k,v, 
                    #substitutions={'#(\w+}' : expression_job_magic}
                    substitutions = {r'#(\w+)' : (lambda m: getattr(job, m.group(1)).Name)}
                )

            self.label_it(pocket, name)

            debug(f"cut_pocket {pocket.Label}")
            let_gui_update(force=True)
            self.recompute()
            let_gui_update(force=True)

            # i.e. if no x,y motion, then the pocket must be too small:
            if (pocket.Path.BoundBox.DiagonalLength - pocket.Path.BoundBox.ZLength) < 1e-5:
                App.Console.PrintError( f"re:\n\t{job.Label}\n\t{pocket.Label}\n\t{ [(objface[0].Label, objface[1]) for objface in pocket.Base]}\n")
                Gui.Selection.addSelection( job.Model )
                raise Exception(f'cut_pocket for {pocket.Label}: no X,Y path. Is the model (.Base) too small for the tool?')
            
            # and ramp it
            if ramp:
                fullname = f'Ramp-{name}'
                pocket = self._ramp( pocket, name=fullname )

            return self 

        def mill_face(self, method='adaptive', name=None, ramp=True, ramp_properties={}, **properties):
            # for each selected job | _last==ajob
            # do a mill-face op
            # Assumes the top face is the mill face
            # NB: Mills the entire Stock down to the face.
            #   You might be thinking more of what .pocket(x) will give you: mill down to the actual face
            # You must set `stock` in job(), i.e. ExtZPos:where-to-start-cutting
            #   or OpStartDepth=
            # Will mill off the edge (i.e. full face milling)
            # method
            #   'adaptive' uses adaptive pocket as a cheat
            #   others... tbd

            # debug: if set to a string, will cut short the algorithm at that step (for each job)
            debug_till = None # 'top_face' # 'show_models' # '...' ...


            debug(f"Milling face(s) w/'{method}'")

            mill_method_dispatch = {
                'adaptive' : self.mill_face_adaptive,
                'face' : self.mill_face_face # OffsetPattern = 'ZigZagOffset' instead of ZigZagProfile
                }

            allowed_methods = mill_method_dispatch.keys()
            if method not in allowed_methods:
                raise Exception("Expected method to be on of {allowed_methods}, saw '{method}'")

            jobs = self._selection or [self._job]
            for job_i,job in enumerate(jobs):
                debug(f"mill-face job {job_i+1}/{len(jobs)} {job.Label} W/{properties}")
                self.recompute(job)

                # we only deal with one model per job
                model = self.first_model(job, debug_till)

                if debug_till=='show_models':
                    continue


                # create the ops and set manymany things
                millops = (mill_method_dispatch[method])(job, model, name=name, **properties)
                debug(f"## DID {method}, ramping on {[x.Label for x in millops]}")

                if ramp and method != 'adaptive': # adaptive has its own ramp
                    for millop in millops:
                        self.recompute(millop)

                        fullname = 'Ramp' + (f"-{name}" if name else '') + (f"-{millop.Label}" if len(jobs)>1 else '')
                        ramp = self._ramp(millop, name=fullname, **ramp_properties)

            self.recompute()
            return self

        def find_top_face(self, solid, find_bottom=False, debug_till=None):
            # find the top face, or bottom if find_bottom
            # `solid` is a thing in the "model" section, so is a link, so .Shape.Faces

            def no_z(geom):
                # false if delta z is 0
                #debug( f'##  delta {geom}:[{self.face_i(solid,geom)}] {geom.BoundBox.ZMax}-{geom.BoundBox.ZMin}={(geom.BoundBox.ZMax - geom.BoundBox.ZMin)} {(geom.BoundBox.ZMax - geom.BoundBox.ZMin) < 1e-5}')
                return (geom.BoundBox.ZMax - geom.BoundBox.ZMin) < 1e-5

            # I thought I was getting extra faces, seems to work now
            #horiz_faces = ( x for x in solid.Shape.Faces if (PathScripts.PathGeom.isHorizontal(x) and no_z(x)) )
            horiz_faces = ( x for x in solid.Shape.Faces if PathScripts.PathGeom.isHorizontal(x) )
            #debug(f"## horiz faces {[(PathScripts.PathGeom.isHorizontal(x) and no_z(x)) for x in solid.Shape.Faces]}")
            # "top" is higher z
            top_down = sorted( horiz_faces, key = (lambda f: f.BoundBox.ZMax), reverse=True )
            if not top_down:
                raise Exception(f"No horizontal faces in the model (try debug_till='cuts_as_positives') in {self.fullname(solid)}")

            # this is the op model face
            top_face = top_down[(-1 if find_bottom else 0)]
            face_i = self.face_i(solid, top_face)
            debug(f'#  top_face {top_face} Face{face_i+1}')
            # need a document object later
            #top_face_part = self.part_show( top_face, name=solid.Label)
            if debug_till=='top_face':
                debug(f"#  select {solid.Label}.Face{face_i+1}")
                Gui.Selection.addSelection(solid, f'Face{face_i+1}')
                return
            else:
                self.hide(solid)

            return (solid, (f'Face{face_i+1}',))

        def op_base_face(self, job, model, base='model-top'):
            # many ops need a face
            # default is model's top-face
            # `stock-bottom', 'model-bottom', 'model-top' (also None), OR explicit?
            if base=='stock-bottom':
                top_face = self.find_top_face(job.Stock, find_bottom=-1 )
                debug(f'  using stock-bottom {top_face}')
            elif base=='model-bottom':
                top_face = self.find_top_face(model, find_bottom=-1 )
                debug(f'  using model-bottom {top_face}')
            elif base == 'model-top' or base==None:
                top_face = self.find_top_face(model)
                debug(f'  using model-top {top_face}')
            else: # FIXME: an explicit base which is ....
                raise Exception(f'`base` can only be None or "stock-bottom", saw "{base}"')
            return top_face

        def mill_face_adaptive(self, job, model, name, base=None, tool_number=None, **properties):
            # internal, use fluent.mill_face(method='adaptive') 
            """The specific "add op for adaptive mill face"
                Default properties are overridden by permitted Path Adaptive `properties`
                You must set the stock for start-depth!
                `base` is the Model's top-face by default, or specify 'stock-bottom'
                    that's like MillFace's boundary=stock
            """

            # we need just the top face as the actual model for the op
            top_face = self.op_base_face(job,model,base)

            if not top_face:
                raise Exception(f"Couldn't find the top-face of {self.fullname(model)} (for adaptive op in {job.Label})")

            if tool_number == None:
                tool = job.Tools.Group[0]
            else:
                tool = next((x for x in job.Tools.Group if x.ToolNumber == tool_number), None)

            all_properties = {
                # likely customization:
                'FinishDepth' : '1.00 mm', # to prevent chipping # FIXME: do a second mill with 0.1 step down?

                # define the mill face operation
                # Base # explicit below
                'FinishingProfile' : False, # 'pocket' not profile
                'ForceInsideOut' : True, # prevent chipping on internal island
                'OperationType' : 'Clearing', # must
                'Side' : 'Inside', # must
                'UseOutline' : True, # ignore internal holes
                'FinalDepth' : '=OpFinalDepth',
                #'ExtensionCorners' : True, # to mill "off" the corner area
                # 'ExtensionLengthDefault' : '=OpToolDiameter/2'
                'ToolController' : tool, # need one
                
                # order matters:
                # 'StartDepth' : default based on stock
                # 'SafeHeight' : default based on stock
                # 'ClearanceHeight' : default based on stock, could be '=SafeHeight',

                **properties
            }

            # src/Mod/Path/PathScripts/PathAdaptiveGui.py  src/Mod/Path/PathScripts/PathAdaptive.py
            #PathAdaptive.Create no hint of how to do .Proxy/addExtension, so
            # FIXME: manually making Adaptive requires stock to be +-x&+-y a bit or no path.
            res = PathScripts.PathAdaptiveGui.Command.res
            obj = res.objFactory(res.name, obj=None, parentJob=job)
            self.own_it(obj)
            self.label_it(obj,name)
            obj.ViewObject.Proxy = PathScripts.PathOpGui.ViewProvider(obj.ViewObject, res)

            # FIXME: this doesn't set .Base compatibly with the GUI
            #   you can't see the selection in the task panel, and 'extend' doesn't work
            #   and you will lose the selected Base if you edit the op in the gui
            debug(f"Top Face: {top_face}")
            #obj.Base = [top_face]
            obj.Proxy.addBase(obj, top_face[0], top_face[1][0]) # ends up doing above
            obj.Proxy.sanitizeBase(obj)

            for k,v in all_properties.items():
                self.set_property(obj,k,v,
                    substitutions = {r'#(\w+)' : (lambda m: getattr(job, m.group(1)).Name)},
                )
            #self.recompute(obj)
            return [obj]

        def mill_face_face(self, job, model, name, tool_number=None, base=None, _hack_finish_depth=True, **properties):
            """The specific "add op for 'Face' aka 'MillFace'"
                Default properties are overridden by permitted Op `properties`
                You must set the stock for start-depth!
                This operation will optimize for the 3d shape of the stock
                    you probably want stock==model for this
                    and base='stock-bottom'
                    NB: this will then compensate for the weird FinishDepth modifying FinalDepth thing
                        by making a second mill operation
                    Hack: to do only the second-milling (finish), StartDepth = f'=FinalDepth + FinishDepth'
                `base`|`Base` = `stock-bottom' | 'model-bottom' | 'model-top'
                Expressions allow `#XX` -> `SetupSheetThisJob.XX`

            """

            # FIXME: using a FinishDepth behaves like "leave z stock" instead

            debug(f"## mill_face_face base={base} _hack={_hack_finish_depth}")
            if tool_number == None:
                tool = job.Tools.Group[0]
            else:
                tool = next((x for x in job.Tools.Group if x.ToolNumber == tool_number), None)

            all_properties = {
                # likely customization:
                #'FinishDepth' : '1.00 mm', # to prevent chipping # FIXME: do a second mill with 0.1 step down?

                # define the operation
                # Base # picked by op_base_face below
                'StepDown' : '=OpToolDiameter/2',
                #'KeepToolDown' : True, # faster if safe
                'BoundaryShape' : 'Stock', # determines where it will mill
                'OffsetPattern' : 'Offset',
                #'ExtraOffset' : '=OpToolDiameter/2 - 1mm', # to clear to edge better
                'PocketLastStepOver' : '=StepOver * 0.8', # prevents obsessing for some shapes
                'ToolController' : tool, # need one
                
                # order matters:
                # 'StartDepth' : default based on stock
                # 'SafeHeight' : default based on stock
                # 'ClearanceHeight' : default based on stock, could be '=SafeHeight',

                **properties
            }

            # we need just the top (bottom) face as the actual model for the op
            base = base or all_properties.pop('Base', None)
            mill_to_face = self.op_base_face(job,model,base)

            # src/Mod/Path/PathScripts/PathMillFaceGui.py  src/Mod/Path/PathScripts/PathMillFace.py
            res = PathScripts.PathMillFaceGui.Command.res
            obj = res.objFactory(res.name, obj=None, parentJob=job)

            #obj = PathScripts.PathMillFace.Create('Job', model_list, template)
            #obj.ViewObject.Proxy = PathScripts.PathJobGui.ViewProvider(obj.ViewObject)
            #obj.ViewObject.addExtension("Gui::ViewProviderGroupExtensionPython")
            self.own_it(obj)
            self.label_it(obj,name)
            obj.ViewObject.Proxy = PathScripts.PathOpGui.ViewProvider(obj.ViewObject, res)

            # FIXME: this doesn't set .Base compatibly with the GUI
            #   you can't see the selection in the task panel, and 'extend' doesn't work
            obj.Base = [mill_to_face]
            #obj.Proxy.addBase(obj, mill_to_face[0], mill_to_face[1][0]) ends up doing above

            for k,v in all_properties.items():
                self.set_property(obj,k,v, substitutions = {r'#(\w+)' : (lambda m: getattr(job, m.group(1)).Name)})
            #self.recompute(obj)

            # do the "cut till actual bottom" to finish
            if base=='model-bottom' and _hack_finish_depth: # fixme: and stock==model?
                second_properties = { 
                    **properties,
                    'FinishDepth' : '0 mm',
                    'StartDepth' : f'=FinalDepth + <<{obj.Label}>>.FinishDepth',
                    'BoundaryShape' : 'Face Region', # causes it to use the bottom face
                    'ClearanceHeight' : "=SafeHeight", # optimization, ought to be safe? maybe StartDepth++ is ok?
                }
                debug(f"## mill_face_face ## stock-bottom hack! again with {second_properties}")
                second_mill = self.mill_face_face(job, model, name+"_hack_finish", tool_number=tool_number, base=base, _hack_finish_depth=False, **second_properties)
                debug(f"## mill_face_face: did 2nd mill, returning {obj.Label},{[x.Label for x in second_mill]}")
                return [obj,*second_mill]

            return [obj]

        def helix(self, name, model, job=None, tool_number=None, base=None, **properties):
            """
             `model` can be a model body, none=job.model, or list of edges
                or ^selection, x.x.x etc
            """
            job = self._job or self._selection

            if model == '^selection':
                model = self._find(model)
            else:
                if not isinstance(model,list):
                    model = [ model ]
                model = [ self._find(m) for m in model ]

            res = PathScripts.PathHelixGui.Command.res
            obj = res.objFactory(res.name)
            obj.ViewObject.Proxy = PathScripts.PathOpGui.ViewProvider(obj.ViewObject, res)
            obj.ViewObject.Proxy.deleteOnReject = False

            obj.Base = model
            
            self.own_it(obj)
            let_gui_update()

            for k,v in properties.items():
                self.set_property(obj,k,v, 
                    substitutions = {r'#(\w+)' : (lambda m: getattr(job, m.group(1)).Name)}
                )

            self.label_it(obj, name)

            debug(f"helix {obj.Label}")
            let_gui_update(force=True)
            self.recompute(obj)
            debug(f"re job")
            self.recompute(job)
            debug(f"re done")
            let_gui_update(force=True)

            return self

        def add_path_speeds(self, jobs=None, tool_number=None):
            # Operates on ajob|_selection|_last
            # Insert custom g-code operation (before a dressup)
            # because dressups forget to set an initial speed
            # tool_number is the operation's tool_number not [i]...

            # FIXME:
            # You can op.Path.Commands . prepend instead! do comment & speed

            def feeds(job, op):
                # do we need to set the speed?
                # `op` is us
                tool = job.Tools.Group[ tool_number if tool_number != None else 0 ]
                feeds = []
                if tool.HorizFeed != 0.0:
                    debug(f"  HorizFeed '{tool.HorizFeed}'")
                    feeds.append( f"G1 F{format( Units.Quantity(tool.HorizFeed).getValueAs('mm/s').Value,'.4f')} X0.0 Y0.0" )
                if tool.HorizRapid != 0.0:
                    feeds.append( f"G0 F{format( Units.Quantity(tool.HorizRapid).getValueAs('mm/s').Value,'.4f')} X0.0 Y0.0" )

                debug(f"Path speeds {feeds} for {job.Label}")

                if not feeds:
                    # 0.00 means use machine default
                    return [f'(No default HorizRapid nor HorizFeed in {job.Label} tool {tool.Label})' ]

                return ['G91', *feeds, 'G90']

            self.custom_gcode(feeds, name='set speeds', jobs=jobs, Comment='Ensure speeds before first movement', CycleTime='0.01 secs')

        def custom_gcode(self, *gcode, jobs=None, tool_number=None, name=None, **properties):
            """ Add gcode
            each `gcode` can be:
                lambda job,op: return [gcodes] or None
                'Gsomething' # any gcodes
                None # skip
            Will do nothing if all None.
            Foreach jobs, or _selection or _last
            Leaves _selection
            """

            debug(f'custom gcode {gcode}')

            if not jobs:
                jobs = self._selection or [self._last]
            elif not islisttype(jobs):
                jobs = [jobs]
            if not jobs:
                raise Exception("Expected a `job`, or _selection, or _last")

            for job in jobs:

                def resolve(op, gcode):
                    # lambda or [] or v or None 
                    if callable(gcode):
                        #debug(f'  call gcode-generator {gcode} with {job},{op}')
                        return gcode(job,op)
                    else:
                        return gcode

                def resolve_gcodes(op): # our op==custom-gcode
                    # `gcode` is a function that generates the gcode
                    resolved_gcode = [resolve(op,x) for x in gcode]
                    flattened_gcode = []
                    for x in resolved_gcode:
                        if x==None:
                            continue
                        if islisttype(x):
                            if x:
                                flattened_gcode.extend(x)
                        else:
                            flattened_gcode.append(x)

                    return flattened_gcode

                debug(f'  for job {job.Label}')

                if tool_number == None:
                    tool = job.Tools.Group[0]
                else:
                    tool = next((x for x in job.Tools.Group if x.ToolNumber == tool_number), None)
                if tool == None:
                    raise Exception(f"Did not find a tool_number ({tool_number or 'first'}) in the tools of {job.Label}")
                debug(f'  tool: {tool}')

                # Custom gcode for setspeed
                res = PathScripts.PathCustomGui.Command.res
                gcoder = res.objFactory(res.name, obj=None, parentJob=job)
                self.own_it(gcoder)
                self.label_it(gcoder, name or "GCode")
                gcoder.ViewObject.Proxy = PathScripts.PathOpGui.ViewProvider(gcoder.ViewObject, res)
                gcoder.ViewObject.Proxy.deleteOnReject = False

                # relative mode so we don't actually move
                #debug(f"  gcode -> {['G91', *feeds, 'G90']}")
                gcoder.ToolController = tool # just for info, apparently not used

                default_properties = {
                    'CycleTime' : '0.01 secs',
                }

                for k,v in { **default_properties, **properties}.items():
                    self.set_property(gcoder,k,v)

                self.recompute(gcoder)

                #debug(f'  gcode fn {gcode}')
                flattened_gcode = resolve_gcodes(gcoder) # using the current gcoder:op, and the `gcode` arg
                if not flattened_gcode:
                    debug(f'  no gcode for {job.Label}')
                    continue
                debug(f'  gcode for {job.Label} : {flattened_gcode}')

                gcoder.Gcode = flattened_gcode
                #debug(f"  gcode <- {gcoder.Gcode}")

                self.recompute(gcoder)

            return self


        def postprocess(self, filename, jobs=None, processor=None, catenate=True, leaveold=False, postArgs=[]):
            # aka make the gcode
            # from jobs, or selection, or last job
            # Will catenate the sbp files together

            if jobs:
                jobs = jobs if islisttype(jobs) else [jobs]
            else:
                jobs = self._selection or [self._job]

            # Let's just be helpful and filter by PathScripts.PathJob.ObjectJob
            was_jobs=jobs
            jobs = [ x for x in jobs if 'Proxy' in dir(x) and isinstance(x.Proxy, PathScripts.PathJob.ObjectJob) ]
            if not jobs:
                raise Exception(f"Selection (or the last job) should have something, but after filtering for PathScripts.PathJob.ObjectJob, there was none in:\n\t{[self.fullname(x) for x in was_jobs]}")

            bad = [ x for x in jobs if not hasattr(x, "Path") ]
            if len(bad) > 0:
                raise Exception(f"Selection (or the last job) should all have a .Path, but these don't: {[self.fullname(x) for x in bad]}")
            debug(f"Postprocess {len(jobs)}")

            debug(f"  Save to R: {filename}")
            if not filename.startswith("/"):
                filename = os.path.dirname( self._doc.FileName) + "/" + filename 
            debug(f"  Save to {filename}")
            if not os.path.exists( os.path.dirname(filename) ):
                os.makedirs( os.path.dirname(filename), exist_ok=True )

            # hack to use the all the code that does the right thing
            cpp = PathScripts.PathPost.CommandPathPost

            fname,ext = os.path.splitext(filename)
            if ext=='':
                raise Exception(f"Expected the postprocess filename to have an extension, saw: {filename}")

            # cleanup output dir for catenate
            if catenate and len(jobs)>1:
                os.makedirs(fname, exist_ok=True)
                # Should be safe to remove, the concate name is the folder, so means "this set"
                if not leaveold:
                    for oldf in os.listdir(fname):
                       os.remove( f'{fname}/{oldf}' )

            job_files=[]
            for job_i,ajob in enumerate(jobs):

                processor = processor or ajob.PostProcessor
                debug(f'  for {self.fullname(ajob)}')

                processor_package = PathScripts.PathPostProcessor.PostProcessor.load(processor)
                # split args on whitespace
                all_args = [ '--no-show-editor', '--comments']
                all_args.extend( ajob.PostProcessorArgs.split() )
                all_args.extend(postArgs)

                #if ajob.isDerivedFrom('Path::FeaturePython'):
                #    debug(f"Consider {ajob.TypeId} {ajob.isDerivedFrom('Path::FeaturePython')} {ajob.Proxy.__class__} {isinstance(ajob.Proxy,PathScripts.PathJob.ObjectJob)} {ajob.Label}")
                # allow non-jobs in the list
                # which happens a lot after a .pocket3d
                if not (ajob.isDerivedFrom('Path::FeaturePython') and isinstance(ajob.Proxy,PathScripts.PathJob.ObjectJob)):
                    continue

                ajob.OrderOutputBy = 'Operation'

                postlist = cpp.buildPostList(cpp, ajob)
                for slist in postlist:
                    fname,ext = os.path.splitext(filename)
                    escaped_ajob_label = re.sub(r'[^0-9A-Za-z.]','_',ajob.Label)
                    if catenate and len(jobs)>1:
                        job_filename = f"{fname}/{escaped_ajob_label}{ext}"
                    else:
                        job_filename = f"{fname}-{escaped_ajob_label}{ext}"
                    job_files.append(job_filename)
                    processor_package.export(slist, job_filename, " ".join(all_args) )
                    debug(f"Postprocess '{processor}' {ajob.Label} as {job_filename}")


            if catenate and len(jobs)>1:
                catenator = f"postprocess_catenate_{processor}"
                debug(f"  Catenate w/{catenator} -> {filename}...")
                getattr(self,catenator)(filename, job_files)

            return self

        def postprocess_catenate_xopensbp(self, out_filename, job_filenames):
           # for my updated version of opensbp postprocess
           self.postprocess_catenate_opensbp(out_filename, job_filenames)

        def postprocess_catenate_opensbp(self, out_filename, job_filenames):
            # 
            # fixme: the processor should provide a "comment" function, opensbp almost does it
            def comment(msg):
                return "'" + msg

            # keep track of some state
            seen = {}
            seen['C6'] = False
            seen['TR'] = 0

            with io.open(out_filename,'w') as cat_h:

                cat_h.write( comment(f"Catenated jobs ({len(job_filenames)})\n") )

                for job_i,job_file in enumerate(job_filenames):
                    cat_h.write( comment(f"# Catenate [{job_i}] {job_file}\n") )

                    with io.open(job_file) as job_h:

                        in_preamble = False # `True` was assuming preambles all the same, so could skip them
                        preamble = []

                        for line in job_h:
                            
                            # Accumulate/bypass preamble
                            if in_preamble:
                                if line.startswith("'(finish operation: TC:") and preamble[-1].startswith('&Tool='):
                                    in_preamble=False
                                    debug(f"  Preamble {len(preamble)}")
                                    continue
                                else:
                                    preamble.append( line )
                                    continue
                                
                            # preamble for all
                            if preamble:
                                if job_i==0:
                                    cat_h.write( comment(f"Preamble lines {len(preamble)} from {job_file}") )
                                    for l in preamble:
                                        cat_h.write( l )
                                    cat_h.write( comment("# Preamble finish\n") )
                                else:
                                    cat_h.write( comment(f"Skipped preamble lines {len(preamble)} from {job_file}") )
                                preamble = []

                            # skip inserted pauses
                            if line == 'PAUSE 2\n':
                                continue

                            # skip redundant lines
                            if line.startswith('C6'):
                                if seen['C6']:
                                    continue
                                seen['C6'] = True
                            if line.startswith('C7'):
                                if not seen['C6']:
                                    continue
                                seen['C6'] = False

                            if line.startswith('TR'):
                                speed=line.split(',')[1]
                                speed = speed.rstrip("\n")
                                if seen['TR'] ==speed:
                                    continue
                                seen['TR'] = speed
                                #debug(f"#### SPEED {seen['TR']}")

                            # copy line
                            cat_h.write( line )

                        if in_preamble:
                            raise Exception(f"Expected to find the end of the preamble in {job_file}")

                    cat_h.write( comment(f"# End {job_file}\n") )
                cat_h.write( comment("# End Catenate\n") )

        def path_template(self, template_file):
            """Sets the path-template for subsequent jobs
                needs extension, e.g. job_025in_template.json
            """
            debug(f"Path-Template {template_file}")

            fullname = template_file
            if not fullname.startswith("/"):
                fullname = os.path.dirname( self._doc.FileName) + "/" + fullname 
            if not os.path.exists(fullname):
                raise Exception(f"Can't find the file: {fullname}")

            # but leave it as relative

            sublang = self.__class__.fork(self, _path_template=template_file )

            return sublang

        def shapestring(self, 
            # "|" "<" ">" or None for each x,y,z, for minmost,centered,maxmost
            text=None, # subst(.Label) | text | name | index
            position = ("|","|",">"),
            # ( regex, lambda match-on-.Label : the label text )
            subst=None,
            in_body=False, # true to put the shapestring in the body (jobs want an shapestring elsewhere)
            relative=None, # relative to this, else relative to each selection

            height=4.0, # text height
            depth=1.0, # depth of engrave
            font='CambamStickFonts/1CamBam_Stick_2.ttf',
            margin=NoArg, # for edges, default to height
            name=None
            ):
            """
            Usage:
                CAD:
                    ...some current _body
                    .shapestring(..., in_body=True)
                CAM:
                    nb: shapestrings should not be in a body
                    Explicit text:
                        .shapestring(text='...')
                        .job(...)
                        .engrave(...)
                    Derive text from selection's labels
                        .shapestring(subst=(r'pattern', lambda m: f'someresult {m.group[0]}'), ...)
                            where m = re.search( pattern, substrate.Label )
                        .job(...)
                        .engrave(...)
                    Or, from selection
                        .shapestring(text='...')
                        ...
                        .select( shapestring things )
                        .job(...)
                        .engrave(...)

            For each selected|._last job
                create a ShapeString -> selection
                With the engrave text
                    `text` or re.sub(Label,subst) or 0..n
            """

            debug(f"ShapeString {subst[0] if subst else ''}")

            # FIXME? this strips the parameterization from the font filename
            font_expanded = self.eval_expression(font) # FIXME maybe per each?

            if not font_expanded.startswith("/"):
                font_expanded = os.path.dirname( self._doc.FileName) + "/" + font_expanded 
            if not os.path.exists(font_expanded):
                raise Exception(f"No such font file (specify an existing one with font=...): {font_expanded}")
            
            if margin == NoArg:
                margin = f'{height} mm'
            elif margin == None:
                margin = '0 mm'

            # we can do anything with .Shape or a Job.Model[0]
            substrates = self._selection or aslist(self._job) or [self._last]

            new_selection = []
            for substrate_i,substrate in enumerate(substrates):
                debug(f"  substrate='{substrate.Label}'")
                if isinstance(substrate, str):
                    substrate = self.find(substrate)

                # If a job, use .Model[0]
                # fixme? the job Model could be multiple, implying some locations, e.g. faces...
                actual_substrate = (
                    substrate.Model.Group[0] 
                    if (substrate.isDerivedFrom('Path::FeaturePython') and isinstance(substrate.Proxy, PathScripts.PathJob.ObjectJob))
                    else substrate
                    )
                if not hasattr(actual_substrate, 'Shape'):
                    raise Exception(f"Expected the substrate ([{substrate_i}]) to have a .Shape, but it didn't: {self.fullname(actual_substrate)}")
                debug(f"  on {actual_substrate.Label} <- {substrate.Label if substrate != actual_substrate else ''}")

                # run subst to get text
                if subst:
                    pattern, subst_fn = subst
                    if m := re.search( pattern, substrate.Label ):
                       #debug(f"    re-search {m} groups {m.groups()}")
                       text = subst_fn( m )
                    else:
                        raise Exception(f"The `subst` did not re.search the substrate Label: /{pattern}/ ~~ {substrate.Label}")
                elif text == None:
                    text = name if name else str(substrate_i)
                debug(f"    text='{text}'")

                # create a cambam2 draft.string-text, makes a full gui object
                ss = Draft.make_shapestring(text, font_expanded, Size=height, Tracking=0)
                self.own_it(ss)
                self.label_it(ss, name or f"ShapeString-{substrate.Label}")

                # shape strings start at bot-left
                correction = (None,None,None) # ('-.Shape.BoundBox.XLength/2 * 1mm', '-.Shape.BoundBox.YLength/2 * 1mm', None)
                if in_body:
                    orig_relative = relative
                    relative = self.find(relative, fail=True)
                    if not relative:
                        raise Exception(f"Expected relative=athinginthisbody when `in_body`=True, saw {orig_relative}")
                    self.move(ss, substrate)
                    self.set_support(ss, body=substrate)
                    # position relative to itself, because we are in the body, so relative to 0
                    self._position_relative( ss, position, relative=relative, margin=margin, correction=correction )
                elif self._group: # for jobs on model
                    self.move(ss, self._group)
                    self._position_relative( ss, position, relative=actual_substrate, margin=margin, correction=correction )

                # position it
                self.show( actual_substrate ) # debug

                new_selection.append(ss)
                self._last = ss

            self._selection = new_selection
            return self

        def engrave(self, 
            depth=0.1,
            # defaults to Model[0].zmax
            start=None,
            name=None
            ):
            """
            Usage
                A sketch, with several wires: "normal geometry" (construction geom is ignored)
                    .select(None)
                    .job(..., model='thesketch')
                    .engrave()
                See shapestring() for text strings

                Do not mix sketches & shapestrings in the same job
            For each selected|._last job
                Engrave the model
                You should pick a suitable tool via path-template() for the job
                The job can ONLY have Draft ShapeStrings (i.e. .label()) in it
                    or other 2D objects (sketches that aren't in a body)
                    (a restriction of engrave)
            """

            jobs = self._selection or [self._job]
            debug(f"engrave {[x.Label for x in jobs]}")


            for job_i,job in enumerate(jobs):
                debug(f"  job='{job.Label} models={[x.Label for x in job.Model.Group]}'")

                wires = []
                for model in job.Model.Group:
                    self.hide( model )

                    if (
                        # set our base to the wires if a sketch
                        # if we just used the model=sketch, we often only get the first wire
                        self.effective(model).isDerivedFrom('Sketcher::SketchObject')
                        ):
                        edges = []
                        for awire in model.Shape.Wires:
                            for anedge in awire.Edges:
                                edges.append( f'Edge{1+self.edge_i(model, anedge)}' )
                        wires.append( (model, tuple(edges)) )
                        # vertexes don't work
                        #wires.append( (model, tuple(f'Vertex{i+1}' for i,w in enumerate(model.Shape.Vertexes)) ) )
                    debug(f'\n\n   wires from {self.effective(model).Label}: {wires}')

                # create an engrave for depth
                obj = self.cut_engrave( job, start=start, depth=depth, base=wires, name=(name or f"Engrave-{job.Label}") )
                self.own_it(obj)
                self._last = obj

            return self

        def cut_engrave(self, job, depth, start=None, base=None, name=None):
            # engrave the thing, assume it is on the zmax
            # NB: we adjust the stock to the model[0]
            # `base` could be wires for a sketch, i.e. geom [ (obj, ('Edge1',...)), ... ]

            if start==None:
                start = job.Model.Group[0].Shape.BoundBox.ZMax
            #debug(f"    ### engrave {start}..{depth} in {job.Label}")
            debug(f' engrave base {base if base else ".Model"}')

            # Engrave targets are 2d shapes
            # So, depth is stock - depth
            # have to adjust the job.stock, this seems to work
            #job.Stock = job.Model.Group[0]
            for axis in ['X','Y']:
                for dir in ['pos','neg']:
                    p = f"Ext{axis}{dir}"
                    # have to have some *Lengths or we get the 'length of box too small' thing
                    if getattr(job.Stock,p) == 0:
                        setattr(job.Stock, p, 0.01)
            self.set_property(job.Stock,'ExtZneg', depth) # negative
            job.Stock.ExtZpos = 0 # 0.01

            self.recompute() # recompute(job) doesn't work
            if 'Invalid' in job.Stock.State and 'length of box too small' in job.Stock.getStatusString():
                App.Console.PrintWarning( "-----\n\n") # we get the error printed again when we exit fluent
                #raise Exception("2D sketches need an explicit 3d stock (setting Ext... isn't enough)\n")
            #debug(f'  State {job.Stock.State} {job.Stock.getStatusString()}')

            with self.recomputing(False):
                res = PathScripts.PathEngraveGui.Command.res
                engrave = res.objFactory(res.name, parentJob=job)
                engrave.ViewObject.Proxy = PathScripts.PathOpGui.ViewProvider(engrave.ViewObject, res)
                engrave.Base = base

            if start != None:
                self.set_property(engrave, 'StartDepth', start)
            self.set_property(engrave, 'FinalDepth', '=OpStockZMin') # has depth encoded in it
            #job.FinalDepth = "={job.Label}.Group[0].Shape.BoundBox.ZMax - depth"

            self.label_it(engrave,name or f"Engrave-{engrave_body.Label}")
            self.own_it(engrave)

            self.recompute(job)

            return engrave

        def part_show(self, facedgetc, name=None, group=None):
            # turn something into a document object (Part::Feature)
            # like a face or edge or other shape
            
            obj = Part.show(facedgetc)
            self.own_it(obj)
            self.label_it(obj, name)
            self.move( obj, group or self._group )
            return obj

        def stl(self, filename=None, catenate=False, stlArgs=[]):
            # aka make the stl file(s)
            # from a selection, or ._body

            self.recompute() # ensure things are right

            jobs = self._selection or [self._body]
            bad = [ x for x in jobs if not x.isDerivedFrom("PartDesign::Body") ]
            if len(bad) == len(jobs):
                raise Exception(f"Selection (or the last job) should all be a PartDesign::Body, but these weren't: {[self.fullname(x) for x in bad]}")
            debug(f"STL {len(jobs)}")

            if not filename:
                filename = self._doc.Label + ".stl"

            if not filename.startswith("/"):
                filename = os.path.dirname( self._doc.FileName) + "/" + filename 

            job_files=[]
            if catenate and len(jobs)>1:
                debug(f"  Catenate w/{catenator} -> {filename}...")
                Mesh.export(jobs,job_filename)

            else:
                for ajob in jobs:
                    fname,ext = os.path.splitext(filename)
                    if not ext:
                        ext='.stl'
                    job_filename = f"{fname}-{ajob.Label}{ext}"
                    debug(f"  Wrote {job_filename}")
                    job_files.append(job_filename)

                    Mesh.export([ajob],job_filename)

            return self


class SubFluentGroup(
    FluentSubLang,
    FluentBase,
    FluentCam
    ):

    def __init__(self, **context):
        # .group() created a group and supplied that as '_group':group
        self.context( **context )
        #debug(f"Group cont {self._context}")
        super().__init__()

class FluentRoot(
    # FIXME: dynamic mixin adding, search "namespace"(?) for other toplevelx
    FluentSubLang,
    FluentBase,
    ):

    def __init__(self, in_script, clean=True):
        """Takes the text-document to establish the root group
        clean=true will delete all of the objects made by that script
        `in_script` is the text-document (or something you want to be the owner)
        """

        debug(f"Init {self.__class__.__name__} {id(self)}")

        if isinstance(in_script,str):
            in_script = self.find(in_script)

        orig_in_script = in_script

        if in_script.isDerivedFrom('App::DocumentObjectGroup') or in_script.isDerivedFrom('App::Part'):
            root = in_script
        else:
            root = in_script

        if not root:
            raise Exception("Expected {self.__class__.__name__}({orig_in_script.Label},...) to be a group or in a group")

        self.context(
            _context_parent = None,
            _last = None,
            _doc = root.Document,
            _root = root,
            _markers = [],
            )

        # so clean sees current state, and everything else
        with self.recomputing(True):
            self.recompute()
        
        # right-to-left init
        super().__init__()

        self.clean(noop=not clean)

    def clean(self, noop, obj=None):
        # whack all owned, from our root unless noop
        global clean_ct

        if noop:
            return

        debug("Going to clean")
        clean_ct += 1
        if clean_ct > 2:
            raise Exception(f"Expected only 1 clean per 'script', saw Clean {clean_ct}")

        if not obj:
            obj = self._doc
        debug(f"  from {self.fullname(obj)}")

        # clean up, so the OutList's etc are correct
        self.recompute()

        if obj.isDerivedFrom("App::DocumentObjectGroup") or obj.isDerivedFrom('App::Part'):
            check = obj.OutListRecursive
        else:
            check = obj.Objects

        self.delete(check, (lambda x: FluentOwned in x.PropertiesList and x.FluentOwned == self._root.Name) )

    def describe(self,msg=None,ancestors=False):
        """Print the context, for debug, etc"""
        self._describe_indent = 0
        if msg:
            print("## " + msg)
        else:
            print("## Describe:")
        self._describe_indent += 1
        self.describe_lang(self._describe_indent)
        self._describe_indent += 1

        print(("  " * self._describe_indent) + f"doc: {self.fullname(self._doc)}")
        self._describe_indent += 1
        print(("  " * self._describe_indent) + f"root: {self.fullname(self._root)}")
        return self

def Fluent(owner, clean=True):
    f = FluentRoot(in_script=owner, clean=clean)
    #f.describe()
    return f.group(f._root)
