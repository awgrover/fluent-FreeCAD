#!/usr/bin/env python3
# Dual CLI and lib
"""
Build sub-templates:
    Can I replace a single k=v
    At depth
        no, oops
        at end of path
            ensure into's[k|i]
            if no into[k|i] just copy, done
        recurse copy
    Can I replace a set of k=v
        yes
    Can I replace an entire sub-dict
        no, only every key
        * add -r path
    Can I delete
        * add k=None for a k

    post: .Post .PostArgs

    if value is None, delete
    SetupSheet.SafeHeightExpression=None

    plywood 0.25"
        SetupSheet
            Pocket Shape
        ToolController.[0]
            vfeed
            hfeed
            xengine
                is a bad list [ {}... }
                where each {} is the {'prop' : property, 'expr' : expr }
                which is stupid. should have been { property : expr, ... }
    slot in y
    SetupSheet
        Pocket Shape
            ZigZagAngle: "90.0"
"""

__all__=[]

import sys,os,inspect
from copy import copy
import json



# --- [--nohack] [-f] a.json path path... -f b.json path... -o output.json 
# each `-f file path...` includes just the path part of that file
#   and merges it ontop of the previous file
# --nohack turns off the MergeHack entries for hack'ish coping
# stdout is default 

UseMergeHack = True
MergeHack = {} # setup:
def setup_MergeHack():
    # namespace the functions
    global MergeHack

    def kvlist_to_dict( alist, indent, key):
        rez = {}
        debug(f'New kv from alist {alist}', indent+2)
        for kv in alist:
            adict = copy( kv )
            adict.pop( key)
            rez[ kv[key] ] = adict
        debug(f'  -> {rez}', indent+2)
        return rez

    def dict_to_kvlist( adict, indent, key):
        rez=[]
        for k,v in adict.items():
            v[key] = k
            rez.append(v)
        return rez

    MergeHack = {
        # various hack fixups to cope with merging issues

        # these are fn(list, indent, **args)->newform & merge, & fn(merged, indent, **args)->oldform
        'list' : {
            # convert a list of { 'prop':p, ... } to dict of {p: {...},...}, merge, then back
            'xengine': { 'pre': kvlist_to_dict, 'post': dict_to_kvlist, 'args': {'key':'prop'} },
        },
        'dict' : {},
    }
setup_MergeHack()

def debug(msg,indent=0,up=0):
    # if lambda...

    frame = inspect.currentframe()
    for i in range(0,up+1):
        poss_frame = frame.f_back
        if poss_frame: # prevent too far
            frame = poss_frame

    frame_code = frame.f_code
    fname = frame_code.co_filename
    #if fname.startswith(App.getUserAppDataDir()):
    #    # trim app dir, esp for in a macro
    #    fname = fname[ len(App.getUserAppDataDir()) : -1]
    sys.stderr.write( ("  "*indent) + f"[{frame.f_lineno}] {msg}\n" )

def merge_dict(into, data, path, indent): # FIXME: a replace=True here, then if no more path just assign
    # path is a list now
    # just do the 1st path piece, which should be a string, and not a [n]

    def _merge_dict(_into,_data,_step, path,indent):
        debug(f"_merge_dict for [{_step}] : {_data[_step]}", indent)
        _data = _data[_step]

        if not _step in _into:
            # make a the type if it doesn't exist
            # can't just assign, because we might be skipping _into the depths
            if isinstance(_data,dict):
                debug(f"Add [{_step}]={_data}",indent+2)
                _into[_step]={}
            elif isinstance(_data,list):
                debug(f"Add [{_step}]={_data}",indent+2)
                _into[_step]=[]

        if not( isinstance(_data,dict) or isinstance(_data,list) ):
            # termimal
            debug(f" [{_step}]={_data}, no recursion",indent+2)
            _into[_step]=_data
            return


        debug(f"merge members [{_step}]",indent+1)

        if UseMergeHack and _step in MergeHack['list']:
            pre_hack = MergeHack['list'][_step]['pre']
            post_hack = MergeHack['list'][_step]['post']
            args = MergeHack['list'][_step]['args']

            into_alist = pre_hack( _into[_step], indent, **args )
            from_alist = pre_hack( _data, indent, **args )
            debug(f"merge as alist [{_step}]",indent+1)

            merge_data( into_alist, from_alist, path[1:], indent+3 )

            debug(f"got merged alist {into_alist}", indent+1)
            into[_step] = post_hack( into_alist, indent, **args )
            debug(f"got {into[_step]}", indent+1)
        else:
            merge_data(_into[_step],_data,path[1:], indent+2)

    if not path:
        # end of path, copy the rest
        debug(f"dict recursive merge: all kv [{path}] {into}", indent+1)
        debug(f"  from {data}",indent+1)
        # recursively merge all the data
        for step in data.keys():
            debug(f"merge into [{step}] : {into.get(step,'n/a')}", indent+1)
            debug(f"from [{step}] {data[step]} {data}",indent+1)
            _merge_dict(into, data,step,path,indent+2)
            debug(f"giving: [{step}] : {into[step]}", indent+1)

    else:
        if path[0].startswith('['):
            raise Exception(f"Expected a dict key (not '[...'), saw {path[0]}")

        _merge_dict(into,data, path[0], indent)

def merge_list(into, data, path, indent): # FIXME: a replace=True here, then if no more path just assign
    # path is a list now
    # just do the 1st path piece, which should be a [n]

    def _merge_list(into,data,i,indent):
        debug(f"_merge_list for [{i}] into {into}", indent+1)
        data = data[i]

        if len(into)-1 < i or into[i]==None:
            # can't just assign, might be skipping into depths
            terminal = False
            if isinstance(data,dict):
                debug(f"Add [{i}]={data}",indent+2)
                new_data={}
            elif isinstance(data,list):
                debug(f"Add [{i}]={data}",indent+2)
                new_data=[]
            else:
                # leaf str/int
                debug(f"Replace None [{i}]={data}",indent+2)
                new_data=data
                terminal = True

            if len(into)-1 < i:
                debug(f"Assign [{i}]={data}",indent+2)
                into.append( new_data )
            else:
                into[i] = new_data

            if terminal:
                return

        debug(f"merge [{i}]",indent+1)
        merge_data(into[i],data,path[1:], indent+2)

    if not path:
        # end of path, copy the rest
        debug(f"list recursive merge: all i in {data}", indent+1)
        debug(f"  into {into}",indent+2)
        # recursively merge all the data
        for i in range(0,len(data)):
            _merge_list(into, data,i,indent+1)

    else:
        m = path[0].match('\[(\d+)]')
        if not m:
            raise Exception(f"Expected a list index like '[4]', saw: {path[0]}")

        _merge_list(into,data, m.group(1), indent)



def merge_data(into, from_data, path, indent):
    # merge dict/list
    # dict keys overwrite "into"
    #   but, only the leaf of the path
    #   so, have to create intermediate elements...
    # for a list position
    #   [i] overwrites
    #   ['+i'] appends from_data[i] to into[]

    debug(f"Merge things {into.__class__.__name__} & {from_data.__class__.__name__}",indent)
    if isinstance(into,dict) and isinstance(from_data,dict):
        try:
            merge_dict(into,from_data,path, indent)
        except Exception as e:
            raise Exception(f"At {path}") from e

    elif isinstance(into,list) and isinstance(from_data,list):
        try:
            merge_list(into,from_data,path, indent)
        except Exception as e:
            raise Exception(f"At {path}") from e
    else:
        raise Exception(f"Expected `into` and `from` to be same type (dict|list), but saw {into.__class__.__name__} & {from_data.__class__.__name__}")

if __name__ == "__main__":

    def merge_extract(sofar, args, indent=0):
        # args should be: -f file path... [-f file path...]... [-o file]

        if not args:
            debug(f"Done")
            return

        debug(f"Read {args[1]} ... {args}",indent)

        # read all the data
        input = args[1] # -f file
        with open(input) as fh:
            data = json.load(fh)

        try:
            merge_paths(sofar, data, args[2:], indent)
        except Exception as e:
            raise Exception(f"At {args}") from e

    def merge_paths(sofar, data, paths, indent):
        
        # if paths, merge them into sofar

        # no paths, merge all
        if not paths or paths[0] == '-f':
            debug(f"No path, merge all",indent+1)
            merge_data(sofar, data, [], indent+1)
            # and recurse on next file
            debug(f"Next file, rest {paths}",indent+1)
            merge_extract(sofar, paths, indent)
            return

        path = paths[0].split('.')
        debug(f"Merge path {path}",indent+1)
        merge_data(sofar, data, path, indent+1)

        # and do rest of paths
        if paths[1:]:
            debug(f"Rest {paths[1:]}",indent)
            merge_paths(sofar, data, paths[1:], indent)


    def main():
        # merge/extract

        sys.argv.pop(0)

        # option 1st -f
        if len(sys.argv) > 0:
            if sys.argv[0] == '--nohack':
                UseMergeHack=False
                sys.argv.pop(0)

            if not sys.argv[0].startswith('-'):
                sys.argv.insert(0,'-f')

            if sys.argv[0] == '-f':
                rez = {}
                try:
                    merge_extract(rez, sys.argv)    
                except Exception as e:
                    sys.stdout.flush()
                    raise Exception(f"CLI args: {' '.join(sys.argv)}") from e
                    
                print( json.dumps(rez, sort_keys=True, indent=2) )

                return

            sys.stderr.write(f"Unknown option {sys.argv}\n")
            return

        sys.stderr.write("Need some arguments\n")

    main()
