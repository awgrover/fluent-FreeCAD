import itertools
import Path

class NullFilter :
    """
        can't filter at Header, Preamble, Postamble, etc.
        Only the job's.Path.Commands
        Because: we are in gcode land, and we can't pass our result to the postprocessor's parse()
            because we'd have to make a job.Path object...
    """
    def __init__(self,objectslist, filename, argstring):
        # same args as a postprocessor.export()
        # could do some init here based on args
        print(f'New! {self}')

    def filter(self, path_obj, path_commands):
        # .Name = comment|Gxxx|Mxxx|etc, .Parameters = { k:v...} for the remaining "words": Kvvvvv
        # Path.Command('G41 X5...')
        # Path.Command() .Name=,.Parameters=

        return path_commands

        """
        rez = []
        rez.append( Path.Command(f'(filtering {path_obj.Label} "{path_obj.Name}" {path_obj.__class__.__name__}: {self.__class__.__name__})') )
        print(f'## {[str(x) for x in rez]}')

        # apply patterns to the path_commands

        return itertools.chain(rez,path_commands)
        """

def eq_gcode(a,b):
    # true if A has all of B's parts, and they are equal
    # This is not a symmetric equal!
    # eq_gcode(G0 X:1 Y:2, G0 X:1) is equal
    # eq_gcode(G0 X:1 Y:2, G0 X:1 Z:4) is not equal
    # specify all the parameters that you want taken into account

    global why

    #print(f'? {a}   {b}')
    if a.Name != b.Name:
        why=f'.Name not equal'
        return False

    for p in b.Parameters.keys():
        if p not in a.Parameters:
            why=f'{p} not in a {a.Parameters}: {a}'
            return False
        if a.Parameters[p] != b.Parameters[p]:
            why=f'{p} !='
            return False
    
    return True

"""
def test(a_args,b_args, expected, msg=None):
    try:
        a=Path.Command(*a_args)
        b=Path.Command(*b_args)
        rez = eq_gcode(a,b)
        if rez != expected:
            print(f"Expected {expected}, saw {rez}. {msg or ''}\n\t{why}\n\t{a_args} ? {b_args}\n\t{a} ? {b}")
    except Exception as e:
        print(f"For {a_args}  {b_args}  {expected} {msg}")
        raise e

#test( ('G1',),('(obviously)',), True)
test( ('G1',),('(obviously)',), False)
test( ('G0',),('G0',), True)
test( ('G0',{'X':1}),('G0',), True, "only b's elements")
test( ('G0',{'X':1}),('G0',{'X':1}), True,'same 1')
test( ('G0',),('G0',{'X':1}), False, "all of b's elements")
test( ('G0',{'X':1,'Y':2}),('G0',{'X':1,'Y':2}), True,'same 2')
test( ('G0',{'X':1,'Y':2,'Z':3}),('G0',{'X':1,'Y':2}), True, "extra a")
test( ('G0',{'X':1,'Z':3}),('G0',{'X':1,'Y':2}), False, "all of b")
print("done")
"""
