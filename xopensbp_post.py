# ***************************************************************************
# *   Copyright (c) 2014 sliptonic <shopinthewoods@gmail.com>               *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

from __future__ import print_function
import datetime
from PathScripts import PostUtils

import sys,importlib
import FreeCAD
from PathScripts.PathPostProcessor import PostProcessor
from PathScripts.PathToolController import ToolController

TOOLTIP = """
This is an postprocessor file for the Path workbench. It will output path data
in a format suitable for OpenSBP controllers like shopbot.  This postprocessor,
once placed in the appropriate PathScripts folder, can be used directly from
inside FreeCAD, via the GUI importer or via python scripts with:

import Path
Path.write(object,"/path/to/file.ncc","post_opensbp")
"""

"""
DONE:
    uses native commands
    handles feed and jog moves
    handles XY, Z, and XYZ feed speeds
    handles arcs
    support for inch output
ToDo
    redo in "modern" style like other postprocessors (argparse.ArgumentParser)
    comments may not format correctly
    drilling.  Haven't looked at it.
    many other things

"""

TOOLTIP_ARGS = """
Arguments for opensbp:
    --comments          ... insert comments - mostly for debugging
    --inches            ... convert output to inches
    --no-header         ... suppress header output
    --no-show-editor    ... don't show editor, just save result
    --filter=name,name... ... filters for gcode (in order) after .Path.Commands, but before this postprocessor sees it. in post/$name_filter.py. nb '='
"""

OUTPUT_COMMENTS = False
OUTPUT_HEADER = True
SHOW_EDITOR = True
COMMAND_SPACE = ","

# Preamble text will appear at the beginning of the GCODE output file.
PREAMBLE = """"""
# Postamble text will appear following the last operation.
POSTAMBLE = """"""

# Pre operation text will be inserted before every operation
PRE_OPERATION = """"""

# Post operation text will be inserted after every operation
POST_OPERATION = """"""

# Tool Change commands will be inserted before a tool change
TOOL_CHANGE = """"""

# to distinguish python built-in open function from the one declared below
if open.__module__ in ["__builtin__", "io"]:
    pythonopen = open

CurrentState = {}


def getMetricValue(val):
    return val


def getImperialValue(val):
    return val / 25.4


GetValue = getMetricValue


def filter(pathobj, path_commands):
    # an iterable of Path.Commands
    # we return the filtered commands
    filtered = path_commands
    for f in Filters:
        filtered = f.filter(pathobj, filtered)
    return filtered

def export(objectslist, filename, argstring):
    global OUTPUT_COMMENTS
    global OUTPUT_HEADER
    global SHOW_EDITOR
    global CurrentState
    global GetValue
    global Filters
    Filters = []
    
    filter_names = [] # ['null']

    for arg in argstring.split():
        if arg == "--comments":
            OUTPUT_COMMENTS = True
        if arg == "--inches":
            GetValue = getImperialValue
        if arg == "--no-header":
            OUTPUT_HEADER = False
        if arg == "--no-show-editor":
            SHOW_EDITOR = False
        if arg.startswith('--filter='):
            dumy,filter_names = arg.split('=')
            filter_names = filter_names.split(',')

    print(f"  # Filtering by {filter_names}")
    user_post_dir = FreeCAD.getUserMacroDir() + "/post"
    if user_post_dir not in sys.path:
        print(f'## Adding {user_post_dir}')
        #sys.path = [ user_post_dir, *list(sys.path) ]
    for f in filter_names:
        #filter_module = PostProcessor.load(f"{f}_filter")
        module_name = f"{f}_filter"
        m = importlib.import_module(module_name)
        importlib.reload(m) # FIXME: only if previously loaded
        klass = module_name.title().replace('_','')
        print(f"new {klass}")
        fobj = getattr(m,klass)(objectslist, filename, argstring)
        Filters.append(fobj)

    for obj in objectslist:
        if not hasattr(obj, "Path"):
            s = "the object " + obj.Name
            s += " is not a path. Please select only path and Compounds."
            print(s)
            return

    CurrentState = {
        "line_number":0, # internal g-code line number
        "X": 0,
        "Y": 0,
        "Z": 0,
        "F": 0, # not used? was placeholder for M speed?
        "S": 0, # not used? was placeholder for spindle speed?
        "Spindle": 0,
        "Tool" : 0,
        # speeds
        "JSXY": 0,
        "JSZ": 0,
        "MSXY": 0,
        "MSZ": 0,
    }
    print("postprocessing...")
    gcode = ""

    # write header
    if OUTPUT_HEADER:
        gcode += linenumber() + "'Exported by FreeCAD\n"
        gcode += linenumber() + "'Post Processor: " + __name__ + "\n"
        gcode += linenumber() + "'Output Time:" + str(datetime.datetime.now()) + "\n"
        for f in Filters:
            gcode += comment(f"Filter: {f.__class__.__name__}")

    # Write the preamble
    if OUTPUT_COMMENTS:
        gcode += linenumber() + "'(begin preamble)\n"
    for line in PREAMBLE.splitlines(True):
        gcode += linenumber() + line

    for obj in objectslist:

        print(f"### Op {obj.Label}")

        # do the pre_op
        if OUTPUT_COMMENTS:
            gcode += linenumber() + "'(begin operation: " + obj.Label + ")\n"

        # Set speeds before each tool change (which is the first command in a job)
        # because sometimes Path forgets to do that before a move
        if  'Proxy' in dir(obj) and isinstance(obj.Proxy, ToolController):
            if OUTPUT_COMMENTS:
                gcode += linenumber() + "'(set speeds: " + obj.Label + ")\n"
            vs = []

            if obj.HorizFeed != 0.0:
                vs.append( format( GetValue(FreeCAD.Units.Quantity(obj.HorizFeed.getValueAs('mm/s')).Value),'.4f') if obj.HorizFeed != 0.0 else '' )
            elif OUTPUT_COMMENTS:
                gcode += linenumber() + "'(no HorizFeed)\n"
            if obj.HorizFeed != 0.0:
                vs.append( format( GetValue(FreeCAD.Units.Quantity(obj.VertFeed.getValueAs('mm/s')).Value),'.4f') if obj.VertFeed != 0.0 else '' )
            elif OUTPUT_COMMENTS:
                gcode += linenumber() + "'(no VertFeed)\n"

            vs.append('') # a-move-speed
            vs.append('') # b-move-speed

            if obj.HorizRapid != 0.0:
                vs.append( format( GetValue(FreeCAD.Units.Quantity(obj.HorizRapid.getValueAs('mm/s')).Value),'.4f') if obj.HorizRapid != 0.0 else '' )
            elif OUTPUT_COMMENTS:
                gcode += linenumber() + "'(no HorizRapid)\n"
            if obj.VertRapid != 0.0:
                vs.append( format( GetValue(FreeCAD.Units.Quantity(obj.VertRapid.getValueAs('mm/s')).Value),'.4f') if obj.VertRapid != 0.0 else '' )
            elif OUTPUT_COMMENTS:
                gcode += linenumber() + "'(no VertRapid)\n"

            gcode += f'VS,{ ",".join(vs)}\n'

        for line in PRE_OPERATION.splitlines(True):
            gcode += linenumber() + line

        gcode += parse(obj)

        # do the post_op
        if OUTPUT_COMMENTS:
            gcode += linenumber() + "'(finish operation: " + obj.Label + ")\n"
        for line in POST_OPERATION.splitlines(True):
            gcode += linenumber() + line

    # do the post_amble
    if OUTPUT_COMMENTS:
        gcode += "'(begin postamble)\n"
    for line in POSTAMBLE.splitlines(True):
        gcode += linenumber() + line

    if SHOW_EDITOR:
        dia = PostUtils.GCodeEditorDialog()
        dia.editor.setText(gcode)
        result = dia.exec_()
        if result:
            final = dia.editor.toPlainText()
        else:
            final = gcode
    else:
        final = gcode

    print("done postprocessing.")

    # Write the output
    gfile = pythonopen(filename, "w")
    gfile.write(final)
    gfile.close()

def _adjust_speed(command):
    """Returns a prefix command if speed needs to be changed""" 

    if "F" in command.Parameters:

        # we need to set speed for whicheve axis is mentioned, whether we have to move or not
        axis = ""
        for p in ["X", "Y", "Z"]:
            if p in command.Parameters:
                axis += p

        speed = command.Parameters["F"]
        if command.Name in move_commands.keys():
            movetype = "MS"
        elif command.Name in jog_commands.keys(): # jog
            movetype = "JS"
        else:
            print(f"warning: A command [{CurrentState['line_number']}] had a F (speed), but we don't know how to handle the speed-change for {command}")
            return ''
            
        zspeed = ""
        xyspeed = ""
        if "Z" in axis:
            speedKey = "{}Z".format(movetype)
            speedVal = GetValue(speed)
            if CurrentState[speedKey] != speedVal and speedVal != 0.0:
                CurrentState[speedKey] = speedVal
                zspeed = "{:f}".format(speedVal)
        if ("X" in axis) or ("Y" in axis):
            speedKey = "{}XY".format(movetype)
            speedVal = GetValue(speed)
            if CurrentState[speedKey] != speedVal and speedVal != 0.0:
                CurrentState[speedKey] = speedVal
                xyspeed = "{:f}".format(speedVal)
        if zspeed or xyspeed:
            txt = "{},{},{}\n".format(movetype, xyspeed, zspeed)
            return txt
        else:
            return ''
    else:
        return ''


def _axis(command):
    axis = ""
    for p in ["X", "Y", "Z"]:
        if p in command.Parameters:
            if command.Parameters[p] != CurrentState[p]:
                axis += p
    return axis


def move(command):
    txt = ""
    debug = comment( str(command) )

    # We try to optimize the command based on what has changed
    axis = ""
    for p in ["X", "Y", "Z"]:
        if p in command.Parameters:
            if command.Parameters[p] != CurrentState[p]:
                axis += p
    debug += comment( f"axis: {axis}")

    txt += _adjust_speed(command)

    if command.Name in jog_commands.keys():
        pref = "J"
    else:
        pref = "M"

    if axis == "X":
        txt += pref + "X"
        txt += "," + format(GetValue(command.Parameters["X"]), ".4f")
        txt += "\n"
    elif axis == "Y":
        txt += pref + "Y"
        txt += "," + format(GetValue(command.Parameters["Y"]), ".4f")
        txt += "\n"
    elif axis == "Z":
        txt += pref + "Z"
        txt += "," + format(GetValue(command.Parameters["Z"]), ".4f")
        txt += "\n"
    elif axis == "XY":
        txt += pref + "2"
        txt += "," + format(GetValue(command.Parameters["X"]), ".4f")
        txt += "," + format(GetValue(command.Parameters["Y"]), ".4f")
        txt += "\n"
    elif axis == "XZ":
        txt += pref + "3"
        txt += "," + format(GetValue(command.Parameters["X"]), ".4f")
        txt += ","
        txt += "," + format(GetValue(command.Parameters["Z"]), ".4f")
        txt += "\n"
    elif axis == "XYZ":
        txt += pref + "3"
        txt += "," + format(GetValue(command.Parameters["X"]), ".4f")
        txt += "," + format(GetValue(command.Parameters["Y"]), ".4f")
        txt += "," + format(GetValue(command.Parameters["Z"]), ".4f")
        txt += "\n"
    elif axis == "YZ":
        txt += pref + "3"
        txt += ","
        txt += "," + format(GetValue(command.Parameters["Y"]), ".4f")
        txt += "," + format(GetValue(command.Parameters["Z"]), ".4f")
        txt += "\n"
    elif axis == "" and txt == "" :
        txt += comment("skipped as duplicate")
        print(f"warning: skipping duplicate move. {command}")
    elif txt == "":
        # otherwise, might be a relative move to 0 to set speed
        print(CurrentState)
        print(command)
        print("I don't know how to handle '{}' for a move.".format(axis))
        txt += comment("Don't know how to handle: {command}")

    #print(f"G0: {command.Parameters}")
    return txt # debug + txt


def arc(command):
    if command.Name == "G2":  # CW
        dirstring = "1"
    else:  # G3 means CCW
        dirstring = "-1"
    txt = ""
    txt += _adjust_speed(command)
    txt += "CG,,"
    txt += format(GetValue(command.Parameters["X"]), ".4f") + ","
    txt += format(GetValue(command.Parameters["Y"]), ".4f") + ","
    # if a G2 (arc) happens to have x|y == 0, then J,I is omitted!
    txt += format(GetValue(command.Parameters["I"]) if 'I' in command.Parameters.keys() else 0.0, ".4f")  + ","
    txt += format(GetValue(command.Parameters["J"]) if 'J' in command.Parameters.keys() else 0.0, ".4f")  + ","
    txt += "T" + ","
    txt += dirstring
    txt += "\n"
    return txt


def tool_change(command):
    txt = ""
    if CurrentState['Tool'] != command.Parameters["T"]:
        if OUTPUT_COMMENTS:
            txt += "'a tool change happens now\n"
        for line in TOOL_CHANGE.splitlines(True):
            txt += line
        txt += "&ToolName=" + str(int(command.Parameters["T"]))
        txt += "\n"
        txt += "&Tool=" + str(int(command.Parameters["T"]))
        txt += "\n"
        CurrentState['Tool'] = command.Parameters["T"]

    return txt


def comment(command):
    if OUTPUT_COMMENTS:
        return "'" + str(command) + "\n"
    else:
        print("a comment", command)
        return ''


def absolute_positions(command):
    return comment("Absolute Positions") + "SA\n"


def relative_positions(command):
    return comment("Relative Positions") + "SR\n"


def from_standard_zero(command):
    return comment(f"Active coordinate system is machine zero, 'base' ({command})")
    
def spindle(command):
    txt = ""
    if command.Name == "M3":  # CW
        pass
    else:
        pass
    if CurrentState['Spindle'] != command.Parameters["S"]:
        txt += comment(f"Spindle was {CurrentState['Spindle']}")
        txt += "TR," + str(command.Parameters["S"]) + "\n"
        txt += "C6\n"
        txt += "PAUSE 2\n"
        CurrentState['Spindle'] = command.Parameters["S"]
    return txt


# Supported Commands
move_commands = {
    # i.e. movement during a cut
    # these commands can change speed, i.e. "F..."
    "G1": move,
    "G2": arc,
    "G3": arc,
    # redundant:
    "G01": move,
    "G02": arc,
    "G03": arc,
    # probably drill & probe go in here https://wiki.freecadweb.org/Path_scripting#The_FreeCAD_Internal_GCode_Format
}
jog_commands = {
    # high-speed moves
    # these commands can change speed, i.e. "F..."
    "G0": move,
    # redundant:
    "G00": move,
}
scommands = {
    # Keep move/jog commands in the above lists so speed changes are tracked
    **move_commands,
    **jog_commands,
    "M3": spindle,
    "M6": tool_change,
    "M06": tool_change,
    "M03": spindle, # redundant
    "G54" : from_standard_zero,
    "G91" : relative_positions,
    "G90" : absolute_positions,
    "message": comment,
}

def parse(pathobj):
    output = ""
    # Above list controls the order of parameters
    # FIXME: there should be parse_objects, and parse(single-gcode)

    if hasattr(pathobj, "Group"):  # We have a compound or project.
        if OUTPUT_COMMENTS:
            output += linenumber() + "'(compound: " + pathobj.Label + ")\n"
        for p in pathobj.Group:
            output += parse(p)
    else:  # parsing simple path
        # groups might contain non-path things like stock.
        if not hasattr(pathobj, "Path"):
            return output
        if OUTPUT_COMMENTS:
            output += linenumber() + "'(Path: " + pathobj.Label + ")\n"

        for c in filter(pathobj, pathobj.Path.Commands):
            CurrentState['line_number'] += 1
            command = c.Name
            if command in scommands:
                output += scommands[command](c)
                if c.Parameters:
                    CurrentState.update(c.Parameters)
            elif command[0] == "(":
                output += "' " + command + "\n"
            else:
                print("I don't know what the hell the command: ", end="")
                print(command + " means.  Maybe I should support it.")

    return output


def linenumber():
    return ""


# print(__name__ + " gcode postprocessor loaded.")
